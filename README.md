# GK Loyalty Membership App

## Configuration
### Select unique package name (Bundle ID)
Choose some package name for both iOS and Android applications (something like `com.gkloyalty.main`).

### Create Firebase config files
1. __Firebase -> Settings -> "General" tab__ add iOS and Android apps, specifying only package name
2. Download `google-services.json` and `GoogleService-Info.plist` and save them into `/firebaseConfigFiles` directory

### Create FB apps
Create Facebook app for both Android and iOS, enable `Facebook Login`
- Steps for Android: https://developers.facebook.com/quickstarts/?platform=android
- Steps for iOS: https://developers.facebook.com/quickstarts/?platform=ios

### Configure app
- Run configuration script: `npm run configure`
You should specify several parameters there:
    - `Application name` - title for app
    - `Bundle ID` - Your package name
    - `facebookAppId` - Facebook App ID

### Installing and run locally
- Run `yarn install`
- Run `pod install` from `/ios` subdirectory
- For iOS open `GkMembershipApp.xcworkspace` __(not `GkMembershipApp.xcodeproj`!)__
- For Android run `react-native run-android`

### Creating release package
- For iOS use standard __Product->Archive__
- For Android:
    - create keys `sudo keytool -genkey -v -keystore my-release-key.keystore -alias gk-loyalty -keyalg RSA -keysize 2048 -validity 10000`, use passwords `mooose` (yes, with three 'o')
    - save `release-key.keystore` into `/android/app` folder
    - run `./gradlew assembleRelease` from `/android` directory

### Customization
- iOS
    - To customize launch screen change image resource `Splash` in `Images.xcassets` in XCode
    - To customize app icon edit `AppIcon` resource in `Images.xcassets` in XCode
- Android
    - To customize launch screen copy corresponding __png__ images named `launch_screen.png` into all `drawable` directories in `android/app/src/res`
    - To customize app icon copy corresponding __png__ images named `ic_launcher.png` and `ic_launcher_round.png` into all `mipmap` directories in `android/app/src/res`
