import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  FlatList,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import moment from 'moment';

import { List } from 'immutable';


import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import styles from '../styles/rewards';

import { notificationsActions } from '../redux/actions';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Rewards extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    const { fetchNotifications } = this.props;
    fetchNotifications();
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderItem(data, index) {
    const {
      appText,
    } = this.props;

    const date = moment(data.get('date')).format('M/D');
    const name = data.get('name');

    return (
      <View
        style={styles.item}
        key={index}>
        <Text style={[styles.itemText, appText]}>
          {`${date}: ${name}`}
        </Text>
      </View>
    );
  }

  render() {
    const {
      rewards,
      appBg,
      barStyle,
      appTitle,
      navBarBgColor,
    } = this.props;
    const header = (
      <View style={styles.headerContainer}>
        <Text style={[styles.headerText, appTitle]}>
          Reward History
        </Text>
      </View>
    );

    firebase.analytics().setCurrentScreen('rewards', 'rewards');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <FlatList
          style={[styles.list, appBg]}
          ref={(ref) => { this.list = ref; }}
          ListHeaderComponent={header}
          ListEmptyComponent={<Text>No rewards yet</Text>}
          ItemSeparatorComponent={() => <View style={styles.separator} />}
          ListFooterComponent={<View style={styles.footer} />}
          data={rewards.toArray()}
          keyExtractor={item => item.get('id')}
          renderItem={({ item, index }) => this.renderItem(item, index)}
        />
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    rewards: (state.getIn(['auth', 'user', 'rewards']) || List([]))
      .sort((a, b) => (moment(a.get('date')).isAfter(moment(b.get('date'))) ? -1 : 1)),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    fetchNotifications: () => notificationsActions.fetchNotifications(),

  },
)(Rewards);
