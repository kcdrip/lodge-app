import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  Share,
  Text,
  Image,
  View,
  StatusBar,
  TouchableOpacity,
  StyleSheet,
  Linking,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import HTMLView from 'react-native-htmlview';

import moment from 'moment';

import styles from '../styles/news';

//import htmlStyles from '../styles/htmlStyles';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import ShareButton from '../components/shareButton';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class NewsletterPost extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  shareNews(news) {
    const {
      props: { screenProps: { modal } },
    } = this;

    modal.setContent(<View />, undefined, this.ref);

    const dataToShare = {
      title: news.get('title') || 'Title',
      message: (news.get('content') || 'Content').replace(/<[^>]*>/g, ''),
    }

    Share.share(dataToShare).then(() => modal.clearContent());
  }

  render() {
    const {
      props: {
        news,
        appBg,
        barStyle,
        cardBg,
        appTitle,
        appText,
        timeColor,
        navBarBgColor,
        navigation: {
          state: {
            params: {
              id,
            },
          },
        },
      },
    } = this;

    // will fetch the right ID
    // depending on whether or not News has been rearranged

    var data = null;
    if (typeof id == 'number') {
      let local = news;
      if (!Array.isArray(news)) {
        local = local.toArray();
      }
      data = local[id];
    } else {
      data = news.get(id);
    }

    const htmlStyles = StyleSheet.create({
      newsletter: {
        color: '#2699FB',
        fontFamily: 'Arial',
        fontSize: 14,
        lineHeight: 18.5,
        ...(appText || {}),
      },
      h1: {
        fontSize: 26,
        fontWeight: '600',
      },
      h2: {
        fontSize: 20,
        fontWeight: '600',
      },
      a: {
        textDecorationLine: 'underline',
      },
      gray: {
        color: '#6E6565',
        fontWeight: 'bold',
      },
      div: {
      },
    });

    const html = (data.get('content') || '').replace(new RegExp('<p><br></p>', 'g'), '<br>');

    firebase.analytics().setCurrentScreen('news post', 'news post');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, appBg]}
      >
         <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView style={[styles.newsDetailsContainer, appBg]}>
          <View style={styles.detailsImageContainer}>
            <Image
              style={[{ width: '100%', height: 180}, cardBg]}
              source={{ uri: data.get('image') }}
            />
            <View style={styles.newsDetailsImageItemsContainer}>
              <View style={styles.newsDateContainer}>
                <Icon
                  name="md-clock"
                  style={styles.facebookIcon}
                  size={18}
                  color={(timeColor || {}).color ||'#2699FB'}
                />
                <Text style={[styles.detailsCardTime, timeColor]}>
                  { moment(data.get('date')).format('MMMM DD, YYYY') }
                </Text>
              </View>
              <ShareButton onPress={() => this.shareNews(data)} />
            </View>
          </View>
          <View style={styles.newsDetailsContentContainer}>
            <Text style={[styles.newsDetailsTitle, appTitle]}>
              {data.get('title')}
            </Text>
            <Text style={[styles.newsDetailsSubtitle, appTitle]}>
              {data.get('subtitle')}
            </Text>
            <HTMLView
              style={styles.newsletterPostMainText}
              paragraphBreak={'\n'}
              stylesheet={htmlStyles}
              onLinkPress={link => Linking.openURL((link.indexOf('://') === -1) ? `http://${link}` : link)}
              value={`<newsletter>${html}</newsletter>`}
            />
          </View>
         {/* <TouchableOpacity style={styles.commentsContainer}>
            <MaterialIcon
              name="comment"
              size={20}
              color="#2699FB"
            />
            <Text style={styles.commentCounter}>
              120
            </Text>
          </TouchableOpacity> */}
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    news: state.getIn(['news', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    timeColor:  getColor(state, 'button', 'background'),
  }),
)(NewsletterPost);
