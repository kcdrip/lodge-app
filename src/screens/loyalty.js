import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, TouchableOpacity, Image, StyleSheet, Text, View, FlatList, StatusBar, Platform } from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';

import moment from 'moment';


import styles from '../styles/home';

import LoyaltyCard from '../components/loyaltyCard';
import OfferCard from '../components/offerCard';
import ScratchCard from '../components/scratchCard';

import { rewardsActions } from '../redux/actions';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import {
  getBg,
  getColor,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import firebase from 'react-native-firebase';


class Loyalty extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    const {
      fetchRewards,
    } = this.props;

    fetchRewards();
     this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  render() {
    const {
      offers,
      loyaltyRewards,
      appBg,
      barStyle,
      buttonBg,
      buttonText,
      navBarBgColor,
      navigation: {
        navigate,
      },
    } = this.props;

    const showScratchCard = true;

    const loyaltyRewardList = loyaltyRewards ?
      loyaltyRewards.map((data, id) => (
        <OfferCard
          {...this.props}
          key={id}
          viewRef={this.ref}
          data={
            data
              .set('offerId', id)
              .set('offerName', data.get('name'))
              .set('offerSubTitle', data.get('name'))
              .set('offerTitle', 'Loyalty reward')
          }
        />
      )).toArray() : null;

    const offerList = offers.map((data, id) => (
      <OfferCard
        {...this.props}
        key={id}
        viewRef={this.ref}
        data={data.set('offerId', id)}
      />
    )).toArray();

   /* const scratchCard = showScratchCard ? (
      <ScratchCard
        {...this.props}
        text={"You could win a bottle of rum"}
      />
    ) : null; */

    firebase.analytics().setCurrentScreen('loyalty', 'loyalty');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView style={[styles.scrollView, appBg]}>
          <LoyaltyCard
            {...this.props}
            onCheckIn={this.openModal}
            totalPoint={10}
            offAmount={2}
          />
          <View style={styles.innerContainer}>
            <View style={styles.sectionHeader}>
              <Text style={styles.title}>
                Rewards
              </Text>
              <TouchableOpacity
                style={[styles.button, buttonBg]}
                onPress={() => navigate('Rewards')}
              >
                <Text style={[styles.buttonText, buttonText]}>
                  HISTORY
                </Text>
              </TouchableOpacity>
            </View>
            {/*scratchCard*/}
            {loyaltyRewardList}
            {offerList}
          </View>
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    loyaltyRewards: state.getIn(['auth', 'user', 'loyaltyRewards']),
    offers: state.getIn(['rewards', 'items']).filter(v => v.get('isOffer') || false),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    buttonBg:  getBg(state, 'button', 'background'),
    buttonText: getColor(state, 'button', 'text'),
  }),
  {
    fetchRewards: () => rewardsActions.fetchRewards(),
  },
)(Loyalty);
