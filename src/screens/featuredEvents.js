import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
  View,
  StatusBar,
  Platform,
  FlatList,
} from 'react-native';

import { connect } from 'react-redux';

import styles from '../styles/events';

import { eventsActions } from '../redux/actions';
import { getTimeFrame } from '../utils/selectors';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import DateCircle from '../components/dateCircle';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import firebase from 'react-native-firebase';


class FeaturedEvents extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.props.fetchEvents();
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderEventCard(event) {
    const {
      cardBg,
      cardTitle,
      cardSubtitle,
       navigation: {
        navigate,
      },
    } = this.props;

    const id = event.get('id');

    const timeFrame = getTimeFrame(event);
    const timeFrameLabel = (timeFrame ? (
      <Text style={[styles.topEventCardTime, cardSubtitle]}>
        (
        {timeFrame}
        )
      </Text>
    ) : null);
  //key={id}
    return (
      <TouchableOpacity
        style={[styles.eventCard, cardBg]}
        onPress={() => navigate('EventDetails', { id })}
      >
        <Image
          style={styles.eventCardImage}
          source={{ uri: event.get('image') }}
        />
        <View style={[styles.cardTitleContainer, cardTitle]}>
          <DateCircle date={event.get('date')} />
          <View style={styles.cardTitleRightBlock}>
            <Text style={[styles.eventCardTitle, cardSubtitle]}>
              {event.get('title')}
            </Text>
            {timeFrameLabel}
          </View>
        </View>
        <Text
          ellipsizeMode="tail"
          numberOfLines={5}
          style={[styles.eventDetails, cardSubtitle]}
        >
          {event.get('details')}
        </Text>
      </TouchableOpacity>
    );
  }

  // double navigation is NOT a typo
  // need it first time to get props from Home screen
  render() {
    const {
      props: {
        events,
        appBg,
        barStyle,
        navBarBgColor,
        navigation,
        navigation: {
          navigate,
        },
      },
    } = this;

    const specials = navigation.getParam('specials', false);

    let allEvents = events;

    if (specials == true) {
      allEvents = allEvents.filter(v => v.get('isStarred')).slice(0, 14);
    }
    else {
      allEvents = allEvents.filter(v => v.get('isFeatured')).slice(0, 50);
    }

    allEvents = allEvents.map((item) => this.renderEventCard(item)).toArray();

    firebase.analytics().setCurrentScreen('featured events', 'featured events');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        {/*
        <FlatList
          style={styles.scrollView}
          ref={(ref) => { this.list = ref; }}
          ListHeaderComponent={<View style={{height: 16}} />}
          data={events.toArray()}
          keyExtractor={item => `${item.get('id')}-${item.get('date')}`}
          renderItem={({ item }) => this.renderEventCard(item)}
        />
        */}
        <ScrollView style={styles.scrollView}>
          <View style={styles.innerContainer}>
            { allEvents }
          </View>
        </ScrollView>
        {/**/}
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

// Events was previously filtered to just Featured Events
export default connect(
  state => ({
    events: state.getIn(['events', 'items']).slice(0, 100),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubtitle: getColor(state, 'card', 'subtitle'),
  }),
  {
    fetchEvents: () => eventsActions.fetchEvents(),
  },
)(FeaturedEvents);
