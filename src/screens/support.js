import React, { Component } from 'react';

import PropTypes from 'prop-types';

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  StatusBar,
  Platform,
} from 'react-native';

import firebase from 'react-native-firebase';
import uuid from 'uuid';

import { connect } from 'react-redux';

import ErrorPopup from '../components/errorPopup';

import { color } from '../styles/Theme';

import { authActions } from '../redux/actions';

import styles from '../styles/auth';

import topNavigation from '../components/topNavigation';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Support extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      keyboardIsVisible: false,
      text: '',
    };
  }

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      () => this.setState({ keyboardIsVisible: true }));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      () => this.setState({ keyboardIsVisible: false }));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  handleBack() {
    const { navigation } = this.props;
    navigation.goBack();
  }

  handleSubmit() {
    const {
      state: {
        text,
      },
      props: {
        name,
        email,
        screenProps: {
          modal: {
            setContent,
          },
        },
        navigation,
      },
    } = this;


    firebase.database().ref('contact')
      .child(uuid.v4())
      .set({
        text,
        sender: name,
        email,
        os: Platform.OS,
      })
      .then((err) => {
        setContent(<ErrorPopup message={err || 'Your message was sent'} />, undefined, this.ref);
        navigation.navigate('Profile');
      });
  }

  render() {
    const {
      text,
      keyboardIsVisible,
    } = this.state;

    const {
      appBg,
      barStyle,
      appTitle,
      appText,
      navBarBgColor,
    } = this.props;

    const title = keyboardIsVisible ? null : (
      <View style={styles.titleContainer}>
        <Text style={[styles.editorScreenTitle, styles.mb_min, appTitle]}>
          Support
        </Text>
        <Text style={[styles.subTitle, appText]}>
          Having an issue with the app?
        </Text>
        <Text style={[styles.subTitle, appText]}>
          Please fill out the form below and
        </Text>
        <Text style={[styles.subTitle, appText]}>
           member of our team will get in touch ASAP
        </Text>
      </View>
    );

    firebase.analytics().setCurrentScreen('support', 'support');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, styles.centeredContainer, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        {title}
        <TextInput
          style={[styles.input, { height: 240 }]}
          multiline
          placeholderTextColor={color.INPUT_TEXT}
          placeholder="Please Describe your issue…"
          onChangeText={text => this.setState({ text })}
          value={text}
        />
        <TouchableOpacity
          style={[styles.button, styles.buttonPrimary]}
          onPress={() => this.handleSubmit() }
        >
          <Text style={styles.buttonPrimaryText}>
            SUBMIT
          </Text>
        </TouchableOpacity>
        <View style={{ height: keyboardIsVisible ? 230 : 0 }} />
      </View>
    )
  }
}

Support.propTypes = {
  screenProps: PropTypes.object.isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default connect(
  state => ({
    id: state.getIn(['auth', 'user', 'id']),
    name: state.getIn(['auth', 'user', 'name']) || '',
    email: state.getIn(['auth', 'user', 'email']) || '',
    phone: state.getIn(['auth', 'user', 'phone']),
    birthdate: state.getIn(['auth', 'user', 'birthdate']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    updateProfile: (data, id) => authActions.updateProfile(data, id),
  },
)(Support);
