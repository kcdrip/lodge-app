import React, { Component } from 'react';

import PropTypes from 'prop-types';

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Keyboard,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import Spinner from '../components/spinner';
import ErrorPopup from '../components/errorPopup';

import { color } from '../styles/Theme';

import { authActions } from '../redux/actions';

import styles from '../styles/auth';

import topNavigation from '../components/topNavigation';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import firebase from 'react-native-firebase';

class ChangePassword extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      forceCheck: false,
      keyboardIsVisible: false,
      oldPassword: '',
      password: '',
      conPassword: '',
    };
  }

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }


  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      () => this.setState({ keyboardIsVisible: true }));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      () => this.setState({ keyboardIsVisible: false }));
  }

  componentWillReceiveProps(newProps) {
    const { props } = this;
    const {
      error,
      isBusy,
      dismissError,
      redirectTo,
      screenProps: {
        modal: {
          setContent,
          clearContent,
          setDismissHandler,
        },
      },
      navigation,
      clearRedirect,
    } = newProps;

    if (props.error !== error || props.isBusy !== isBusy) {
      const modalContent =
        isBusy ? <Spinner text='One moment...' /> :
        error ? <ErrorPopup message={error} /> :
        null;

      if (modalContent) {
        setContent(modalContent, undefined, this.ref);
        setDismissHandler(() => dismissError());
      } else {
        clearContent();
      }
    }

    if (redirectTo && props.redirectTo !== redirectTo) {
      navigation.navigate(redirectTo);
      clearRedirect();
    }

    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }


  handleBack() {
    const { navigation } = this.props;
    navigation.goBack();
  }

  handleChangePassword() {
    const {
      props: {
        id,
        updatePassword,
      },
      state: {
        oldPassword,
        password,
      },
    } = this;

    updatePassword(
      oldPassword,
      password,
      id,
    );
  }

  render() {
    const {
      keyboardIsVisible,
      forceCheck,
      oldPassword,
      password,
      conPassword,
    } = this.state;

    const {
      appBg,
      barStyle,
      appTitle,
      navBarBgColor,
    } = this.props;

    const passwordLongEnough = password.length > 5;
    const passwordEqual = password === conPassword;

    const changePasswordEnabled = passwordLongEnough && passwordEqual;

    firebase.analytics().setCurrentScreen('changePassword', 'changePassword');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, styles.centeredContainer, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <Text style={[styles.editorScreenTitle, appTitle]}>
          Change Password
        </Text>
        <TextInput
          style={styles.input}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          placeholder="Old Password"
          secureTextEntry
          onChangeText={oldPassword => this.setState({ oldPassword })}
          value={oldPassword}
        />
        <TextInput
          style={[styles.input, forceCheck && !passwordLongEnough ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          placeholder="New Password"
          secureTextEntry
          onChangeText={password => this.setState({ password })}
          value={password}
        />
        <TextInput
          style={[
            styles.input,
          ]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          placeholder="Confirm Password"
          secureTextEntry
          onChangeText={conPassword => this.setState({ conPassword, forceCheck: true })}
          value={conPassword}
        />
        <View style={styles.spacer} />
        <TouchableOpacity
          style={[
            styles.button,
            styles.buttonPrimary,
            changePasswordEnabled ? null : styles.buttonDisabled,
          ]}
          onPress={() => changePasswordEnabled ? this.handleChangePassword() : null}
        >
          <Text style={styles.buttonPrimaryText}>
            CHANGE PASSWORD
          </Text>
        </TouchableOpacity>
        <View style={{ height: keyboardIsVisible ? 200 : 0 }} />
      </View>
    );
  }
}

ChangePassword.propTypes = {
  id: PropTypes.string.isRequired,
  updatePassword: PropTypes.func.isRequired,
  redirectTo: PropTypes.func,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

ChangePassword.defaultProps = {
  redirectTo: () => null,
};

export default connect(
  state => ({
    id: state.getIn(['auth', 'user', 'id']),
    email: state.getIn(['auth', 'user', 'email']) || '',
    error: state.getIn(['auth', 'error']),
    redirectTo: state.getIn(['auth', 'redirectTo']),
    isBusy: state.getIn(['auth', 'isBusy']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    updatePassword: (
      oldPassword,
      newPassword,
      id,
    ) => authActions.updatePassword(oldPassword, newPassword, id),
    clearRedirect: () => authActions.clearRedirect(),
    dismissError: () => authActions.dismissError(),
  },
)(ChangePassword);
