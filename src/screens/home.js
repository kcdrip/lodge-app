import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  TouchableOpacity,
  Image,
  StyleSheet,
  Text,
  View,
  Dimensions,
  StatusBar,
  Linking
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import { connect } from 'react-redux';

import moment from 'moment';

import SpecialsCard from '../components/specialsCard';

import styles from '../styles/home';

import LoyaltyCard from '../components/loyaltyCard';
import DateCircle from '../components/dateCircle';
import DayEvents from '../components/dayEvents';
import PageButton from '../components/pageButton';

import { newsActions, eventsActions, articlesActions, notificationsActions } from '../redux/actions';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import NewsCard from '../components/newsCard';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

const arrayToPairs = arr => arr.reduce((p, v, i) => (
  (i % 2 === 0) ? [...p, [v, (i + 1 < arr.length) ? arr[i + 1] : null]] : p), []);

// There's something you should know about us, Lone Star.
// I am your father's brother's nephew's cousin's former roommate.

class Home extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    const {
      fetchNews,
      fetchEvents,
      fetchArticles,
      fetchNotifications,
      navBarBgColor,
       screenProps: {
        homePage: {
          setScrollToBottomHandler,
        },
      },
    } = this.props;

    fetchNews();
    fetchEvents();
    fetchArticles();

    setTimeout(() => fetchNotifications(), 1000);

    this.updateNavColor(navBarBgColor);

    setScrollToBottomHandler(() => this.scrollToEnd());
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  scrollToEnd() {
    if (this._scroll != null) {
      this._scroll.scrollToEnd();
    }
  }

  renderTopEventCard(event, id, navigate) {
    const {
      cardBg,
      cardTitle,
      cardSubtitle,
    } = this.props;

    const time = event.get('from');
    const timeValue = time != null ? `(${time})` : ' ';
    return (
      <TouchableOpacity
        key={id}
        style={[styles.topEventCard, cardBg]}
        onPress={() => navigate('EventDetails', { id })}
      >
        <Image
          style={styles.topEventCardImage}
          source={{ uri: event.get('image') }}
        />
        <DateCircle date={event.get('date')} />
        <Text style={[styles.topEventCardTitle, cardTitle]}>
          {event.get('title')}
        </Text>
        <Text style={[styles.topEventCardTime, cardSubtitle]}>
          {timeValue}
        </Text>
      </TouchableOpacity>
    );
  }

  renderEventCard(event, id, navigate) {
    const {
      boxBg,
      boxTitle,
    } = this.props;

    return (
      <TouchableOpacity
        key={id}
        style={[styles.eventCard, boxBg]}
        onPress={() => navigate('EventDetails', { id })}
      >
        <DateCircle
          small
          date={event.get('date')}
        />
        <Text
          style={[styles.eventCardTitle, boxTitle]}
          ellipsizeMode='tail'
          numberOfLines={2}
        >
          {event.get('title') }
        </Text>
        <Icon
          name="md-arrow-forward"
          size={24}
          color={(boxTitle || {}).color || '#2699FB'}
        />
      </TouchableOpacity>
    );
  }

  renderEvents() {
    const {
      props: {
        events,
        navigation: {
          navigate,
        },
      },
    } = this;

    const featuredEvents = events.filter(v => v.get('isFeatured')).slice(0, 5);

    if (featuredEvents.size === 0) {
      return null;
    }

    const firstEventId = featuredEvents.keySeq().first();
    return featuredEvents
      .map((v, id) => (id === firstEventId
        ? this.renderTopEventCard(v, id, navigate)
        : this.renderEventCard(v, id, navigate)
    )).toArray();
  }

  renderSpecials() {
    const {
      props: {
        events,
        navigation: {
          navigate,
        },
      },
    } = this;

    const specialEvents = events.filter(v => v.get('isStarred')).slice(0, 5);

    if (specialEvents.size === 0) {
      return null;
    }

    const firstEventId = specialEvents.keySeq().first();
    return specialEvents
      .map((v, id) => (id === firstEventId
        ? this.renderTopEventCard(v, id, navigate)
        : this.renderEventCard(v, id, navigate)
    )).toArray();
  }

  renderPageButtons() {
    const {
      navigation: {
        navigate,
      },
      articles,
      links,
      friendsLabel,
      friendsPriority,
      friendsBackground,
      newsLabel,
      newsPriority,
      newsBackground,
      boxBg,
      boxTitle,
    } = this.props;

    const staticPageButtons = [{
      name: newsLabel,
      screen: 'Newsletter',
      priority: newsPriority,
      background: newsBackground,
    }, {
      name: friendsLabel,
      screen: 'Friends',
      priority: friendsPriority,
      background: friendsBackground,
    }];

    const articleButtons = articles
      .map((v, id) => ({
        screen: 'Article',
        screenId: id,
        name: v.get('buttonTitle') || v.get('title'),
        background: v.get('background'),
        priority: v.get('priority'),
      }))
      .toArray();

    const linksButtons = links ? links
      .map((v, id) => ({
        screen: 'Link',
        screenId: id,
        openUrl: v.get('openLinkInBrowser') ? v.get('link') : undefined,
        name: v.get('buttonTitle') || v.get('title'),
        background: v.get('background'),
        priority: v.get('priority'),
      }))
      .toArray() : [];

    const buttons = [
      ...staticPageButtons,
      ...articleButtons,
      ...linksButtons,
    ].sort((a, b) => a.priority - b.priority);

    return arrayToPairs(buttons).map((row, rowIndex) => (
      <View
        style={styles.notificationRow}
        key={rowIndex}
      >
        {row.map((v, index) => (v ? (
          <PageButton
            key={index}
            style={boxBg}
            backgroundImage={v.background}
            textStyle={boxTitle}
            name={v.name}
            left={index === 0}
            onPress={() => v.openUrl ? Linking.openURL(v.openUrl) : navigate(v.screen, { id: v.screenId })}
          />
        ) : null))}
      </View>
    ));
  }

  render() {
    const {
      visible,
      appBg,
      barStyle,
      buttonBg,
      buttonText,
      navBarBgColor,
      featuredNews,
      navigation: {
        navigate,
      },
    } = this.props;

    firebase.analytics().setCurrentScreen('home', 'home');

    return visible ? (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView
          ref={ref => { this._scroll = ref; }}
          style={[styles.scrollView]}
        >
          <LoyaltyCard
            {...this.props}
            viewRef={this.ref}
            onPress={() => navigate('Loyalty')}
          />
          <View>
          <View style={styles.innerContainer}>
            {featuredNews ? (
              <NewsCard
                data={featuredNews}
                id={featuredNews.get('id')}
                navigate={navigate}
              />
            ) : null}
            <View style={styles.sectionHeader}>
              <Text style={styles.titleEvents}>
                Upcoming Events
              </Text>
              <TouchableOpacity
                style={[styles.button, buttonBg]}
                onPress={() => navigate('FeaturedEvents',
                {specials: false}
              )}
              >
                <Text style={[styles.buttonText, buttonText]}>
                  VIEW ALL
                </Text>
              </TouchableOpacity>
            </View>
            <View style={styles.eventsSection}>
              { this.renderEvents() }
            </View>
            <View style={styles.sectionHeader}>
              <Text style={styles.titleEvents}>
                Kitchen Specials
              </Text>
              <TouchableOpacity
                style={[styles.button, buttonBg]}
                onPress={() => navigate('FeaturedEvents',
                {specials: true}
              )}
              >
                <Text style={[styles.buttonText, buttonText]}>
                  VIEW ALL
                </Text>
              </TouchableOpacity>
            </View>
            </View>
            {/* innerContainer has side padding, so SpecialsCard is placed
              here in the middle where it is unpadded */}
            <SpecialsCard
              navigate={navigate}
            />
          <View style={styles.innerContainer}>

            <View style={styles.sectionHeader}>
              <View>
                <Text style={styles.title}>
                  What’s Happening
                </Text>
                <Text style={styles.day}>
                  {moment().format('dddd')}
                </Text>
              </View>
              <TouchableOpacity
                style={[styles.button, buttonBg]}
                onPress={() => navigate('Calendar')}
              >
                <Text style={[styles.buttonText, buttonText]}>
                  VIEW CALENDAR
                </Text>
              </TouchableOpacity>
            </View>
            <DayEvents
              {...this.props}
              viewRef={this.ref}
            />
            { this.renderPageButtons() }
            <View style={styles.footer} />
          </View>
          </View>
        </ScrollView>
        <BottomNavigation
          {...this.props}
          onMore={() => this._scroll.scrollToEnd()}
        />
      </View>
    ) : <View />;
  }
}

export default connect(
  state => ({
    events: state.getIn(['events', 'items']),
    featuredNews: state.getIn(['news', 'items']).find(v => v.get('isFeatured')),
    articles: state.getIn(['articles', 'items']),
    friendsLabel: state.getIn(['auth', 'settings', 'friends']) || ' ',
    friendsPriority: state.getIn(['auth', 'settings', 'friendsPriority']) || -1,
    friendsBackground: state.getIn(['auth', 'settings', 'friendsBackground']),
    links: state.getIn(['auth', 'settings', 'links']),
    newsLabel: state.getIn(['auth', 'settings', 'newsletter']) || ' ',
    newsPriority: state.getIn(['auth', 'settings', 'newsletterPriority']) || -2,
    newsBackground: state.getIn(['auth', 'settings', 'newsletterBackground']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    boxBg: getBg(state, 'box', 'background'),
    boxTitle: getColor(state, 'box', 'title'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubtitle: getColor(state, 'card', 'subtitle'),
    buttonBg:  getBg(state, 'button', 'background'),
    buttonText: getColor(state, 'button', 'text'),
    visible: state.getIn(['auth', 'themeIsLoaded']) && state.getIn(['auth', 'settingsAreLoaded']),
  }),
  {
    fetchNews: () => newsActions.fetchNews(),
    fetchEvents: () => eventsActions.fetchEvents(),
    fetchArticles: () => articlesActions.fetchArticles(),
    fetchNotifications: () => notificationsActions.fetchNotifications(),
  },
)(Home);
