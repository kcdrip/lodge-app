import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  Share,
  Text,
  Image,
  View,
  StatusBar,
  TouchableOpacity,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import moment from 'moment'

import styles from '../styles/events';
import commonStyles from '../styles/common';

import { getTimeFrame } from '../utils/selectors';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import DateCircle from '../components/dateCircle';
import ShareButton from '../components/shareButton';

import { addToCalendar } from '../utils/events';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class EventDetails extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    console.log('Art up col', color);
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  shareEvent(event) {
    const {
      props: { screenProps: { modal } },
    } = this;

    modal.setContent(<View />, undefined, this.ref);

    Share.share({
      title: event.get('title'),
      message: event.get('details') || event.get('title'),
    }).then(() => modal.clearContent());
  }

  handleAddToCalendar(event) {

    const {
      props: { screenProps: { modal } },
    } = this;


    const toCalDate = v => `${moment(v).format(moment.HTML5_FMT.DATETIME_LOCAL_MS)}Z`;
    const toTime = v => moment.utc(v, 'HH:mmA').format('HH:mm');

    const startTime = event.get('from');
    const endTime = event.get('to');

    const allDay = startTime == null && endTime == null;

    let startDate;
    let endDate;

    if (allDay) {
      startDate = toCalDate(event.get('date'));
      endDate = startDate;
    } else {
      const date = moment(event.get('date')).format('YYYY-MM-DD');
      startDate = toCalDate(`${date} ${toTime(startTime)}`);
      endDate = toCalDate(`${date} ${toTime(endTime)}`);
    }

    const id = event.get('id');

    console.log('addToCalendar ed', {
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    });

    addToCalendar({
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    }).then(v =>
      modal.setContent(
        <Text style={{color: 'white', fontSize: 20}}>
          Event "{event.get('title')}" added to calendar
        </Text>,
        undefined,
        this.ref,
      ));

  }

  render() {
    const {
      props: {
        events,
        barStyle,
        appBg,
        cardBg,
        cardSubtitle,
        appTitle,
        appSubtitle,
        appText,
        buttonBackground,
        buttonText,
        navBarBgColor,
        navigation: {
          state: {
            params: {
              id,
            },
          },
        },
      },
    } = this;

    const event = events.get(id);

    const timeFrame = getTimeFrame(event);
    const timeFrameLabel = (timeFrame ? (
      <Text style={[styles.detailsCardTime, cardSubtitle]}>
        {timeFrame}
      </Text>
    ) : <View />);

    const addToCalendar = (
      <TouchableOpacity
        onPress={() => this.handleAddToCalendar(event)}
        style={[
          commonStyles.modalButton,
          commonStyles.modalButtonCalendar,
          buttonBackground,
        ]}
      >
        <Text style={[
          commonStyles.modalButtonText,
          buttonText,
        ]}>
          ADD TO CALENDAR
        </Text>
      </TouchableOpacity>
    );

    firebase.analytics().setCurrentScreen('event details', 'event details');

    return (
      <View
        style={[styles.container, appBg]}
        ref={ref => this.ref = ref}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView style={[styles.eventDetailsContainer, appBg]}>
          <View style={[styles.detailsImageContainer, cardBg]}>
            <Image
              style={{ width: '100%', height: 180 }}
              source={{ uri: event.get('image') }}
            />
            <View style={styles.eventDetailsImageItemsContainer}>
              <DateCircle date={event.get('date')} />
              <View style={styles.eventDetailsImageRightBlock}>
                {timeFrameLabel}
                <ShareButton onPress={() => this.shareEvent(event)} />
              </View>
            </View>
          </View>
          <View style={styles.eventDetailsContantContainer}>
            <Text style={[styles.eventDetailsTitle, appTitle]}>
              {event.get('title')}
            </Text>
            <Text style={[styles.eventDetails, styles.eventDetailsDoubleLine, appText]}>
              {event.get('details')}
            </Text>
          </View>
          <View style={styles.bottomButtonsContainer}>
            {addToCalendar}
          </View>
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    events: state.getIn(['events', 'items']),
    articles: state.getIn(['articles', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appSubtitle: getColor(state, 'general', 'subtitle'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    cardSubtitle: getColor(state, 'card', 'subtitle'),
    buttonBackground:  getBg(state, 'button', 'background'),
    buttonText:  getColor(state, 'button', 'text'),
  }),
)(EventDetails);
