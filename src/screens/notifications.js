import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  FlatList,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import NotificationCard from '../components/notificationCard';
import CommentBar from '../components/commentBar';

import styles from '../styles/notifications';

import moment from 'moment';

import { notificationsActions } from '../redux/actions';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Notifications extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      commentingId: null,
    };
  }

  componentWillMount() {
    const { fetchNotifications } = this.props;
    fetchNotifications();
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }

    newProps.notifications.forEach((n, id) => {
      if (!n.hasIn(['readers', newProps.userId])) {
        newProps.markRead(id);
        console.log('Mark read', id);
      }
    })


  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderItem(data, index) {
    const { commentingId } = this.state;
    const {
      setLikeFor,
      setDislikeFor,
    } = this.props
    const id = data.get('id');

    const commentMode = id === commentingId;
    return (
      <NotificationCard
        {...this.props}
        data={data}
        commentMode={commentMode}
        onPressLike={liked => liked ? setDislikeFor(id) : setLikeFor(id)}
        onPressComment={() => {
          if (!commentMode) {
            setTimeout(() =>this.list.scrollToIndex({
              index,
              viewOffset: 8,
            }), 100);
          }
          this.setState({ commentingId: commentMode ? null : id });
        }}
      />
    );
  }

  render() {
    const { commentingId } = this.state;
    const {
      notifications,
      sendComment,
      userId,
      profileImage,
      appBg,
      barStyle,
      navBarBgColor,
    } = this.props;
    const header = (
      <View style={styles.headerContainer}>
        <Text style={styles.headerText}>
          Notifications
        </Text>
      </View>
    );

    firebase.analytics().setCurrentScreen('notifications', 'notifications');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <FlatList
          style={appBg}
          ref={(ref) => { this.list = ref; }}
          ListHeaderComponent={header}
          ListFooterComponent={<View style={{height: 50}} />}
          data={notifications.toArray()}
          keyExtractor={item => item.get('id')}
          renderItem={({ item, index }) => this.renderItem(item, index)}
        />
        <CommentBar
          visible={commentingId != null}
          onSend={text => sendComment(commentingId, text, { userId, profileImage })}
        />
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    notifications: state
      .getIn(['notifications', 'items'])
      .filter(v => moment().isAfter(moment(v.get('date')))),
    userId: state.getIn(['auth', 'user', 'id']),
    profileImage: state.getIn(['auth', 'user', 'profileImage']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    fetchNotifications: () => notificationsActions.fetchNotifications(),
    setDislikeFor: id => notificationsActions.setDislikeFor(id),
    setLikeFor: id => notificationsActions.setLikeFor(id),
    sendComment: (id, text, userdata) => notificationsActions.sendComment(id, text, userdata),
    markRead: id => notificationsActions.markRead(id),
  },
)(Notifications);
