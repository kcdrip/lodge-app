import React, { Component } from 'react';

import {
  View,
  ScrollView,
  Text,
  TextInput,
  TouchableOpacity,
  StatusBar,
  Keyboard,
} from 'react-native';

import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Spinner from '../components/spinner';
import ErrorPopup from '../components/errorPopup';

import Logo from '../components/logo';

import { color } from '../styles/Theme';

import { authActions } from '../redux/actions';

import styles from '../styles/auth';

import { validateEmail } from '../utils/validate';
import { formatPhone } from '../utils/format';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Signup extends Component {
  static navigationOptions = () => ({
    headerMode: 'none',
    header: null,
  });

  constructor(props) {
    super(props);
    this.state = {
      forceCheck: false,
      keyboardIsVisible: false,
      name: '',
      email: '',
      phone: '',
      password: '',
      conPassword: '',
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      () => this.setState({ keyboardIsVisible: true }));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      () => this.setState({ keyboardIsVisible: false }));
  }

  componentWillReceiveProps(newProps) {
    const { props } = this;
    const {
      error,
      isBusy,
      dismissError,
      screenProps: {
        modal: {
          setContent,
          clearContent,
          setDismissHandler,
        },
      },
    } = newProps;

    if (props.error !== error || props.isBusy !== isBusy) {
      const modalContent =
        isBusy ? <Spinner text="One moment..." /> :
        error ? <ErrorPopup message={error} /> :
        null;

      if (modalContent) {
        setContent(modalContent, undefined, this.ref);
        setDismissHandler(() => dismissError());
      } else {
        clearContent();
      }
    }
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  handleBack() {
    const { props: { navigation } } = this;
    navigation.goBack();
  }

  handleRegister() {
    const { props, state } = this;
    const {
      name,
      email,
      phone,
      password,
    } = state;

    console.log('REG', {
      name,
      email,
      phone,
      password,
      point: 0,
    });

    props.register({
      name,
      email,
      phone,
      password,
      point: 0,
    });
  }

  handleFbLogin() {
    const { props } = this;
    Keyboard.dismiss();
    props.loginWithFacebook();
  }

  render() {
    const {
      keyboardIsVisible,
      forceCheck,
      name,
      email,
      phone,
      password,
      conPassword,
    } = this.state;

    const {
      appBg,
      barStyle,
      navBg,
      navColor,
    } = this.props;

    const nameLongEnough = name.length > 2;
    const emailValid = validateEmail(email);
    const phoneValid = phone.length > 5;
    const passwordLongEnough = password.length > 5;
    const passwordEqual = password === conPassword;

    const registerEnabled = nameLongEnough && emailValid && passwordLongEnough && passwordEqual;

    firebase.analytics().setCurrentScreen('signup', 'signup');

    return (

      <ScrollView
        ref={ref => this.ref = ref}
        contentContainerStyle={[styles.container, keyboardIsVisible ? styles.avoidKeyboard : null, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={(navBg || {}).backgroundColor}
          hidden={keyboardIsVisible}
        />
        <View style={[styles.createYourAccountHeaderContainer, navBg]}>
          {/* <Logo /> */}

          <View style={styles.screenTitleContainer}>
            {/* <Text style={[,styles.screenTitle, { color: navColor }]}>
              Create your account
            </Text> */}
            <Text style={styles.subTitle}>Create your account</Text>
            <TouchableOpacity
              style={styles.backButtonContainer}
              onPress={() => this.handleBack()}
            >
              <Icon
                name="arrow-left"
                size={25}
                color={navColor}
              />
            </TouchableOpacity>
          </View>
        </View>
        <TextInput
          style={[styles.input, forceCheck && !nameLongEnough ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          placeholder="Name"
          onChangeText={name => this.setState({ name })}
          autoCorrect={false}
          autoCapitalize="words"
          value={name}
        />
        <TextInput
          style={[styles.input, forceCheck && !emailValid ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          placeholder="Email"
          autoCapitalize="none"
          keyboardType="email-address"
          autoCorrect={false}
          onChangeText={email => this.setState({ email })}
          value={email}
        />
        <TextInput
          style={[styles.input]}
          placeholderTextColor={color.INPUT_TEXT}
          dataDetectorTypes="phoneNumber"
          placeholder="Phone (Optional)"
          autoCapitalize="none"
          keyboardType="phone-pad"
          autoCorrect={false}
          onChangeText={phone => this.setState({ phone })}
          onBlur={() => this.setState({ phone: phone != '' ? formatPhone(phone) : '' })}
          value={phone}
        />
        <TextInput
          style={[styles.input, forceCheck && !passwordLongEnough ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          placeholder="Password (6 Characters or More)"
          secureTextEntry
          onChangeText={password => this.setState({ password })}
          value={password}
        />
        <TextInput
          style={[
            styles.input,
            styles.mb_min,
            forceCheck && (conPassword.length > 0) && !passwordEqual ? styles.failedInput : null,
          ]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          placeholder="Confirm Password"
          secureTextEntry
          onChangeText={conPassword => this.setState({ conPassword, forceCheck: true })}
          value={conPassword}
        />
        <View style={styles.spacer} />
        <TouchableOpacity
          style={[
            styles.button,
            styles.buttonPrimary,
            registerEnabled ? null : styles.buttonDisabled,
          ]}
          onPress={() => registerEnabled ? this.handleRegister() : this.setState({ forceCheck: true })}
        >
          <Text style={styles.buttonPrimaryText}>
            CONTINUE
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={[styles.button, styles.buttonSecondary]}
          onPress={() => this.handleFbLogin()}
        >
          <Icon
            name="facebook"
            style={styles.facebookIcon}
            size={18}
            color="#2699FB"
          />
          <Text style={styles.buttonSecondaryText}>
            SIGN UP WITH FACEBOOK
          </Text>
        </TouchableOpacity>

        {/* <View style={{paddingTop: 30}} /> */}

        <TouchableOpacity
          style={[
            styles.button,
            styles.buttonPrimary,
          ]}
          onPress={() => this.handleBack()}
        >
          <Text style={styles.buttonPrimaryText}>
            LOGIN
          </Text>
        </TouchableOpacity>

      </ScrollView>

    );
  }
}

export default connect(
  state => ({
    error: state.getIn(['auth', 'error']),
    isBusy: state.getIn(['auth', 'isBusy']),
    navBg:  getBg(state, 'title', 'background'),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    navColor: state.getIn(['auth', 'theme', 'title', 'darkStatusBar']) ? 'black' : 'white',
  }),
  {
    register: data => authActions.register(data),
    loginWithFacebook: () => authActions.loginWithFacebook(),
    dismissError: () => authActions.dismissError(),
  },
)(Signup);
