import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  ScrollView,
  TouchableOpacity,
  Image,
  Text,
  View,
  StatusBar,
  Platform,
} from 'react-native';
// react redux connect
import { connect } from 'react-redux';
// style imports
import styles from '../styles/news';
// actions import
import { newsActions } from '../redux/actions';
// component imports
import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import NewsCard from '../components/newsCard';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import firebase from 'react-native-firebase';

// Oh, stewardess? I speak Jive.

// class declaration
class Newsletter extends Component {
  // set up navigation
  static navigationOptions = topNavigation;
  // fetch news and set navigation background color
  componentWillMount() {
    const { fetchNews } = this.props;
    fetchNews();
    this.updateNavColor(this.props.navBarBgColor);
  }
  // update navigation background color
  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }
  // set parameters
  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }
  render() {
    const {
      props: {
        appBg,
        barStyle,
        news,
        navBarBgColor,
        navigation: {
          navigate,
        },
      },
    } = this;

    /*
    let newsArray = [];
    for (let i = 0; i < news.size; ++i) {
      newsArray.push(news.get(i.toString()));
    }
    if (newsArray[0] == null) {
      newsArray = news;
    }
    */
    let newsArray = news.toArray();
    var newsList = newsArray.map((v, id) => (
      <NewsCard
        data={v}
        id={id}
        navigate={navigate}
      />)
    );

    firebase.analytics().setCurrentScreen('news', 'news');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView style={[styles.scrollView, appBg]}>
          <View style={styles.innerContainer}>
            { newsList }
          </View>
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    news: state.getIn(['news', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubtitle: getColor(state, 'card', 'subtitle'),
  }),
  {
    fetchNews: () => newsActions.fetchNews(),
  },
)(Newsletter);
