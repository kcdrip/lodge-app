import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  ScrollView,
  Image,
  TouchableOpacity,
  Platform,
  Linking,
} from 'react-native';

import { connect } from 'react-redux';

import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import OfferCard from '../components/offerCard';

import { getStaticStyle } from '../utils/maps';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import styles, { blueMapStyle } from '../styles/friends';

import { GOOGLE_MAPS_API, GOOGLE_MAPS_API_KEY } from '../config/constants';

const openCoords = ({lat, lng}) => {
  const link = Platform.OS === 'ios' ? 'http://maps.apple.com/?daddr=' : 'google.navigation:q=';
  Linking.openURL(`${link}${lat},${lng}`);
}

const blueMapStyleString = getStaticStyle(blueMapStyle);

const createIcon = (color = "#2699FB") => ({
  PHONE: (
    <FontAwesome
      name="phone"
      size={20}
      color={color}
    />
  ),
  MAIL: (
    <Entypo
      name="mail"
      size={20}
      color={color}
    />
  ),
  SITE: (
    <Entypo
      name="news"
      size={20}
      color={color}
    />
  ),
  PIN: (
    <Entypo
      name="location-pin"
      size={20}
      color={color}
    />
  ),
});


class FriendLocation extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      mapWidth: 0,
    }
  }

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderContact(icon, name, data, handler) {
    if (!data) return null;

    const {
      boxBg,
      boxTitle,
      boxSubtitle,
    } = this.props;

    return (
      <TouchableOpacity
        style={[styles.contact, boxBg]}
        onPress={() => handler(data)}
      >
        <View style={styles.contactIconContainer}>
          {icon}
        </View>
        <View style={styles.contactContentContainer}>
          <Text style={[styles.contactData, boxTitle]}>
            {data}
          </Text>
          <Text style={[styles.contactName, boxSubtitle]}>
            {name}
          </Text>
        </View>
        <View style={styles.contactArrowContainer}>
          <Entypo
            name="chevron-right"
            size={20}
            color={(boxSubtitle || {}).color || "#2699FB"}
          />
        </View>

      </TouchableOpacity>
    );

  }

  renderMap(coords) {
    const { mapWidth } = this.state;
    const mapHeight = 400;

    if (!coords || mapWidth === 0) return null;

    const options = {
      center: `${coords.lat},${coords.lng}`,
      zoom: 16,
      size: `${mapWidth}x${mapHeight}`,
      key: GOOGLE_MAPS_API_KEY,
    };

    const stringifiedOptions = Object.keys(options).map(key => `${key}=${options[key]}`);

    const optionsString = stringifiedOptions.join('&');

    const staticMapsApiUrl = `${GOOGLE_MAPS_API}?${optionsString}&${blueMapStyleString}`;

    return (
      <Image source={{
        uri: staticMapsApiUrl,
        width: mapWidth,
        height: mapHeight,
      }}
      />
    );
  }

  render() {
    const {
      props: {
        friends,
        appBg,
        cardBg,
        barStyle,
        appText,
        boxTitle,
        navBarBgColor,
        navigation: {
          state: {
            params: {
              id,
            },
          },
        },
      },
    } = this;

    const data = friends.get(id);

    if (!data) return <View />;

    const coords = data.has('coords') ? {
      lat: data.getIn(['coords', 'lat']),
      lng: data.getIn(['coords', 'lng']),
    } : null;

    const hasImage = data.has('image');

    const logo = hasImage ? (
      <Image
        style={[styles.logoImage, cardBg]}
        source={{ uri: data.get('image') }}
      />
    ) : (
      <Text style={styles.logoTitle}>
        {data.get('name')}
      </Text>
    );

    const ICON = createIcon((boxTitle || {}).color);

    firebase.analytics().setCurrentScreen('friend location', 'friend location');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView
          style={styles.friendScrollContainer}
          contentContainerStyle={styles.friendScrollContent}
        >
          <View style={styles.logoContainer}>
            {logo}
          </View>
          <OfferCard
            {...this.props}
            data={data}
          />
          <View style={styles.aboutContainer}>
            <Text style={[styles.aboutText, appText]}>
              {data.get('about')}
            </Text>
          </View>
          {this.renderContact(ICON.PHONE, 'Number', data.get('phone'), phone => Linking.openURL(`tel:${phone}`))}
          {this.renderContact(ICON.MAIL, 'Email', data.get('email'), email => Linking.openURL(`mailto:${email}`))}
          {this.renderContact(ICON.SITE, 'Website', data.get('site'), v => Linking.openURL(v))}
          {this.renderContact(ICON.PIN, 'Direction', data.get('location'), () => openCoords(coords))}
          <View
            style={styles.mapContainer}
            onLayout={({ nativeEvent: { layout: { width } } }) => this.setState({ mapWidth: width })} >
            {this.renderMap(coords)}
          </View>
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    friends: state.getIn(['friends', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    boxBg: getBg(state, 'box', 'background'),
    boxTitle: getColor(state, 'box', 'title'),
    boxSubtitle: getColor(state, 'box', 'subtitle'),
  }),
  {
    fetchFriends: () => friendsActions.fetchFriends(),
  },
)(FriendLocation);
