import React, { Component } from 'react';
import {
  Text,
  View,
  StatusBar,
  TouchableOpacity,
  ScrollView,
  Share,
  AsyncStorage,
  Platform,
} from 'react-native';

import Icon from 'react-native-vector-icons/FontAwesome';

import { connect } from 'react-redux';

import moment from 'moment';

import Immutable from 'immutable';

import { eventsActions } from '../redux/actions';

import { Agenda } from '../components/RNCalendars';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import { addToCalendar } from '../utils/events';

import styles from '../styles/calendar';
import commonStyles from '../styles/common';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

import { getTimeFrame } from '../utils/selectors';

class CalendarScreen extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      date: null,
      showTutorial: false,
    }
  }

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
    this.props.fetchSpecials();
    this.props.fetchEvents();

    const {
      props: { screenProps: { modal } },
    } = this;

    AsyncStorage.getItem('tutorial_calendar').then(data => this.setState({showTutorial: data !== 'watched'}));
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    console.log('Art up col', color);
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  shareEvent(event) {
    const {
      props: { screenProps: { modal } },
    } = this;

    modal.setContent(<View />, 'light', this.ref);

    Share.share({
      title: event.get('title'),
      message: event.get('description') || event.get('content') || event.get('details') || ' ',
    }).then(() => modal.clearContent());
  }

  handleAddToCalendar(event) {

    const {
      props: { screenProps: { modal } },
    } = this;


    const toCalDate = v => `${moment(v).format(moment.HTML5_FMT.DATETIME_LOCAL_MS)}Z`;
    const toTime = v => moment.utc(v, 'HH:mmA').format('HH:mm');

    const startTime = event.get('from');
    const endTime = event.get('to');

    const allDay = startTime == null && endTime == null;

    let startDate;
    let endDate;

    if (allDay) {
      startDate = toCalDate(event.get('date'));
      endDate = startDate;
    } else {
      const date = moment(event.get('date')).format('YYYY-MM-DD');
      startDate = toCalDate(`${date} ${toTime(startTime)}`);
      endDate = toCalDate(`${date} ${toTime(endTime)}`);
    }

    const id = event.get('id');

    console.log('addToCalendar cal', {
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    });

    addToCalendar({
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    }).then(v =>
      modal.setContent(
        <Text style={{color: 'white', fontSize: 20}}>
          Event "{event.get('title')}" added to calendar
        </Text>,
        undefined,
        this.ref,
      ));

  }

  showEventModal(event, calendarShare = false) {
    const {
      props: {
        buttonBg,
        buttonText,
        title,
        subtitle,
        text,
        screenProps: { modal }
      },
    } = this;

    const timeFrame = getTimeFrame(event);
    const subTitle = timeFrame ? `(${timeFrame})` : moment(event.get('date')).format('l');

    const addToCalendar = calendarShare ? (
      <TouchableOpacity
        onPress={() => this.handleAddToCalendar(event)}
        style={[commonStyles.modalButton, commonStyles.modalButtonCalendar, buttonBg]}
      >
        <Text style={[commonStyles.modalButtonText, buttonText]}>
          ADD TO CALENDAR
        </Text>
      </TouchableOpacity>
    ) : null;

    modal.setContent((
      <View style={styles.modalContainer}>
        <View style={styles.modal}>
          <Text style={[styles.modalTitle, title]}>
            {event.get('title')}
          </Text>
          <Text style={[styles.modalSubTitle, subtitle]}>
            {subTitle}
          </Text>
          <Text style={[styles.modalText, text]}>
            {event.get('description') || event.get('content') || event.get('details')}
          </Text>
          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              style={[commonStyles.modalButton, commonStyles.modalButtonShare, buttonBg]}
              onPress={() => this.shareEvent(event)}
            >
              <Text style={[commonStyles.modalButtonText, buttonText]}>
                SHARE
              </Text>
            </TouchableOpacity>
            {addToCalendar}
          </View>
        </View>
      </View>
    ), 'light', this.ref);
  }

  renderEvent(event, id) {
    const featured = event.get('isFeatured') || false;

    const star = featured ? (
      <Icon
        name="star"
        size={18}
        color="white"
      />
    ) : null;

    return (
      <View
        key={id}
        style={styles.eventContainer}
      >
        <View style={styles.eventTimeContainer}>
          <Text style={styles.eventTime}>
            {event.get('from')}
          </Text>
        </View>
        <View style={styles.eventCircle} />
        <TouchableOpacity
          style={[styles.dataBox, styles.eventBox, featured ? styles.featuredEventBox : null]}
          onPress={() => this.showEventModal(event, true)}
        >
          <Text style={[styles.eventTitle , featured ? styles.featuredEventTitle : null]}>
            {event.get('title')}
          </Text>
          {star}
        </TouchableOpacity>
      </View>
    );
  }

  renderAgenda() {
    const {
      props: {
        specials,
        events,
      },
      state: {
        date,
      }
    } = this;

    if (date == null) return null;


    const parseTime = v => v.has('from') && v.get('from') !== '' ? moment.utc(v.get('from'), 'HH:mmA') : null;

    const dayEvents = events
      .filter(v => moment(v.get('date')).format('YYYY-MM-DD') === date)
      .sort((a, b) => {
        const ma = parseTime(a);
        const mb = parseTime(b);
        return ma == null && mb == null ? 0 :
          ma == null ? -1 :
          mb == null ? 1 :
          ma.diff(mb);
        });

    const eventsBlock = dayEvents
      .map((v, id) => this.renderEvent(v, id))
      .toArray();

    const dailySpecial = specials.get(moment(date, 'YYYY-MM-DD').isoWeekday() - 1);

    const dailySpecialBlock = dailySpecial ? (
      <TouchableOpacity
        onPress={() => this.showEventModal(dailySpecial, false)}
        style={[styles.dataBox, styles.dailySpecialBox]}
      >
        <Text style={styles.dailySpecialTitle}>
          { dailySpecial.get('title') || 'Daily Special:' }
           {/*Date: {date}, Weekday: {moment(date, 'YYYY-MM-DD').isoWeekday()}, IsoWeekDay: {moment(date, 'YYYY-MM-DD').weekday()}*/}
        </Text>
        <Text style={styles.dailySpecialText}>
          {dailySpecial.get('content')}
        </Text>
      </TouchableOpacity>
    ) : null;

    return (
      <ScrollView style={styles.agendaContainer}>
        { dailySpecialBlock }
        { eventsBlock }
      </ScrollView>
    );
  }

  renderTutorial() {
    return (
      <TouchableOpacity
        onPress={() => {
          this.setState({showTutorial: false});
          AsyncStorage.setItem('tutorial_calendar', 'watched');
        }}
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
          backgroundColor: '#000c',
          padding: 125,
          flexDirection: 'column',
          alignItems: 'center'
        }}
      >
      <View style={{
        backgroundColor: 'white',
        width: 40,
        height: 10,
        borderRadius: 5,
        marginBottom: 30,
      }}/>
         <Icon
          name="arrow-up"
          size={60}
          color="#ccc"
        />
        <View style={{width: 250, marginTop: 30}}>
        <Text style={{color: '#ccc', fontSize: 25, textAlign: 'center', fontWeight: '900'}}>
          Swipe the handler down to see the calendar
        </Text>

        </View>


      </TouchableOpacity>
    );
  }

  render() {
    const {
      props: {
        events,
        barStyle,
        appBg,
        navBarBgColor,
      },
      state: {
        showTutorial,
      },
    } = this;

    const markedDates = events.filter(v => v.get('isFeatured')).reduce(
      (p, v) => ({
        ...p,
        [moment(v.get('date')).format('YYYY-MM-DD')]: { marked: true }
      }),
      {}
    );

    firebase.analytics().setCurrentScreen('calendar', 'calendar');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <Agenda
          onDayPress={day => {
            console.log('day1', day, day.dateString);
            this.setState({date: day.dateString})
          }}
          pastScrollRange={Platform.OS === 'ios' ? 0 : 2}
          futureScrollRange={6}
          rowHasChanged={(r1, r2) => r1.text !== r2.text}
          markedDates={markedDates}
          refreshing={false}
          refreshControl={null}
          theme={{
            'stylesheet.calendar.header': {
              week: {
                marginTop: 5,
                flexDirection: 'row',
                justifyContent: 'space-between'
              }
            },
          }}

          agenda={this.renderAgenda()}
        />
        <BottomNavigation {...this.props} />
       {showTutorial ? this.renderTutorial() : null}
      </View>
    );
  }
}

export default connect(
  state => ({
    events: state.getIn(['events', 'items']),
    specials: state.getIn(['events', 'specials']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    buttonText: getColor(state, 'button', 'text'),
    buttonBg:  getBg(state, 'button', 'background'),
    title: getColor(state, 'general', 'title'),
    text: getColor(state, 'general', 'text'),
    subtitle: getColor(state, 'general', 'subtitle'),
  }),
  {
    fetchEvents: () => eventsActions.fetchEvents(),
    fetchSpecials: () => eventsActions.fetchSpecials(),
  },
)(CalendarScreen);
