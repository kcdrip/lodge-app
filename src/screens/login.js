import React, { Component } from 'react';
import {
  View,
  ScrollView,
  Text,
  TouchableOpacity,
  StatusBar,
  TextInput,
  Keyboard,
} from 'react-native';

import { connect } from 'react-redux';

import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../components/logo';
import Spinner from '../components/spinner';
import ErrorPopup from '../components/errorPopup';

import { validateEmail } from '../utils/validate';

import styles from '../styles/auth';
import { color } from '../styles/Theme';

import { authActions, eventsActions, articlesActions, notificationsActions, friendsActions } from '../redux/actions';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Login extends Component {
  static navigationOptions = () => ({
    headerMode: 'none',
    header: null,
  });

  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      keyboardIsVisible: false,
      forceCheckEmail: false,
      forceCheckPassword: false,
      showForgotBlock: false,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      () => this.setState({ keyboardIsVisible: true }));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      () => this.setState({ keyboardIsVisible: false }));
  }


   componentWillMount() {
    const {
      fetchEvents,
      fetchArticles,
      fetchNotifications,
      fetchFriends,
    } = this.props;

    fetchEvents();
    setTimeout(() => fetchArticles(), 3000);
    setTimeout(() => fetchNotifications(), 5000);
    setTimeout(() => fetchFriends(), 7000);
  }

  componentWillReceiveProps(newProps) {
    const { props } = this;
    const {
      error,
      isBusy,
      dismissError,
      screenProps: {
        modal: {
          setContent,
          clearContent,
          setDismissHandler,
        },
      },
    } = newProps;

    if (props.error !== error || props.isBusy !== isBusy) {
      const modalContent =
        isBusy ? <Spinner text='One moment...' /> :
        error ? <ErrorPopup message={error} /> :
        null;

      if (modalContent) {
        setContent(modalContent, undefined, this.ref);
        setDismissHandler(() => {
          this.setState({ showForgotBlock: true })
          dismissError();
        });
      } else {
        clearContent();
      }
    }
  }

  handleLogin() {
    Keyboard.dismiss();

    this.props.login({
      email: this.state.email,
      password: this.state.password,
    });
  }

  handleFbLogin() {
    Keyboard.dismiss();
    this.props.loginWithFacebook();
  }

  render() {
    const {
      keyboardIsVisible,
      email,
      password,
      forceCheckEmail,
      forceCheckPassword,
      showForgotBlock,
    } = this.state;

    const {
      isBusy,
      error,
      restorePassword,
      appBg,
      barStyle,
      navBg,
      navigation: {
        navigate
      }
    } = this.props;

    const emailValid = validateEmail(email);
    const passwordValid = password.length > 5;
    const loginEnabled = email !== '' && emailValid && passwordValid;

    const forgotPasswordBlock = showForgotBlock ? (
      <TouchableOpacity
        onPress={() => restorePassword(email)}
        style={{marginTop: -20, marginBottom: 10}}
      >
        <Text style={{color: '#777'}}>
          Forgot your password?
        </Text>
      </TouchableOpacity>
    ): null;

    firebase.analytics().setCurrentScreen('login', 'login');

    return (
        <ScrollView
          ref={ref => this.ref = ref}
          contentContainerStyle={[styles.container, keyboardIsVisible ? styles.avoidKeyboard : null, appBg]}
        >
          <StatusBar
            barStyle={barStyle}
            backgroundColor={(navBg || {}).backgroundColor}
          />
          <View nestedScrollEnabled={true} style={[, styles.logoContainer, navBg]}>
            <Logo />
          </View>
          <Text style={{color: '#000',paddingBottom: 15, textAlign: 'center',}}>
          New to the App? Please press the "Sign Up" button at the bottom of the page and create an account!
        </Text>
          <TextInput
            style={[styles.input, forceCheckEmail && !emailValid ? styles.failedInput : null]}
            autoCorrect={false}
            placeholder="Email"
            onBlur={() => this.setState({ forceCheckEmail: true })}
            autoCapitalize="none"
            keyboardType="email-address"
            placeholderTextColor={color.INPUT_TEXT}
            onChangeText={email => this.setState({ email })}
            value={email}
          />
          <TextInput
            style={[styles.input, forceCheckPassword && !passwordValid ? styles.failedInput : null]}
            autoCorrect={false}
            placeholder="Password"
            onBlur={() => this.setState({ forceCheckPassword: true })}
            placeholderTextColor={color.INPUT_TEXT}
            secureTextEntry
            onChangeText={password => this.setState({ password })}
            value={password}
          />
          {forgotPasswordBlock}
          <TouchableOpacity
            style={[styles.button, styles.buttonPrimary, loginEnabled ? null : styles.buttonDisabled]}
            onPress={() => (loginEnabled ? this.handleLogin() : this.setState({
              forceCheckEmail: true,
              forceCheckPassword: true,
            }))}
          >
            <Text style={styles.buttonPrimaryText}>
              LOGIN
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[styles.button, styles.buttonSecondary]}
            onPress={() => this.handleFbLogin()}
          >
            <Icon
              name="facebook"
              style={styles.facebookIcon}
              size={18}
              color="#2699FB"
            />
            <Text style={styles.buttonSecondaryText}>
              LOGIN WITH FACEBOOK
            </Text>
          </TouchableOpacity>
          <View style={[, styles.spacer]} />
          <TouchableOpacity
            style={[styles.button, styles.buttonPrimary]}
            onPress={() => navigate('SignUp')}
          >
            <Text style={styles.buttonPrimaryText}>
              SIGN UP
            </Text>
          </TouchableOpacity>
        </ScrollView>

    );
  }
}

export default connect(
  state => ({
    error: state.getIn(['auth', 'error']),
    isBusy: state.getIn(['auth', 'isBusy']),
    articles: state.getIn(['articles', 'items']),
    navBg:  getBg(state, 'title', 'background'),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    login: data => authActions.login(data),
    loginWithFacebook: () => authActions.loginWithFacebook(),
    restorePassword: email => authActions.restorePassword(email),
    dismissError: () => authActions.dismissError(),
    fetchEvents: () => eventsActions.fetchEvents(),
    fetchArticles: () => articlesActions.fetchArticles(),
    fetchNotifications: () => notificationsActions.fetchNotifications(),
    fetchFriends: () => friendsActions.fetchFriends(),
  },
)(Login);
