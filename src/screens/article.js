import React, { Component } from 'react';
import {
  Dimensions,
  View,
  StatusBar,
  Text,
  ScrollView,
  TouchableOpacity,
  Image,
  Linking,
  StyleSheet,
} from 'react-native';

import { connect } from 'react-redux';

//import HTMLView from 'react-native-htmlview';
import HTMLView from 'react-native-render-html';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import styles from '../styles/articles';
//import htmlStyles from '../styles/htmlStyles';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Article extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderButton(title, onPress) {
    return (
      <TouchableOpacity
        style={styles.contactButton}
        onPress={() => onPress()}
      >
        <Text style={styles.contactButtonText}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }

  render() {
    const {
      props: {
        barStyle,
        appBg,
        appTitle,
        appText,
        cardBg,
        articles,
        navBarBgColor,
        navigation: {
          state: {
            params: {
              id,
            },
          },
        },
      },
    } = this;

    const data = articles.get(id);

    if (!data) return <View />;

    const mailButton = data.has('email') ? this.renderButton('Email', Linking.openURL(`mailto:${data.get('email')}`)) : null;
    const phoneButton = data.has('phone') ? this.renderButton('Call', Linking.openURL(`tel:${data.get('phone')}`)) : null;

    const contactBlock = data.has('email') || data.has('phone') ? (
      <View style={styles.contactBlock}>
        {mailButton}
        {phoneButton}
      </View>
    ) : null;

    const htmlStyles = StyleSheet.create({
      newsletter: {
        color: '#2699FB',
        fontFamily: 'Arial',
        fontSize: 14,
        lineHeight: 18.5,
        //paddingBottom: 50,
        ...(appText || {}),
      },
      h1: {
        fontSize: 26,
        fontWeight: '600',
      },
      h2: {
        fontSize: 20,
        fontWeight: '600',
      },
      a: {
        fontWeight: '300',
        color: '#FF3366', // make links coloured pink
      },
      gray: {
        color: '#6E6565',
        fontWeight: 'bold',
      },
      div: {
      },
    });

    const html = (data.get('content') || '').replace(new RegExp('<p><br></p>', 'g'), '<br>');

    firebase.analytics().setCurrentScreen('article', 'article');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView style={[styles.articleDetailsContainer, appBg]}>
          <View style={[styles.detailsImageContainer, cardBg]}>
            <Image
              style={{ width: '100%', height: 180 }}
              source={{ uri: data.get('image') }}
            />
          </View>
          <View style={[styles.articleDetailsContentContainer, ]}>
            <Text style={[styles.articleDetailsTitle, appTitle, ]}>
              {data.get('title')}
            </Text>
            {contactBlock}
            {/* <HTMLView
              style={styles.articleMainText}
              stylesheet={htmlStyles}
              paragraphBreak={'\n'}
              value={`<newsletter>${html}</newsletter>`}
            /> */}
            <HTMLView
              //tagSyles={'<p>'={textAlign: 'right'}}
              imagesMaxWidth={Dimensions.get('window').width -5}
              html={`<newsletter>${html}</newsletter>`}
            />
          </View>
        </ScrollView>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    articles: state.getIn(['articles', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
)(Article);
