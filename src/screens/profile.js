import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  Image,
  Platform,
} from 'react-native';

import { Map } from 'immutable';
import IonIcon from 'react-native-vector-icons/Ionicons';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';

import ImagePicker from 'react-native-image-crop-picker';

import { formatPhone, formatDate } from '../utils/format';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';
import TwoOptionsDialog from '../components/twoOptionsDialog';

import { authActions } from '../redux/actions';

import styles from '../styles/profile';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

const PROFILE_IMAGE_PARAMS = {
  width: 200,
  height: 200,
  cropping: true,
  cropperCircleOverlay: true,
};

class Profile extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  handleChangePhoto() {
    const { props: { screenProps: { modal } } } = this;
    modal.setContent(
      <TwoOptionsDialog
        {...this.props}
        title="CHOOSE PROFILE IMAGE"
        buttonLeftText="GALLERY"
        buttonRightText="CAMERA"
        buttonLeftAction={() => this.openGallery()}
        buttonRightAction={() => this.openCamera()}
      />,
      undefined,
      this.ref
    );
  }

  closeModal() {
    const { props: { screenProps: { modal } } } = this;
    setTimeout(() => modal.clearContent(), 500);
  }

  openGallery() {
    const { props: { id, uploadImage } } = this;
    this.closeModal();

    ImagePicker.openPicker(PROFILE_IMAGE_PARAMS)
      .then(image => uploadImage(image.path, id));
  }

  openCamera() {
    const { props: { id, uploadImage } } = this;
    this.closeModal();

    ImagePicker.openCamera(PROFILE_IMAGE_PARAMS)
      .then(image => uploadImage(image.path, id));
  }

  render() {
    const {
      user,
      appBg,
      barStyle,
      cardBg,
      appText,
      navBarBgColor,
      buttonBg,
      buttonText,
      navigation: {
        navigate,
      },
      logout,
    } = this.props;

    if (!user) {
      return <View />;
    }

    const profileImageUri = user.get('profileImage');
    const phone = user.get('phone');
    const birthdate = user.get('birthdate');


    const profileImage = profileImageUri ? (
      <Image
        source={{ uri: profileImageUri }}
        style={styles.profileImage}
      />
    ) : (
      <IonIcon
        name="md-person"
        size={100}
        color="#7FC4FD"
      />
    );

    const renderProfileData = text => (
      <Text style={[styles.textProfileData, appText]}>
        {text}
      </Text>
    );

    const iconColor = (appText || {}).color || '#2699FB';

    firebase.analytics().setCurrentScreen('profile', 'profile');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <View style={[styles.profileImageContainer, cardBg]}>
          {profileImage}
          <TouchableOpacity
            style={styles.changePhotoButton}
            onPress={() => this.handleChangePhoto()}
          >
            <Text style={styles.buttonText}>
              Change Photo
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.profileDataContainer}>
          <View>
            <Text style={[styles.textName, appText]}>
              {user.get('name')}
            </Text>
            { renderProfileData(user.get('email')) }
            { renderProfileData(formatPhone(phone)) }
            { birthdate ? renderProfileData(`Birthday ${formatDate(birthdate)}`) : null }
          </View>
          <TouchableOpacity
            style={[styles.profileEditButton, buttonBg]}
            onPress={() => navigate('EditProfile')}
          >
            <Text style={[styles.buttonText, buttonText]}>
              EDIT
            </Text>
          </TouchableOpacity>
        </View>
        <View style={styles.actionContainer}>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => navigate('ChangePassword')}
          >
            <MaterialIcon
              name="settings"
              size={20}
              color={iconColor}
            />
            <Text style={[styles.actionText, appText]}>
              Change Password
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => navigate('Support')}
          >
            <Entypo
              name="help-with-circle"
              size={20}
              color={iconColor}
            />
           <Text style={[styles.actionText, appText]}>
              Support
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.actionButton}
            onPress={() => logout()}
          >
            <Entypo
              name="log-out"
              size={20}
              color={iconColor}
            />
            <Text style={[styles.actionText, appText]}>
              Log Out
            </Text>
          </TouchableOpacity>
        </View>
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    user: state.getIn(['auth', 'user']) || Map(),
    id: state.getIn(['auth', 'user', 'id']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
    buttonBg:  getBg(state, 'button', 'background'),
    buttonText: getColor(state, 'button', 'text'),
  }),
  {
    logout: data => authActions.logout(data),
    uploadImage: (image, id) => authActions.uploadImage(image, id),
  },
)(Profile);
