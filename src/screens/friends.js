import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  FlatList,
  Image,
  Platform,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import { connect } from 'react-redux';

import { friendsActions } from '../redux/actions';

import styles from '../styles/friends';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Friends extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    const { fetchFriends } = this.props;
    this.updateNavColor(this.props.navBarBgColor);
    fetchFriends();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  renderItem(item) {
    const {
      cardBg,
      cardTitle,
      navigation: {
        navigate,
      },
    } = this.props;

    const hasImage = item.has('image');

    const image = hasImage ? (
      <Image
        style={[styles.friendCardImage, cardBg]}
        source={{ uri: item.get('image') }}
      />
    ) : null;

    return (
      <TouchableOpacity
        style={[styles.friendCard, cardBg]}
        onPress={() => navigate('FriendLocation', { id: item.get('id') })}
      >
        <View style={styles.headerContainer}>
          {image}
        </View>
        <View style={styles.cardFooter}>
          <Text style={[styles.placeName, cardTitle]}>
            {item.get('name')}
          </Text>
          <Icon
            name="md-arrow-forward"
            size={24}
            color={(cardTitle || {}).color || '#2699FB'}
          />
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {
      friends,
      appBg,
      barStyle,
      navBarBgColor,
    } = this.props;

    firebase.analytics().setCurrentScreen('friends', 'friends');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <FlatList
          style={appBg}
          ref={(ref) => { this.list = ref; }}
          data={friends.toArray()}
          keyExtractor={item => item.get('id')}
          renderItem={({ item, index }) => this.renderItem(item, index)}
          ListFooterComponent={<View style={{height: 20}} />}
        />
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}

export default connect(
  state => ({
    friends: state.getIn(['friends', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
  }),
  {
    fetchFriends: () => friendsActions.fetchFriends(),
  },
)(Friends);
