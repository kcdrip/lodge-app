import React, { Component } from 'react';

import PropTypes from 'prop-types';

import {
  View,
  Text,
  TextInput,
  TouchableOpacity,
  Keyboard,
  StatusBar,
  DatePickerIOS,
  DatePickerAndroid,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';
import moment from 'moment';

import { color } from '../styles/Theme';

import { authActions } from '../redux/actions';

import styles from '../styles/auth';

import { validateEmail } from '../utils/validate';
import { formatPhone, formatDate } from '../utils/format';

import topNavigation from '../components/topNavigation';

import firebase from 'react-native-firebase';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class EditProfile extends Component {
  static navigationOptions = topNavigation;

  constructor(props) {
    super(props);
    this.state = {
      forceCheck: false,
      keyboardIsVisible: false,
      name: props.name,
      email: props.email,
      phone: props.phone,
      birthdate: props.birthdate ? new Date(props.birthdate) : null,
    };
  }

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow',
      () => this.setState({ keyboardIsVisible: true }));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide',
      () => this.setState({ keyboardIsVisible: false }));
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  handleBack() {
    const { navigation } = this.props;
    navigation.goBack();
  }

  handleSave() {
    const { props, state } = this;
    const newData = {};

    if (props.name !== state.name) {
      newData.name = state.name;
    }

    if (props.email !== state.email) {
      if (!validateEmail(state.email)) {
        this.setState({ forceCheck: true });
        return;
      }
      newData.email = state.email;
    }

    if (props.phone !== state.phone) {
      newData.phone = formatPhone(state.phone);
    }

    const formattedBirthdate = formatDate(state.birthdate);

    if (state.birthdate && props.phone !== formattedBirthdate) {
      newData.birthdate = formattedBirthdate;
    }

    props.updateProfile(newData, props.id);
    props.navigation.navigate('Profile');
  }

  handleEditBirthday() {
    const { state } = this;

    const {
      screenProps: {
        modal: {
          setContent,
          clearContent,
        },
      },
    } = this.props;

    const maximumDate = moment().subtract(14, 'years').toDate();
    const defaultDate = state.birthdate || moment().subtract(30, 'years').toDate();

    Keyboard.dismiss();

    // TODO: iOS, add android support

    setContent(Platform.OS === 'ios' ? (
      <View style={styles.datePickerContainer}>
        <DatePickerIOS
          mode="date"
          date={defaultDate}
          maximumDate={maximumDate}
          onDateChange={birthdate => this.setState({ birthdate })}
        />
        <View style={styles.datePickerButtonContainer}>
          <TouchableOpacity
            style={[styles.button, styles.buttonPrimary]}
            onPress={() => clearContent()}
          >
            <Text style={styles.buttonPrimaryText}>
              CONFIRM
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    ) : ( <View style={styles.datePickerContainer} /> ), 'light', this.ref);

    if (Platform.OS === 'android') {
      DatePickerAndroid.open({ date: defaultDate })
        .then((x) => {
          const {action, year, month, day} = x;
          console.log("X", x);
          if (action !== DatePickerAndroid.dismissedAction) {
            this.setState({ birthdate: moment([year, month, day]).toDate() });
          }
          clearContent();
        });
    }
  }

  render() {
    const {
      keyboardIsVisible,
      forceCheck,
      name,
      email,
      phone,
      birthdate,
    } = this.state;

    const {
      appBg,
      barStyle,
      appTitle,
      navBarBgColor,
    } = this.props;

    const nameLongEnough = name.length > 2;
    const emailValid = validateEmail(email);

    const title = keyboardIsVisible ? null : (
      <Text style={[styles.editorScreenTitle, appTitle]}>
        Edit Account Info
      </Text>
    );

    firebase.analytics().setCurrentScreen('edit profile', 'edit profile');

    return (
      <View
        ref={ref => this.ref = ref}
        style={[styles.container, styles.centeredContainer, appBg]}
      >
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        {title}
        <TextInput
          style={[styles.input, forceCheck && !nameLongEnough ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          placeholder="Name"
          onChangeText={name => this.setState({ name })}
          autoCorrect={false}
          autoCapitalize="words"
          value={name}
        />
        <TextInput
          style={[styles.input, forceCheck && !emailValid ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          placeholder="Email"
          autoCapitalize="none"
          keyboardType="email-address"
          autoCorrect={false}
          onChangeText={email => this.setState({ email })}
          value={email}
        />
        <TextInput
          style={[styles.input]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize="none"
          dataDetectorTypes="phoneNumber"
          placeholder="Phone"
          keyboardType="phone-pad"
          onBlur={() => this.setState({ phone: formatPhone(phone) })}
          onChangeText={phone => this.setState({ phone })}
          value={phone}
        />
        <TouchableOpacity
          style={[styles.input, styles.mb_min]}
          onPress={() => this.handleEditBirthday()}
        >
          <Text style={{ color: color.INPUT_TEXT }}>
            {`Birthday ${formatDate(birthdate)}`}
          </Text>
        </TouchableOpacity>
        <View style={styles.spacer} />
        <TouchableOpacity
          style={[styles.button, styles.buttonPrimary]}
          onPress={() => this.handleSave() }
        >
          <Text style={styles.buttonPrimaryText}>
            SAVE
          </Text>
        </TouchableOpacity>
        <View style={{ height: keyboardIsVisible ? 200 : 0 }} />
      </View>
    );
  }
}

EditProfile.propTypes = {
  id: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  phone: PropTypes.string.isRequired,
  birthdate: PropTypes.string.isRequired,
  updateProfile: PropTypes.func.isRequired,
  screenProps: PropTypes.object.isRequired,
  navigation: PropTypes.shape({
    goBack: PropTypes.func.isRequired,
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

export default connect(
  state => ({
    id: state.getIn(['auth', 'user', 'id']),
    name: state.getIn(['auth', 'user', 'name']) || '',
    email: state.getIn(['auth', 'user', 'email']) || '',
    phone: state.getIn(['auth', 'user', 'phone']),
    birthdate: state.getIn(['auth', 'user', 'birthdate']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    appTitle: getColor(state, 'general', 'title'),
    appText: getColor(state, 'general', 'text'),
    cardBg: getBg(state, 'card', 'background'),
  }),
  {
    updateProfile: (data, id) => authActions.updateProfile(data, id),
  },
)(EditProfile);
