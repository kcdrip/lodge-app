import React, { Component } from 'react';
import {
  WebView,
  View,
  StatusBar,
  Platform,
} from 'react-native';

import { connect } from 'react-redux';

import topNavigation from '../components/topNavigation';
import BottomNavigation from '../components/bottomNavigation';

import styles from '../styles/payDues';

import firebase from 'react-native-firebase';

import {
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Link extends Component {
  static navigationOptions = topNavigation;

  componentWillMount() {
    this.updateNavColor(this.props.navBarBgColor);
  }

  componentWillReceiveProps(newProps) {
    if (newProps.navBarBgColor != this.props.navBarBgColor) {
      this.updateNavColor(newProps.navBarBgColor);
    }
  }

  updateNavColor(color) {
    if (color) {
      this.props.navigation.setParams({ color });
    }
  }

  render() {
    const {
      props: {
        links,
        appBg,
        barStyle,
        navBarBgColor,
        navigation: {
          state: {
            params: {
              id,
            },
          },
        },
      },
    } = this;

    const uri = links.getIn([id, 'link']);

    firebase.analytics().setCurrentScreen('link', 'link');

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <WebView
          source={{ uri }}
          style={[styles.webView, appBg]}
        />
        <BottomNavigation {...this.props} />
      </View>
    );
  }
}


export default connect(
  state => ({
    links: state.getIn(['auth', 'settings', 'links']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
  }),
)(Link);
