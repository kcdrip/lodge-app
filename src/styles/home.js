import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
    marginBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
  },
  innerContainer: {
    paddingHorizontal: 20,
    paddingTop: 10,
    flex: 1,
    justifyContent: 'space-around',
  },
  sectionHeader: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 5,
  },
  button: {
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2699FB',
    height: 21,
    paddingHorizontal: 10,
  },
  buttonText: {
    color: 'white',
    fontSize: 8,
    fontWeight: 'bold',
    fontFamily: 'arial',
  },
  title: {
    color: '#767676',
    fontSize: 23,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
  },
  titleEvents: {
    color: '#767676',
    fontSize: 18,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
  },
  eventsSection: {
    marginBottom: 15,
  },
  day: {
    color: '#767676',
    fontSize: 16,
    fontFamily: 'Lato-Bold',
    fontWeight: 'bold',
  },
  topEventCard: {
    flex: 1,
    height: 160,
    backgroundColor: '#D1D9DF',
    alignItems: 'center',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  topEventCardImage: {
    position: 'absolute',
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
  },
  topEventCardTitle: {
    color: 'white',
    textAlign: 'center',
    fontSize: 29,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
     textShadowOffset: {
      width: 2,
      height: 2,
    },
    textShadowRadius: 3,
    textShadowColor: '#333',
  },
  topEventCardTime: {
    color: 'white',
    fontSize: 17,
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
     textShadowOffset: {
      width: 2,
      height: 2,
    },
    textShadowRadius: 3,
    textShadowColor: '#333',
  },
  eventCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    height: 57,
    backgroundColor: '#F1F9FF',
    alignItems: 'center',
    marginTop: 10,
    paddingHorizontal: 15,
  },
  eventCardTitle: {
    textTransform: 'uppercase',
    fontSize: 18,
    color: '#2699FB',
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
    marginHorizontal: 15,
    flex: 1,
  },
  notificationRow: {
    flexDirection: 'row',
    marginTop: 15,
  },
  footer: {
    width: '100%',
    height: 15,
  },
});
