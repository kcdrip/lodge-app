import { StyleSheet } from 'react-native';

import { 
  color, 
  fontSize, 
  SIZE
} from './Theme';

const rounderElement = {
  borderRadius: SIZE.INPUT_HEIGHT / 2, 
  height: SIZE.INPUT_HEIGHT,
  width: '80%'
}

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#ffffff',
  },
  profileImageContainer: {
    position: 'relative', 
    height: 136, 
    width: '100%', 
    backgroundColor: '#BCE0FD', 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  profileImage: {
    width: SIZE.PROFILE_IMAGE_SIZE, 
    height: SIZE.PROFILE_IMAGE_SIZE, 
    backgroundColor: '#DDD', 
    borderRadius: SIZE.PROFILE_IMAGE_SIZE / 2,
  },
  changePhotoButton: {
    position: 'absolute', 
    right: 33, 
    bottom: 19, 
    backgroundColor: '#7F7F7F', 
    width: 82, 
    height: 25,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 6,
  },
  buttonText: {
    color: 'white', 
    fontSize: 10,
    fontFamily: 'arial',
    fontWeight: 'bold'
  },
  profileDataContainer: {
    height: 160, 
    paddingLeft: 32,
    paddingTop: 25,
    backgroundColor: '#F1F9FF', 
    flexDirection: 'row', 
    //justifyContent: 'space-around', 
    alignItems: 'flex-start'
  },
  profileEditButton: {
    width: 96, 
    height: 40, 
    position: 'absolute', 
    right: 25, 
    top: 28, 
    backgroundColor: '#2699FB', 
    borderRadius: 4, 
    justifyContent: 'center', 
    alignItems: 'center'
  },
  textName: {
    color: '#2699FB',
    fontFamily: 'arial',
    fontWeight: 'bold',
    fontSize: 19,
    marginBottom: 4,
  },
  textProfileData: {
    color: '#2699FB',
    fontFamily: 'arial',
    fontSize: 14,
    marginBottom: 4,
  },
  actionContainer: {
    paddingTop: 30,
    paddingLeft: 37
  },
  actionButton: {
    flexDirection: 'row', 
    marginBottom: 20,
    alignItems: 'center'
  },
  actionText: {
    marginLeft: 25,
    color: '#2699FB', 
    fontSize: 12,
    fontFamily: 'arial',
    fontWeight: 'bold'
  }

 
}); 


