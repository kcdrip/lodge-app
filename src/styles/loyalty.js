import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  loyaltyCard: {
    backgroundColor: '#537EFF',
    alignItems: 'center',
    justifyContent: 'space-around',
    height: 175,
    padding: 10,
  },
  pointsContainer: {
    flex: 1,
    flexDirection: 'row',
  //  marginTop: -7
  },
  text: {
    fontFamily: 'Lato-Regular',
    color: 'white',
  },
  title: {
    marginTop: -3,
    fontSize: 14,
    fontFamily: 'Lato-Regular',
    color: 'white',
  },
  pointsMessageText: {
    fontFamily: 'Lato-Bold',
    color: 'white',
    marginTop: 10,
    marginBottom: 5,
  },
  pointsText: {
    fontSize: 45,
    fontFamily: 'Lato-Bold',
    color: 'white',
    textAlignVertical: 'center',
    includeFontPadding: false,
    padding: 0,
  },
  buttonText: {
    fontSize: 7,
    fontFamily: 'arial',
    fontWeight: 'bold',
    color: 'white',
  },
  checkinButton: {
    marginTop: 12,
    height: 35,
    width: 99,
    borderStyle: 'solid',
    borderWidth: 3,
    borderColor: 'white',
    borderRadius: 15,
    justifyContent: 'center',
    alignItems: 'center',
  }, 
  modalContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    padding: 20,
    borderRadius: 10,
  },
  innerContainer: {
    alignItems: 'center',
  },
  button: {
    width: '25%',
    height: 48,
    borderRadius: 4,
    backgroundColor: '#2699FB',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_white: {
    width: '25%',
    height: 48,
    borderRadius: 4,
    borderColor: '#2699FB',
    borderWidth: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});
