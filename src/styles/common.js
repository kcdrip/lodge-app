import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  modalButton: {
    height: 40,
    borderRadius: 4,
    backgroundColor: '#2699FB',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    color: 'white',
    fontSize: 10,
    fontFamily: 'arial',
    fontWeight: 'bold',
  },
  modalButtonShare: {
    width: 96,
  },
  modalButtonCalendar: {
    width: 125,
  },
  dateCircle: {
    height: SIZE.DATE_CIRCLE,
    width: SIZE.DATE_CIRCLE,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#3366FF',
    borderRadius: SIZE.DATE_CIRCLE / 2,
  },
  dateCircleSmall: {
    height: SIZE.DATE_CIRCLE_SMALL,
    width: SIZE.DATE_CIRCLE_SMALL,
    borderRadius: SIZE.DATE_CIRCLE_SMALL / 2,
  },
  dateCircleText: {
    color: 'white',
    fontWeight: 'bold',
    fontFamily: 'Lato-Bold',
    fontSize: 16,
  },
  dateCircleTextSmall: {
    fontSize: 11,
  },
  offerContainer: {
    width: '100%',
    height: 160,
    backgroundColor: '#BCE0FD',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingVertical: 20,
    marginBottom: 16,
  },
  offerTitle: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 20,
  },
  comeInText: {
    color: 'black',
    fontFamily: 'Arial',
    fontSize: 14,
  },
  redeemButton: {
    backgroundColor: '#2699FB',
    borderRadius: 4,
    height: 38,
    width: 258,
    alignItems: 'center',
    justifyContent: 'center',
  },
  redeemButtonText: {
    color: 'white',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 17,
  },
  modalContainer: {
    borderWidth: 1,
    borderColor: '#2699FB30',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: 10,
    minHeight: 200,
    width: '100%',
  },
  modalContainerRedemption: {
    height: '70%',
  },
  modalText: {
    color: '#2699FB',
    fontFamily: 'arial',
    fontSize: 14,
    lineHeight: 24,
    textAlign: 'center',
  },
  bold: {
    fontWeight: 'bold',
  },
  offerSubText: {
    marginTop: 24,
    color: '#7F7F7F',
    fontFamily: 'arial',
    fontSize: 14,
    fontWeight: 'bold',
  },
  modalButtonsContainer: {
    marginTop: 50,
    flexDirection: 'row',
    width: 160,
    justifyContent: 'space-around',
  },
  modalRoundButton: {
    height: 56,
    width: 56,
    borderColor: '#F1F9FF',
    borderWidth: 1,
    borderRadius: 28,
    alignItems: 'center',
    justifyContent: 'center',
  },
  modalRoundSelected: {
    backgroundColor: '#F1F9FF',
  },
  modalContainerScratch: {
    paddingVertical: 30,
  },
  scratchCardContainer: {
    marginTop: 30,
  },
  shareButtonContainer: {
    width: 72,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  shareButtonArrow: {
    width: 0,
    height: 0,
    borderBottomWidth: 6,
    borderTopWidth: 6,
    borderRightWidth: 6,
    borderStyle: 'solid',
    backgroundColor: 'transparent',
    borderTopColor: 'transparent',
    borderRightColor: '#2699FB',
    borderBottomColor: 'transparent',
  },
  shareButton:{
    width: 66,
    height: 40,
    backgroundColor: '#2699FB',
    justifyContent: 'center',
    alignItems: 'center',
  },
  shareButtonText: {
    color: 'white',
    fontFamily: 'Arial',
    fontSize: 12,
    fontWeight: 'bold',
  },
});
