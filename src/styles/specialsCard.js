import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    marginBottom: 10,
  },
  friendCard: {
    height: 160,
    width: 192,
    marginTop: 12,
    marginHorizontal: 5,
    backgroundColor: '#F1F9FF',
  },
  headerContainer: {
    backgroundColor: '#BCE0FD',
    width: '100%',
    height: 108,
  },
  friendCardImage: {
    height: 108,
    width: '100%',
  },
  cardFooter: {
    width: '100%',
    height: 52,
    marginHorizontal: 5,
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  placeName: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 18,
    fontWeight: 'bold',
    width: 150,
    paddingHorizontal: 4,
    /*textAlign: 'center',*/
  },
});
