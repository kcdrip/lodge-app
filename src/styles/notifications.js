import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
  },
  headerContainer: {
    height: 50,
    alignItems: 'center',
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Lato',
    fontWeight: 'bold',
    fontSize: 23,
    color: '#767676',
  },
  notificationContainer: {
    marginBottom: 16,
  },
  notificationHeader: {
    marginHorizontal: 8,
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  iconCircle: {
    width: 40,
    height: 40,
    borderRadius: 20,
    backgroundColor: '#2699FB',
    alignItems: 'center',
    justifyContent: 'center',
  },
  notificationBox: {
    marginLeft: 50,
    marginRight: 8,
    backgroundColor: '#F1F9FF',
    padding: 30,
  },
  notificationTime: {
    fontFamily: 'Arial',
    fontSize: 14,
    color: '#2699FB',
    marginLeft: 8,
  },
  notificationText: {
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 24,
    color: '#2699FB',
  },
  notificationBottomRow: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 20,
  },
  notificationLeftButtons: {
    flexDirection: 'row',
  },
  iconButton: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 30,
    borderRadius: 4,
    backgroundColor: 'transparent',
  },
  iconButtonComment: {
    paddingHorizontal: 8,
    marginLeft: 20,
  },
  iconButtonPressed: {
    backgroundColor: '#2699FB',
  },
  iconButtonText: {
    fontFamily: 'Arial',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#2699FB',
    marginLeft: 6,
  },
  commentBar: {
    position: 'absolute',
    bottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
    left: 0,
    right: 0,
    height: 65,
    backgroundColor: 'white',
    flexDirection: 'row',
    alignItems: 'center',
  },
  commentBarInput: {
    marginLeft: 8,
    height: 48,
    borderRadius: 24,
    borderColor: '#BCE0FD',
    borderWidth: 1,
    color: '#2699FB',
    paddingHorizontal: 20,
    flex: 1,
  },
  sendCommentButton: {
    height: 48,
    width: 48,
    borderRadius: 24,
    backgroundColor: '#BCE0FD',
    marginHorizontal: 24,
    justifyContent: 'center',
    alignItems: 'center',
  },
  commentsContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    paddingTop: 6,
    paddingHorizontal: 0,
  },
  commentsTitle: {
    fontFamily: 'Arial',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#6E6565',
  },
  commentContainer: {
    marginRight: 20,
    marginBottom: 8,
    paddingLeft: 30,
    width: '100%',
    paddingBottom: 20,
    paddingTop: 10,
    borderBottomColor: '#BCE0FD77',
    borderBottomWidth: 1,
  },
  commentHeader: {
    width: '100%',
    height: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  commentUsername: {
    flex: 1,
    fontFamily: 'Arial',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#2699FB',
    marginLeft: 8,
  },
  commentTime: {
    fontFamily: 'Arial',
    fontSize: 14,
    color: '#2699FB',
    marginRight: 20,
  },
  commentText: {
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 24,
    color: '#2699FB',
    marginLeft: 50,
    marginRight: 40,
  },
});
