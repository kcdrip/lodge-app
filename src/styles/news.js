import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
    marginBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
  },
  newsDetailsContainer: {
     marginBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
  },
  innerContainer: {
    paddingHorizontal: 16,
    paddingTop: 16,
    flex: 1,
    justifyContent: 'space-around',
  },
  newsDetailsImageItemsContainer: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 20,
    height: 52,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',

  },
  detailsImageContainer: {
    minHeight: 150,
    backgroundColor: '#F1F9FF',
  },
  newsCard: {
    marginBottom: 16,
    flex: 1,
    height: 356,
    width: '100%',
    backgroundColor: '#F1F9FF',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  newsCardWithoutImage: {
    height: 176,
  },
  newsCardWithoutSubtitle: {
    height: 245,
  },
  newsCardImage: {
    height: 180,
    width: '100%',
  },
  newsCardTitle: {
    color: '#2699FB',
    fontSize: 22,
    fontWeight: 'bold',
    fontFamily: 'Arial',
  },
  newsDetailsTitle: {
    paddingLeft: 16,
    paddingTop: 16,
    color: '#2699FB',
    fontSize: 40,
    fontFamily: 'Georgia',
  },
  detailsCardTime: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'Arial',
    marginLeft: 6,
  },
  cardTitleContainer: {
    paddingLeft: 20,
    marginTop: 20,
    height: 26,
  },
  newsDateContainer: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  newsDetailsContentContainer: {
    padding: 16,
  },
  newsDetailsSubtitle: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 20,
    paddingTop: 20,
    paddingLeft: 16,
  },
  newsCardSubtitle: {
    marginHorizontal: 22,
    marginVertical: 13,
    flex: 1,
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 24,
  },
  newsletterPostMainText: {
    marginHorizontal: 20,
    marginVertical: 16,
    flex: 1,
  },
  commentsContainer: {
    marginBottom: 80,
    paddingLeft: 32,
    flexDirection: 'row',
  },
  commentCounter: {
    color: '#2699FB',
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'Arial',
    marginLeft: 6,
  },
});
