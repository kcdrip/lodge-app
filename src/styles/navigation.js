import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';


export default StyleSheet.create({
  bottomNavBar: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center',
    backgroundColor: '#2699FB',
    overflow: 'hidden',
    width: '100%',
    height: SIZE.BOTTOM_NAVIGATION_HEIGHT,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    paddingLeft: 25,
    paddingRight: 25,
  },
  item: {
    height: '100%',
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingBottom: 10,
    paddingTop: 10,
  },
  itemText: {
    color: 'white',
    fontSize: 9,
    fontFamily: 'arial',
    fontWeight: 'bold',
  },
  redDot: {
    backgroundColor: 'red',
    width: 6,
    height: 6,
    borderRadius: 3,
    position: 'absolute',
    top: 25,
    right: 20,
  },
});
