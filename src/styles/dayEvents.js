import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    marginBottom: 10,
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  special: {
    minHeight: 85,
    padding: 10,
    backgroundColor: '#F8F8F8',
  },
  eventCard: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 15,
  },
  eventCardButton: {
    height: 43,
    width: '80%',
    shadowColor: 'black',
    shadowRadius: 6,
    shadowOpacity: 0.06,
    elevation: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
    backgroundColor: '#F8F8F8',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 5,
  },
  card_text: {
    fontFamily: 'Lato-Black',
    fontSize: 10,
  },
  button: {
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2699FB',
    height: 21,
    width: 92,
  },
  buttonText: {
    color: 'white',
    fontSize: 8,
    fontFamily: 'arial',
  },
  title: {
    color: '#181743',
    fontSize: 11,
    fontFamily: 'Lato',
    textTransform: 'uppercase',
  },
  modalContainer: {
    height: '100%',
    width: '100%',
  },
  modal: {
    flex: 1,
    marginHorizontal: 0,
    marginVertical: 60,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    backgroundColor: '#F1F9FF',
    borderRadius: 10,
    padding: 30,
    elevation: 8,
    shadowColor: 'black',
    shadowRadius: 10,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  modalTitle: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
    fontWeight: 'bold',
  },
  modalSubTitle: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
  },
  modalText: {
    color: '#868889',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
    flex: 1,
  },
  innerContainer: {
    position: 'relative',
  },
  modalButtonContainer: {
    height: 40,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
  },
 /* modalButton: {
    height: 40,
    borderRadius: 4,
    backgroundColor: '#2699FB',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalButtonText: {
    color: 'white',
    fontSize: 10,
    fontFamily: 'arial',
    fontWeight: 'bold',
  },
  modalButtonShare: {
    width: 96,
  },
  modalButtonCalendar: {
    width: 125,
  }, */

});