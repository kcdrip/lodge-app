import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  agendaContainer: {
    flex: 1,
    paddingBottom: 10,
  },
  dataBox: {
    backgroundColor: 'white',   
    borderRadius: 6,    
    shadowColor: 'black',
    shadowRadius: 6,
    shadowOpacity: 0.06,
    elevation: 5,
    shadowOffset: {
      width: 0,
      height: 1,
    },
  },
  dailySpecialBox: {
    marginLeft: 35,
    marginRight: 20,
    marginTop: 20,
    marginBottom: 16,
    minHeight: 86,
    padding: 14,
  },
  dailySpecialTitle: {
    fontFamily: 'Arial',
    fontSize: 12,
    fontWeight: 'bold',
  },
  dailySpecialText: {
    marginTop: 6,
    fontFamily: 'Lato',
    fontSize: 12,
    fontWeight: 'normal',
  },
  eventContainer: {
    height: 68,
    marginHorizontal: 20,
    flexDirection: 'row',
    alignItems: 'center',
    paddingBottom: 16,
  },
  eventTimeContainer: {
    width: 50,
  },
  eventTime: {
    fontFamily: 'Lato',
    fontSize: 11,
    fontWeight: '900',
  },
  eventCircle: {
    width: 7,
    height: 7,
    borderRadius: 3.5,
    backgroundColor: '#FEA64C',
  },
  eventBox: {
    height: 52,
    paddingHorizontal: 20,
    marginLeft: 10,
    justifyContent: 'space-between',
    alignItems: 'center',
    flex: 1,
    flexDirection: 'row',

  },
  featuredEventBox: {
    backgroundColor: '#2699FB',
  },
  eventTitle: {
    fontSize: 12,
    fontFamily: 'Lato',
    textTransform: 'uppercase',
    color: 'black',
  },
  featuredEventTitle: {
    color: 'white',
  },
  modalContainer: {
    width: '100%',
    height: '100%',
  },
  modal: {
    flex: 1,
    
    marginHorizontal: 12,
    marginVertical: 100,
    justifyContent: 'flex-start',
    flexDirection: 'column',
    backgroundColor: '#F1F9FF',
    borderRadius: 10,
    padding: 30,
    elevation: 8,
    shadowColor: 'black',
    shadowRadius: 10,
    shadowOpacity: 0.2,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  modalTitle: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
    fontWeight: 'bold',
  },
  modalSubTitle: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
  },
  modalText: {
    color: '#868889',
    fontSize: 14,
    fontFamily: 'arial',
    lineHeight: 24,
    flex: 1,
  },
  innerContainer: {
    position: 'relative',
  },
  modalButtonContainer: {
    height: 40,
    width: '100%',
    flexDirection: 'row',
    alignSelf: 'flex-end',
    justifyContent: 'space-between',
  },
});
