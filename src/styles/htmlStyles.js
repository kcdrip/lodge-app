import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  newsletter: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 24,
  },
  a: {
    fontWeight: '300',
    color: '#FF3366', // make links coloured pink
  },
  gray: {
    color: '#6E6565',
    fontWeight: 'bold',
  },
  div: {
  },
});
