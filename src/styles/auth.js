import { StyleSheet } from 'react-native';

import { 
  color, 
  fontSize, 
  SIZE
} from './Theme';

const rounderElement = {
  borderRadius: SIZE.INPUT_HEIGHT / 2, 
  height: SIZE.INPUT_HEIGHT,
  width: '80%'
}

export default StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#ffffff',
  },
  centeredContainer: {
    justifyContent: 'center',
  },
  logoContainer: {
    //height: SIZE.HEADER_HEIGHT, 
    paddingVertical: 30, 
    width: '100%', 
    backgroundColor: color.LOGO_BACKGROUND, 
    alignItems: 'center', 
    justifyContent: 'flex-end',
    marginBottom: 16
  },
  input: {
    ...rounderElement,
    padding: 10, 
    paddingHorizontal: 30, 
    marginBottom: SIZE.INPUT_MARGIN,
    borderColor: color.INPUT_BORDER, 
    borderWidth: 2,
    color: color.INPUT_TEXT,
    fontSize: fontSize.INPUT,
    fontFamily: 'arial',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  failedInput: {
    borderColor: 'red'
  },
  button: {
    ...rounderElement,    
    alignItems: 'center',
    marginBottom: SIZE.BUTTON_MARGIN,
  }, 
  buttonPrimary: {
    backgroundColor: color.BUTTON_PRIMARY,
    justifyContent: 'center',
  },
  buttonSecondary: {
    backgroundColor: 'white',
    borderColor: color.BUTTON_PRIMARY,
    borderWidth: 2,
    justifyContent: 'center',
  },
  buttonDisabled: {
    backgroundColor: color.BUTTON_PRIMARY_DISABLED,
  },
  buttonPrimaryText: {
    fontSize: fontSize.BUTTON,
    fontWeight: 'bold',
    fontFamily: 'arial',
    color: 'white',
  },
  buttonSecondaryText: {
    fontSize: fontSize.BUTTON,
    fontWeight: 'bold',
    fontFamily: 'arial',
    color: color.BUTTON_PRIMARY,
  },
  spacer: {
    backgroundColor: color.SPACER,
    width: SIZE.SPACER_WIDTH,
    height: SIZE.SPACER_HEIGHT,
    borderRadius: SIZE.SPACER_HEIGHT / 2,
    marginTop: SIZE.SPACER_MARGIN,
    marginBottom: SIZE.SPACER_MARGIN,
  },
  facebookIcon: {
    position: 'absolute',
    left: 20,
  },
  mb_min: {
    marginBottom: SIZE.INPUT_MARGIN / 4,
  },
  backButtonContainer: {
    position: 'absolute',
    left: 15,
    width: 25,
    height: 25,
  },
  screenTitleContainer: {
    marginTop: 20,
    //marginBottom: -17,
    //height: 15,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'center',
  },
  screenTitle: {
    color: 'white', 
    fontSize: 23, 
    fontFamily: 'Lato-Bold',
  },
  avoidKeyboard: {
    marginTop: -SIZE.HEADER_HEIGHT + 55,
  },
  datePickerContainer: {
    width: '100%',
    height: '100%',
    justifyContent: 'center',
  },
  datePickerButtonContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  editorScreenTitle: {
    color: '#7F7F7F',
    fontSize: 30,
    marginBottom: 25,
    fontWeight: 'bold',
    fontFamily: 'arial',
  },
  subTitle: {
    color: '#7F7F7F',
    fontSize: 14,
    fontWeight: 'bold',
    fontFamily: 'arial',
  },
  titleContainer: {
    alignItems: 'center',
    marginBottom: 10,
  },
  //BMS 2019-02-14
  createYourAccountHeaderContainer: {
    //height: SIZE.HEADER_HEIGHT, 
    //height: '0%',
    paddingVertical: 25, 
    width: '100%', 
    backgroundColor: color.LOGO_BACKGROUND, 
    alignItems: 'center', 
    justifyContent: 'flex-start',
    marginBottom: 16,
    marginTop: 5,
  }
});

