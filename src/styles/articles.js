import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  articleDetailsContainer: {
  },
  detailsImageContainer: {
    minHeight: 150,
    backgroundColor: '#BCE0FD',
  },
  articleDetailsContentContainer: {
    //paddingHorizontal: 16,
    alignItems: 'center',
    marginHorizontal: 2,
    paddingVertical: 65,
  },
  articleDetailsTitle: {
    color: '#2699FB',
    fontSize: 20,
    fontFamily: 'Arial',
    fontWeight: 'bold',
  },
  articleMainText: {
    marginHorizontal: 20,
    marginVertical: 16,
    flex: 1,
  },
  contactBlock: {
    marginTop: 10,
    flexDirection: 'row',
    justifyContent: 'space-around',
    width: '100%',
    height: 40,
    paddingHorizontal: 30,
  },
  contactButton: {
    width: 96,
    height: 40,
    backgroundColor: '#2699FB',
    borderRadius: 4,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactButtonText: {
    color: 'white',
    fontSize: 10,
    fontFamily: 'Arial',
    fontWeight: 'bold',
  },
});
