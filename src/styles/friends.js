import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
  },
  friendCard: {
    height: 206,
    marginTop: 16,
    marginHorizontal: 16,
    backgroundColor: '#F1F9FF',
    flexDirection: 'column',
  },
  headerContainer: {
    backgroundColor: '#BCE0FD',
    width: '100%',
    height: 157,
  },
  friendCardImage: {
    height: 157,
    width: '100%',
  },
  cardFooter: {
    width: '100%',
    height: 49,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  placeName: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
  },
  friendScrollContainer: {
    flex: 1,
  },
  friendScrollContent: {
    padding: 16,
    flexDirection: 'column',
  },
  logoContainer: {
    width: '100%',
    height: 208,
    backgroundColor: '#BCE0FD',
    marginBottom: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  logoImage: {
    height: 208,
    width: '100%',
  },
  logoTitle: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontWeight: 'bold',
    fontSize: 24,
  },
  aboutContainer: {
    width: '100%',
    backgroundColor: '#F1F9FF',
    marginBottom: 16,
    padding: 24,
  },
  aboutText: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 24,
  },
  contact: {
    width: '100%',
    height: 64,
    backgroundColor: '#BCE0FD',
    flexDirection: 'row',
    marginBottom: 16,
  },
  contactIconContainer: {
    width: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactContentContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  contactArrowContainer: {
    width: 64,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contactData: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
    lineHeight: 16,
  },
  contactName: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 10,
  },
  mapContainer: {
    width: '100%',
    backgroundColor: '#F1F9FF',
    height: 400,
    marginBottom: 16,
  },
});

export const blueMapStyle = [
  {
    stylers: [
      {
        hue: '#2699FB',
      },
      {
        saturation: 250,
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'geometry',
    stylers: [
      {
        lightness: 50,
      },
      {
        visibility: 'simplified',
      },
    ],
  },
  {
    featureType: 'road',
    elementType: 'labels',
    stylers: [
      {
        visibility: 'off',
      },
    ],
  },
];
