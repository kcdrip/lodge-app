import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerContainer: {
    height: 50,
    justifyContent: 'center',
  },
  headerText: {
    fontFamily: 'Lato',
    fontWeight: 'bold',
    fontSize: 23,
    color: '#767676',
    textDecorationLine: 'underline',
  },
  list: {
    backgroundColor: 'white',
    marginHorizontal: 35,
  },
  item: {
    height: 54,
    justifyContent: 'center',
  },
  itemText: {
    fontSize: 14,
    fontFamily: 'arial',
    fontWeight: 'bold',
    color: '#7F7F7F',
  },
  separator: {
    height: 1,
    width: '100%',
    backgroundColor: '#707070',
  },
  footer: {
    height: 50,
  },
});
