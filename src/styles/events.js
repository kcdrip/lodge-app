import { StyleSheet } from 'react-native';

import {
  color,
  fontSize,
  SIZE,
} from './Theme';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  scrollView: {
   marginBottom: SIZE.BOTTOM_NAVIGATION_HEIGHT,
   paddingHorizontal: 16,
  },
  eventDetailsContainer: {
  },
  innerContainer: {
    paddingHorizontal: 16,
    paddingTop: 16,
    flex: 1,
    justifyContent: 'space-around',
  },
  eventDetailsImageItemsContainer: {
    position: 'absolute',
    left: 20,
    right: 20,
    bottom: 20,
    height: 52,
    flexDirection: 'row',

  },
  detailsImageContainer: {
    minHeight: 150,
    backgroundColor: '#F1F9FF',
  },
  eventCard: {
    marginBottom: 16,
    flex: 1,
    height: 356,
    width: '100%',
    backgroundColor: '#F1F9FF',
    flexDirection: 'column',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  eventCardImage: {
    height: 180,
    width: '100%',
  },
  eventDetailsTitleImage: {
    width: '100%',
  },
  eventCardTitle: {
    textTransform: 'uppercase',
    color: '#2699FB',
    fontSize: 20,
    fontWeight: 'bold',
    fontFamily: 'Arial',
  },
  topEventCardTime: {
    color: '#2699FB',
    fontSize: 14,
    fontFamily: 'Arial',
  },
  eventDetailsTitle: {
    paddingLeft: 16,
    paddingTop: 16,
    color: '#2699FB',
    fontSize: 40,
    fontFamily: 'Georgia',
  },
  detailsCardTime: {
    color: 'white',
    textShadowOffset: {
      width: 2,
      height: 2,
    },
    textShadowRadius: 3,
    textShadowColor: '#333',
    fontSize: 17,
    fontFamily: 'Arial',
    fontWeight: 'bold',
  },
  cardTitleContainer: {
    paddingLeft: 20,
    marginTop: 10,
    height: 52,
    flexDirection: 'row',
  },
  cardTitleRightBlock: {
    paddingLeft: 16,
    flex: 1,
    justifyContent: 'center',
  },
  eventDetailsImageRightBlock: {
    paddingLeft: 16,
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  eventDetailsContantContainer: {
    padding: 16,
  },
  eventDetailsSubtitle: {
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 20,
    paddingTop: 20,
    paddingLeft: 16,
  },
  eventDetails: {
    marginHorizontal: 20,
    marginVertical: 16,
    flex: 1,
    color: '#2699FB',
    fontFamily: 'Arial',
    fontSize: 14,
  },
  eventDetailsDoubleLine: {
    lineHeight: 24,
  },
  bottomButtonsContainer: {
    marginBottom: 80,
    paddingLeft: 32,
    paddingTop: 16,
  },
});
