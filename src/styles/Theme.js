import { Dimensions, Platform } from 'react-native';

const color = {
  LOGO_BACKGROUND: '#2699FB',
  INPUT_TEXT: '#2699FB',
  INPUT_BORDER: '#BCE0FD',
  BUTTON_PRIMARY: '#2699FB',
  BUTTON_PRIMARY_DISABLED: '#90C7f6',
  SPACER: '#BCE0FD',
  BLUE: '#2699FB',
  black: "#010101",
  main: "#397af8",
  white: "#ffffff",
  grey: "#eaeaea"
}

const fontSize = {
  INPUT: 14,
  BUTTON: 10,
  tiny: 12,
  smaller: 13,
  small: 14,
  regular: 15,
  large: 22
}

const fontFamily = {
}

const misc = {
  navbar_height: (Platform.OS === 'ios') ? 64 : 54,
  window_width: Dimensions.get('window').width,
  window_height: Dimensions.get('window').height
}

const padding = 8;

const SIZE = {
  HEADER_HEIGHT: 169,
  INPUT_HEIGHT: 48,
  INPUT_MARGIN: 16,
  BUTTON_MARGIN: 16,
  SPACER_WIDTH: 24,
  SPACER_HEIGHT: 4,
  SPACER_MARGIN: 10,
  PROFILE_IMAGE_SIZE: 100,
  BOTTOM_NAVIGATION_HEIGHT: 56,
  DATE_CIRCLE: 52,
  DATE_CIRCLE_SMALL: 38,
}



export {
  color, 
  fontSize, 
  fontFamily, 
  misc, 
  padding,
  SIZE
}