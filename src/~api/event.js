import { Events } from '../data';

export function getFeaturedEvents() {
    return Events.featured_events;
}

export function getDailyEvents(month, day) {
    return Events.daily_events;
}