export const Events = {
    featured_events: [
        {
            id: 1,
            month: 1,
            day: 15,
            from: "9:30",
            to: "10:40",
            'title': "Event1",
            detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            image: '',
        }, {
            id: 2,
            month: 2,
            day: 15,
            from: "9:30",
            to: "10:40",
            title: "Event2",
            image: '',
            detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        }, {
            id: 3,
            month: 3,
            day: 15,
            from: "9:30",
            to: "10:40",
            title: "Event3",
            image: '',
            detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        }, {
            id: 4,
            month: 4,
            day: 15,
            from: "9:30",
            to: "10:40",
            title: "Event4",
            image: '',
            detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        },
    ],
    /* Most of these Daily Events repeat weekly or monthly. Would like an option to set that*/
    daily_events: {
        special: "Daily Special Details - Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est eopksio laborum. Sed ut perspiciatis unde omnis istpoe natus error sit voluptatem accusantium doloremque eopsloi laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae ",
        events: [
            {
                id: 0,
                month: 1,
                day: 5,
                isWeekly: false,
                isMonthly: false,
                from: "ALL",
                to: "DAY",
                title: "Event4",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 1,
                month: 1,
                day: 5,
                from: "3:00",
                to: "5:40",
                title: "Event12",
                isWeekly: false,
                isMonthly: false,
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 2,
                month: 1,
                day: 5,
                from: "6:00",
                to: "7:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 3,
                month: 1,
                day: 11,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 4,
                month: 2,
                day: 4,
                from: "5:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event24",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 5,
                month: 2,
                day: 25,
                from: "4:00",
                to: "4:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event225",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 6,
                month: 3,
                day: 3,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 7,
                month: 3,
                day: 15,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event33",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 8,
                month: 4,
                day: 5,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 9,
                month: 4,
                day: 9,
                from: "7:00",
                isWeekly: false,
                isMonthly: false,
                to: "7:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 10,
                month: 5,
                day: 5,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 11,
                month: 5,
                day: 14,
                from: "3:00",
                isWeekly: false,
                isMonthly: false,
                to: "5:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 12,
                month: 6,
                day: 7,
                isWeekly: false,
                isMonthly: false,
                from: "3:00",
                to: "5:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 13,
                month: 7,
                day: 1,
                isWeekly: false,
                isMonthly: false,
                from: "3:00",
                to: "5:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 14,
                month: 7,
                day: 6,
                isWeekly: false,
                isMonthly: false,
                from: "3:00",
                to: "5:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 15,
                month: 7,
                day: 15,
                from: "3:00",
                isWeekly: false,
                isMonthly: false,
                to: "5:40",
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 16,
                month: 8,
                day: 3,
                from: "3:00",
                to: "5:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }, {
                id: 17,
                month: 8,
                day: 15,
                from: "6:00",
                to: "6:40",
                isWeekly: false,
                isMonthly: false,
                title: "Event12",
                detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            }
        ]
    }
};
export const Rewards = [
    {
        id: 1,
        title: "$2 OFF Your Next Purchase",
        /* Description: With further details. ie. $2 off on orders over $15*/
        /*Valid Until: Specific Date or No expiration*/
        /*UPC Code/Unique code (Optional)*/        
    }, {
        id: 2,
        title: "Welcome Reward: $3 OFF",
    }, {
        id: 3,
        title: "Welcome Reward: $3 OFF",
    }, {
        id: 4,
        title: "$2 OFF Your Next Purchase",
    }
];
export const Newsletters = [
    {
        id: 1,
        title: "Excepteur sint occaecat.",
        subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
        /* Featured Image */
    }, {
        id: 2,
        title: "Excepteur sint occaecat.",
        subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    }, {
        id: 3,
        title: "Excepteur sint occaecat.",
        subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    }, {
        id: 4,
        title: "Excepteur sint occaecat.",
        subTitle: "Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim.",
        detail: "Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
    }
];
export const Friends = [
    {
        id: 1,
        nameOfPlace: "ST Coel Wilston",
        /*Description*/
        /*Website*/
        /*Phone*/
        /*Email*/
        /*Location Details*/
        /*Special Offer - Ability to put a "Reward" on their page and have it show up in the rewards section as well*/
    }, {
        id: 2,
        nameOfPlace: "AE Stoma Welson",
    }, {
        id: 3,
        nameOfPlace: "CE Stoma Welson",
    }, {
        id: 4,
        nameOfPlace: "VE Coel Wilston",
    }, {
        id: 5,
        nameOfPlace: "WE Coel Wilston",
    }
];
