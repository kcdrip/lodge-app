import {
  createSwitchNavigator,
  createStackNavigator,
} from 'react-navigation';

import Login from '../screens/login';
import SignUp from '../screens/signup';

import Home from '../screens/home';

import Profile from '../screens/profile';
import EditProfile from '../screens/editProfile';
import ChangePassword from '../screens/changePassword';
import Support from '../screens/support';

import FeaturedEvents from '../screens/featuredEvents';
import EventDetails from '../screens/eventDetails';
import Newsletter from '../screens/newsletter';
import NewsletterPost from '../screens/newsletterPost';
import Link from '../screens/link';
import Calendar from '../screens/calendar';
import Notifications from '../screens/notifications';
import Friends from '../screens/friends';
import FriendLocation from '../screens/friendLocation';
import Rewards from '../screens/rewards';
import Loyalty from '../screens/loyalty';
import Article from '../screens/article';


/*

import GeoFencing from '../containers/loyalty/geofencing';
import ScanQRCode from '../containers/loyalty/scanqr';
import { WomenOfMoose } from '../containers/other';

*/


export default createSwitchNavigator({
  SignedIn: {
    screen: createStackNavigator({
      Home: { screen: Home },
      Profile: { screen: Profile },
      EditProfile: { screen: EditProfile },
      ChangePassword: { screen: ChangePassword },
      Support: { screen: Support },
      FeaturedEvents: { screen: FeaturedEvents },
      EventDetails: { screen: EventDetails },
      Newsletter: { screen: Newsletter },
      NewsletterPost: { screen: NewsletterPost },
      Link: { screen: Link },
      Calendar: { screen: Calendar },
      Notifications: { screen: Notifications },
      Friends: { screen: Friends },
      FriendLocation: { screen: FriendLocation },
      Rewards: { screen: Rewards },
      Loyalty: { screen: Loyalty },
      Article: { screen: Article },
    },
    {
      initialRouteName: "Home",
      swipeEnabled: false,
      animationEnabled: false,
      lazy: true,
      transitionConfig: () => ({
        transitionSpec: {
          duration: 0,
        },
      }),
    },),
  },
  SignedOut: {
    screen: createStackNavigator({
      Login: { screen: Login },
      SignUp: { screen: SignUp },
    },
    {
      initialRouteName: 'Login',
    }),
  },
},
{
  initialRouteName: 'SignedOut',
});
