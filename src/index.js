import React from 'react';
import { YellowBox, View, Alert, findNodeHandle, Text } from 'react-native';
import { Provider } from 'react-redux';
import SplashScreen from 'react-native-splash-screen';

import { createStore, applyMiddleware } from 'redux';
import { combineReducers } from 'redux-immutable';
import { reduxKittens } from 'redux-kittens';
import thunk from 'redux-thunk';

import { NavigationActions } from 'react-navigation';

import firebase from 'react-native-firebase';

import Router from './config/routes';

import stores from './redux/stores';

import { authActions, notificationsActions, rewardsActions, eventsActions } from './redux/actions';

import Blur from './components/blur';


if (Text.defaultProps == null) Text.defaultProps = {};
Text.defaultProps.allowFontScaling = false;

YellowBox.ignoreWarnings([
  'Module RNCalendarEvents',
]);


const store = applyMiddleware(
  thunk,
  ...reduxKittens({
    enableLog: true,
  }),
)(createStore)(combineReducers(stores));

export default class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      dismissHandler: null,
      modal: null,
      background: null,
      scrollToBottomHandler: null,
      viewRef: -1,
      splash: true,
    };
    
    this.navigator = null;
  }

  themeListener()  {
    if (this.state.splash 
      && store.getState().getIn(['auth', 'themeIsLoaded']) 
      && store.getState().getIn(['auth', 'settingsAreLoaded'])) {
        this.setState({ splash: false }); 
        setTimeout(() => SplashScreen.hide(), 500); 
      }
  }

  componentWillMount() {
    this.themeListener();
    store.subscribe(this.themeListener.bind(this));
    store.dispatch(authActions.loadSettings());
    store.dispatch(authActions.loadTheme());
    store.dispatch(eventsActions.fetchEvents());   
    setTimeout(() => SplashScreen.hide(), 5000);
  }

  componentDidMount() {
    this.themeListener();
    firebase.auth()
      .onAuthStateChanged((user) => {        
        if (user) {
          firebase.messaging().subscribeToTopic('all').then(v => {
            console.log('Subscribed to all');
          });
          store.dispatch(authActions.fetchUserInfo(user.uid));
          firebase.messaging().hasPermission()
            .then((enabled) => {
              if (!enabled) {
                firebase.messaging().requestPermission()
                  .then((v) => {
                    console.log('Firebase permission: requested', v);
                  });
                }
              console.log('Firebase permission:', enabled);
              /*firebase.messaging().getToken().then(t => {
                console.log('Firebase messaging token', t);
              })*/
            });
            
        } else {
          firebase.messaging().unsubscribeFromTopic('all');
        };

        this.navigator.dispatch(
          NavigationActions.navigate({
            routeName: user ? 'SignedIn' : 'SignedOut',
          }),
        );

        this.setState({
          modal: null,
          dismissHandler: null,
          background: null,
        });
      });

     this.messageListener = firebase.messaging().onMessage(message => {
      console.log('Firebase message:', message);
      store.dispatch(notificationsActions.fetchNotifications());
    });

  }

  componentWillUnmount() {
    this.messageListener();
  }

  render() {
    const modalSetters = {
      setContent: (modal, background, view) => this.setState({
        modal,
        background,
        viewRef: view ? findNodeHandle(view) : -1,
      }),
      clearContent: () => this.setState({
        modal: null,
        dismissHandler: null,
        background: null,
      }),
      setDismissHandler: dismissHandler => this.setState({
        dismissHandler,
      }),
    };

    const homePageSetters = {
      setScrollToBottomHandler: scrollToBottomHandler => this.setState({ scrollToBottomHandler }),
      scrollToBottom: () => typeof this.state.scrollToBottomHandler === 'function' ? this.state.scrollToBottomHandler() : null,
    }

    const {
      modal,
      background,
      dismissHandler,
      viewRef,
    } = this.state;


    return (
      <View style={{ flex: 1 }}>
        <Provider store={store}>
          <Router
            screenProps={{ 
              modal: modalSetters,
              homePage: homePageSetters,
            }}
            ref={(ref) => { this.navigator = ref; }}
          />
        </Provider>
        <Blur
          content={modal}
          type={background}
          viewRef={viewRef}
          onDismiss={() => {
            if (dismissHandler) {
              dismissHandler();
            }
            this.setState({
              modal: null,
              dismissHandler: null,
            });
          }}
        />
      </View>
    );
  }
}
