import React, { Component } from 'react';
import {
  View,
  TouchableOpacity,
  TextInput,
  Keyboard,
  Dimensions,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import styles from '../styles/notifications';

import {
  SIZE,
} from '../styles/Theme';

class CommentBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: '',
      spacer: 0,
    };
  }

  componentDidMount() {
    this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', e => this.keyboardDidShow(e));
    this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', () => this.keyboardDidHide());
  }

  componentWillUnmount() {
    this.keyboardDidShowListener.remove();
    this.keyboardDidHideListener.remove();
  }

  keyboardDidShow(e) {
    this.setState({ spacer: Dimensions.get('window').height - e.endCoordinates.screenY - SIZE.BOTTOM_NAVIGATION_HEIGHT });
  }

  keyboardDidHide() {
    this.setState({ spacer: 0 });
  } 


  handleSend() {
    const {
      state: {
        text,
      },
      props: {
        onSend,
      },
    } = this;

    if (text !== '') { 
      onSend(text);
      this.setState({ text: '' });
    }
    Keyboard.dismiss();
  }

  render() {
    const {
      state: {
        text,
      },
      props: {
        visible,
      },
    } = this;

    if (!visible) {
      return null;
    }

    return (
      <View style={[styles.commentBar, {marginBottom: this.state.spacer}]}>
        <TextInput
          style={styles.commentBarInput}
          value={text}
          placeholder="Say something…"
          placeholderTextColor="#BCE0FD"
          onSubmitEditing={() => this.handleSend()}
          onChangeText={text => this.setState({ text })}
        />
        <TouchableOpacity
          onPress={() => this.handleSend()}
          style={styles.sendCommentButton}
        >
          <Icon
            name="md-arrow-forward"
            size={24}
            color="#2699FB"
          />          
        </TouchableOpacity>        
      </View>
    );
  }
}

export default CommentBar;
