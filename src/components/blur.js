import React, { Component } from 'react';
import {
  Animated,
  StyleSheet,
  TouchableWithoutFeedback,
} from 'react-native';

import { BlurView } from 'react-native-blur';

const styles = StyleSheet.create({
  container: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
    padding: 24,
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  absolute: {
    position: 'absolute',
    left: 0,
    right: 0,
    top: 0,
    bottom: 0,
  },
});

class Blur extends Component {
  constructor(props) {
    super(props);
    this.fader = new Animated.Value(0);
  }

  componentWillReceiveProps(newProps) {
    const { props } = this;

    const changeVisibility = visible => Animated.timing(
      this.fader,
      {
        toValue: visible ? 1 : 0,
        duration: 300,
      },
    ).start();

    if (newProps.content && !props.content) {
      changeVisibility(true);
    } else if (!newProps.content && props.content) {
      this.fader.setValue(0);
    }
  }

  render() {
    const {
      content,
      type,
      onDismiss,
      viewRef,
    } = this.props;

    return content ? (
      <TouchableWithoutFeedback onPress={() => onDismiss()} style={styles.absolute}>
        <Animated.View
          style={[
            styles.absolute, {
              opacity: this.fader,
            },
          ]}
        >
          <BlurView
            style={styles.absolute}
            blurType={type || 'dark'}
            blurAmount={5}
            viewRef={viewRef}
          />
          <Animated.View
            style={[
              styles.container, {
                transform: [{ scaleX: this.fader }, { scaleY: this.fader }],
              },
            ]}
          >
            { content }
          </Animated.View>
        </Animated.View>
      </TouchableWithoutFeedback>
    ) : null;
  }
}

export default Blur;
