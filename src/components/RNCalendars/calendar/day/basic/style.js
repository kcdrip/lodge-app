import {StyleSheet, Platform} from 'react-native';
import * as defaultStyle from '../../../style';

const STYLESHEET_ID = 'stylesheet.day.basic';

export default function styleConstructor(theme={}) {
  const appStyle = {...defaultStyle, ...theme};
  return StyleSheet.create({
    base: {
      width: 64,
      height: 64,
      alignItems: 'center',
      //marginBottom: -22,
    },
    text: {
      fontSize: 16,
      marginTop: 12,
      fontFamily: 'Baloo',
      fontWeight: '300',
      color: 'black',
      backgroundColor: 'transparent',
    },
    alignedText: {
      marginTop: Platform.OS === 'android' ? 4 : 6
    },
    selected: {
      backgroundColor: 'green',
      borderRadius: 6
    },
    today: {
      backgroundColor: appStyle.todayBackgroundColor
    },
    todayText: {
      color: appStyle.todayTextColor
    },
    selectedText: {
      color: 'white',
      marginTop: 6,
      fontSize: 22,
    },
    disabledText: {
      color: appStyle.textDisabledColor
    },
    dot: {
      width: 6,
      height: 6,
      marginTop: 18,
      borderRadius: 3,
      opacity: 0
    },
    visibleDot: {
      opacity: 1,
      backgroundColor: 'red'
    },
    selectedDot: {
     // backgroundColor: 'red',
      transform: [{
        translateY: Platform.OS === 'android' ? 0 : -6,
      }]
    },
    ...(theme[STYLESHEET_ID] || {})
  });
}
