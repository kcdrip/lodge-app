import React from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity,  StyleSheet, Text, View } from 'react-native';

import { connect } from 'react-redux';

import {
  getColor,
  getBg,
} from '../utils/theme';

import styles from '../styles/common';

const ShareButton = ({
  onPress,
  background,
  text,
}) => (
  <TouchableOpacity 
    onPress={() => onPress()}
    style={styles.shareButtonContainer}
  >
    <View style = {[
        styles.shareButtonArrow,
        { borderRightColor: (background || {}).backgroundColor },
      ]}
    />
    <View style={[styles.shareButton, background]}>
      <Text style={[styles.shareButtonText, text]}>
        SHARE
      </Text>      
    </View>    
  </TouchableOpacity>
  
);

ShareButton.propTypes = {
  onPress: PropTypes.func.isRequired,
  small: PropTypes.bool,
  style: PropTypes.any,
};

const mapStateToProps = state => ({
  background:  getBg(state, 'button', 'background'),
  text:  getColor(state, 'button', 'text'),
});

export default connect(mapStateToProps)(ShareButton);

