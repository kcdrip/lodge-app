import React, { Component } from 'react';
import {
  StyleSheet,
  View, 
  TouchableWithoutFeedback,
  Text,
} from 'react-native';

const styles = StyleSheet.create({
  text: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 22,
    fontFamily: 'arial',
    textAlign: 'center'
  }
});

class ErrorPopup extends Component {
  render() {
    const {
      message
    } = this.props;

    return message ? (
      <Text style={styles.text}>
        {message}
      </Text>
    ) : null;
  }
}

export default ErrorPopup
