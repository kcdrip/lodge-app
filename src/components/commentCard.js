import React, { Component } from 'react';
import {
  View,
  Text,
  Image,
} from 'react-native';

import Icon from 'react-native-vector-icons/Ionicons';

import moment from 'moment';

import styles from '../styles/notifications';

const CommentCard = ({ 
  data,
  cardText,
  iconBg,
}) => {
  const circleContent = data.profileImage ? (
    <Image
      source={{ uri: data.profileImage, width: 40, height: 40 }}
      style={{ width: 40, height: 40, borderRadius: 20 }}
    />
  ) : (
    <Icon
      name="md-person"
      size={24}
      color="white"
    />
  );

  return (
    <View style={styles.commentContainer}>
      <View style={styles.commentHeader}>
        <View style={[styles.iconCircle, iconBg]}>
          {circleContent}    
        </View>
        <Text style={[styles.commentUsername, cardText]}>
          {data.userName || 'Anonymus'}
        </Text>
        <Text style={[styles.commentTime, cardText]}>
          {data.date ? moment(data.date).fromNow() : ''}
        </Text>
      </View>
      <Text style={[styles.commentText, cardText]}>
        {data.content}
      </Text>

    </View>
  );
}


export default CommentCard;
