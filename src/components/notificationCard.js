import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import { connect } from 'react-redux';

import { Map } from 'immutable';

import moment from 'moment';

import CommmentCard from './commentCard';

import styles from '../styles/notifications';

import {
  getColor,
  getBg,
} from '../utils/theme';

class NotificationCard extends Component {
  renderComments(comments) {
    const commentsData = comments
      .sort((a, b) => (moment(a.get('date')).isAfter(moment(b.get('date'))) ? 1 : -1))
      .map((comment, id) => (
        <CommmentCard          
          key={id}
          {...this.props}
          data={comment.toJS()}
        />
      )).toArray();

    return (
      <View style={styles.commentsContainer}>
        <Text style={styles.commentsTitle}>
          Comments…
        </Text>
        {commentsData}
      </View>
    );
  }

  render() {
    const {
      commentMode,
      data,
      onPressLike,
      onPressComment,
      userId,
      cardBg,
      cardText,
      iconColor,
      iconBg,
    } = this.props;

    const content = data.get('content');
    const likes = data.has('likes') ? data.get('likes') : Map();
    const numberOfLikes = likes.size;
    const liked = likes.has(userId);

    const commentButtonContentColor = commentMode ? "white" : iconColor;

    const comments = data.has('comments') ? data.get('comments') : Map();

    const numberOfComments = comments.size;

    return (
      <View style={styles.notificationContainer}>
        <View style={styles.notificationHeader}>
          <View style={[styles.iconCircle, iconBg]}>
            <MaterialIcon
              name="notifications"
              size={24}
              color="white"
            />
          </View>
          <Text style={[styles.notificationTime, cardText]}>
             {data.has('date') ? moment(data.get('date')).fromNow() : ''}
          </Text>
        </View>
        <View style={[styles.notificationBox, cardBg]}>
          <Text style={[styles.notificationText, cardText]}>
            {content}
          </Text>
          <View style={styles.notificationBottomRow}>
            <View style={styles.notificationLeftButtons}>
              <TouchableOpacity
                style={styles.iconButton}
                onPress={() => onPressLike(liked)}
              >
                <Entypo
                  name={liked ? 'heart' : 'heart-outlined'}
                  size={18}
                  color={iconColor}
                />
                <Text style={[styles.iconButtonText, cardText]}>
                  {numberOfLikes || ''}
                </Text>
              </TouchableOpacity>
              <TouchableOpacity
                style={[
                  styles.iconButton,
                  styles.iconButtonComment,
               //   commentMode ? styles.iconButtonPressed : null,
                  ...(commentMode ? [styles.iconButtonPressed, iconBg] : []),
                ]}
                onPress={() => onPressComment()}
              >
                <MaterialIcon
                  name="comment"
                  size={18}
                  style={{ paddingTop: 2 }}
                  color={commentButtonContentColor}
                />
                <Text style={[
                  styles.iconButtonText,
                  { color: commentButtonContentColor },
                ]}>
                  {numberOfComments || ''}
                </Text>
              </TouchableOpacity>
            </View>
           {/* <TouchableOpacity style={styles.iconButton}>
              <Entypo
                name="dots-three-vertical"
                size={20}
                color={iconColor}
              />
            </TouchableOpacity> */}
          </View>
        </View>
        {commentMode ? this.renderComments(comments) : null}
      </View>
    );
  }
}

const mapStateToProps = state => ({
  cardBg: getBg(state, 'notification', 'background'),
  cardText:  getColor(state, 'notification', 'text'),
  iconBg: getBg(state, 'notification', 'icon'),
  iconColor:  state.getIn(['auth', 'theme', 'notification', 'icon']) || '#2699FB',
});

export default connect(mapStateToProps)(NotificationCard);
