import React, { Component } from 'react';
import {
  View,
  StatusBar,
  Text,
  TouchableOpacity,
  Image,
  Platform,
  ScrollView,
  Dimensions,
} from 'react-native';

import { connect } from 'react-redux';

import { eventsActions } from '../redux/actions';

import styles from '../styles/specialsCard';

import DateCircle from '../components/dateCircle';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class Specials extends Component {
  constructor(props) {
    super(props);
    this.state = {
      numItems: 0,
      index: 0,
    };
    this.continuousScroll = this.continuousScroll.bind(this);
  }

  componentWillMount() {
    const { fetchEvents } = this.props;
    fetchEvents();
    //this.interval = setInterval(this.continuousScroll, 3000);
  }

  // currently disabled
  continuousScroll() {
    numItems = this.state.numItems;
    index = this.state.index + 1;
    if (numItems == index) {
      this.list.scrollTo({x: (index * 202) + 101, animated: true});
      setTimeout(() => this.list.scrollTo({x: 101, animated: false}), 1000);
      index = 0;
    } else {
      this.list.scrollTo({x: (index * 202) + 101, animated: true});
    }
    this.setState({index: index});
  }

  componentWillUnmount() {
    clearInterval(this.interval);
  }


  renderItem(item) {
    const {
      cardBg,
      cardTitle,
      navigate,
    } = this.props;

    const hasImage = item.has('image');

    const image = hasImage ? (
      <Image
        style={[styles.friendCardImage, cardBg]}/*, cardBg]*/
        source={{ uri: item.get('image') }}
      />
    ) : null;

    return (
      <TouchableOpacity
        style={[styles.friendCard, cardBg]} /* , cardBg]*/
        onPress={() => navigate('EventDetails', { id: item.get('id') })}
      >
        <View style={styles.headerContainer}>
          {image}
        </View>

        <View style={styles.cardFooter}>
          <DateCircle small date={item.get('date')} />
          <Text style={[styles.placeName, cardTitle]}
            ellipsizeMode='tail'
            numberOfLines={2}
          >
            {item.get('title')}
          </Text>
        </View>
      </TouchableOpacity>
    );
  }

  render() {
    const {
      events,
      appBg,
      barStyle,
      navBarBgColor,
    } = this.props;

    let specialEvents = events.filter(v => v.get('isStarred')).slice(0, 5);
    // add a second set of Events to allow for smoother scrolling
    specialEvents = specialEvents.toArray();
    if (this.state.numItems == 0 && specialEvents.length != 0) {
        this.setState({numItems: specialEvents.length});
    }

    //specialEvents = specialEvents.concat(specialEvents);

    return (
      <View style={[styles.container, appBg]}>
        <StatusBar
          barStyle={barStyle}
          backgroundColor={navBarBgColor}
        />
        <ScrollView
          horizontal
          style={appBg}
          ref={(ref) => { this.list = ref; }}
          pointerEvents={'box-none'}
          showsHorizontalScrollIndicator = {false}
        >
          { specialEvents.map((item, index) => (
            this.renderItem(item, index)
          ))}
        </ScrollView>
      </View>
    );
  }
}

export default connect(
  state => ({
    events: state.getIn(['events', 'items']),
    navBarBgColor: getNavBarBgColor(state),
    barStyle: getBarStyle(state),
    appBg: getBg(state, 'general', 'background'),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
  }),
  {
    fetchEvents: () => eventsActions.fetchEvents(),
  },
)(Specials);
