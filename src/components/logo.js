import React, { Component } from 'react';
import { Image } from 'react-native';
import { connect } from 'react-redux';

const Logo = ({ logo }) => logo ? (
  <Image
    resizeMode='contain'
    source={{ uri: logo, width: 250, height: 50 }}
    style={{ width: 250, height: 50, backgroundColor: 'transparent' }}
  />
) : null;

export default connect(
  state => ({
    logo: state.getIn(['auth', 'theme', 'title', 'logo']),
  })
)(Logo);
