import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, Text, View, Image, Alert } from 'react-native';
import Modal from 'react-native-modal';

import Icon from 'react-native-vector-icons/MaterialIcons'
import { connect } from 'react-redux';
import moment from 'moment';

import ProgressBar from './progressBar';

import styles from '../styles/loyalty';

import QRCodeScanner from 'react-native-qrcode-scanner';
import Permissions from 'react-native-permissions'

import TwoOptionsDialog from '../components/twoOptionsDialog';

import { rewardsActions } from '../redux/actions';

import {
  getColor,
  getBg,
} from '../utils/theme';

//BMS GK-4 Login error
import firebase from 'react-native-firebase';

const measureDistance = (lat1, lon1, lat2, lon2) => {
  const p = 0.017453292519943295;    // Math.PI / 180
  const c = Math.cos;
  const a = 0.5 - c((lat2 - lat1) * p)/2 +
          c(lat1 * p) * c(lat2 * p) *
          (1 - c((lon2 - lon1) * p))/2;

  return 12742 * Math.asin(Math.sqrt(a)); // 2 * R; R = 6371 km
}

class LoyaltyCard extends Component {

  constructor(props) {
    super(props)
    this.state = {
    }
  }

  componentWillMount() {
    this.props.fetchRewards();
  }

  showCheckinDialog() {
    const {
      props: {
        viewRef,
        screenProps: { modal }
      },
      handleCheckIn,
      handleScanQR,
    } = this;

    modal.setContent(
      <TwoOptionsDialog
        {...this.props}
        title="CHECK IN TO RECEIVE A POINT"
        buttonLeftText="CHECK IN"
        buttonRightText="SCAN QR"
        buttonLeftAction={handleCheckIn.bind(this)}
        buttonRightAction={handleScanQR.bind(this)}
      />,
      'light',
      viewRef,
    );
  }

  handleCheckIn() {
    let result = null;
    let date = null;
//    Permissions && Permissions.request && Permissions.request('location', { type: 'always' }).then(response => {
//       if (response == 'denied') {
//         Alert.alert(
//           'Should enable location service',
// `1. Tap on "Settings" (for your phone)
// 2. Next, tap on "Privacy"
// 3. Tap on "Location Services" then tap on the toggle bar next to "Location Services" so that the color is changed from grey/white to green or blue.
// 4. Under this toggle, you will see a list of apps which are allowed to use Location Services on your phone, please make sure the app is allowed to use it.
// 5. Exit your settings.
// 6. Now you should be able to Check In to receive a point.
// `,
//           [
//             {text: 'OK', onPress: () => console.log('OK Pressed')},
//           ],
//           { cancelable: false }
//         )
//       }
//     });

    navigator.geolocation.getCurrentPosition(
      ({coords: { latitude, longitude }}) => {
        try {
          this.props.rewards
            .filter(v => v.has('location'))
            .forEach((v, id) => {
              const rewardLat = v.getIn(['location', 'lat']);
              const rewardLng = v.getIn(['location', 'lng']);
              const dist = measureDistance(latitude, longitude, rewardLat, rewardLng);
              if (dist < this.props.checkinRadius) {
                //if (this.props.checkinRadius) { /* this allows you to check in anywhere*/}
                result = id;
                date = v.getIn(['rewards', this.props.userId, 'date']);
                throw {};
              }
            });
        }
        catch (e) {}

       this.handleCheckInResult(result, date);
      },
      () => this.props.screenProps.modal.clearContent()
    );
  }

  componentWillReceiveProps(newProps) {
    const { currentPoint, totalPoint, offAmount } = newProps;
    if (currentPoint !== this.props.currentPoint || totalPoint !== this.props.totalPoint) {
      if (currentPoint >= totalPoint) {
        newProps.createLoyaltyReward(offAmount);
        newProps.updatePoints(currentPoint - totalPoint);
      }

    }

  }

  componentDidMount() {

  }

  handleCheckInResult(id, date, isLocation = true) {
    const {
      delay,
      currentPoint,
      totalPoint,
      offAmount,
      viewRef,
    } = this.props;

    // //BMS GK-4 stopping infinite point claim by users without auth
    // //Grabs the parent userid then grabs the child id of user
    // uid = firebase.auth().currentUser.uid? firebase.auth().currentUser.uid:'null' ;
    // var a = '';
    // firebase.database().ref('users').child(uid).once('value', function(snap) {
    //     // this.setState({idField: snap.val()});
    //     a = snap.val();
    //     if(a != uid) {
    //   this.props.screenProps.modal.setContent(
    //     <Text style={{color: 'white', fontSize: 20}}>
    //       Something went wrong! You are not authorized to claim any points. If this seems incorrect, please, contact the developers.
    //     </Text>,
    //     //<Button>Contact</Button>,
    //     undefined,
    //     viewRef,
    //     );
    //   return;
    // }
    // });

    if (id == null) {
      this.props.screenProps.modal.setContent(
        <Text style={{color: 'white', fontSize: 20}}>
          Please make sure your location settings are enabled. Try again later or scan the QR Code.
        </Text>,
        undefined,
        viewRef,
        );
      return;
    }

    if (date) {
      if (moment().diff(moment(date), 'minutes') < delay * 60) {
      //if (!date) { { /* this allows you to check in any time*/}

        this.props.screenProps.modal.setContent(
          <Text style={{color: 'white', fontSize: 20}}>
            You have already checked in today. Please try again on your next visit.
          </Text>,
          undefined,
          viewRef,
          );
        return;
      }
    }

    this.props.checkIn(id);

    let newPoint = currentPoint + 1;

    if (newPoint >= totalPoint) {
      newPoint = newPoint - totalPoint;
      this.props.createLoyaltyReward(offAmount);
    }

    // update check-in time
    this.props.updateLastCheckInTime(date);
    // update total points earned
     this.props.updateTotalPointsEarned();
    // update current points
    this.props.updatePoints(newPoint);

    this.props.screenProps.modal.setContent(
      <View style={{
        borderWidth: 1,
        borderColor:'#2699FB30',
        justifyContent: 'center',
        backgroundColor: 'white',
        alignItems: 'center',
        borderRadius: 10,
        height: 250,
        width: '100%',
      }}>
        <Icon name="check" style={{fontSize: 150, color:'#2699FB'}} />
        <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16, marginBottom: 30}}>
          POINT VALIDATED
        </Text>
      </View>,
      'light',
      this.props.viewRef,
    );
  }

  handleScanQR() {
    this.props.screenProps.modal.setContent(
        <QRCodeScanner
          onRead={(data) => this.handleQRScanned(data.data)}
        />,
        undefined,
        this.props.viewRef,
    );
  }

  handleQRScanned(id) {
    let result = null;
    let date = null;
    try {
      this.props.rewards
        .forEach((v, rid) => {
          if (id == rid) {
            result = id;
            date = v.getIn(['rewards', this.props.userId, 'date']);
            throw {};
          }
        });
    }
    catch (e) {}
    this.handleCheckInResult(result, date, false);
  }


  render() {
    const { currentPoint, totalPoint, offAmount, onPress, cardBg, cardText, cardTitle, backgroundImage } = this.props;
    const remainPoint = totalPoint - currentPoint;

    const pressProps = onPress ?
      { onPress } : {};

    const Wrapper = onPress ? TouchableOpacity : View;

    const Background = backgroundImage ? (
      <Image
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
        }}
        source={{ uri: backgroundImage }}
      />
      ) : null;

    return (
      <Wrapper
        style={[styles.loyaltyCard, cardBg ]}
        { ...pressProps }
      >
        { Background }
        <Text style={[styles.title, cardTitle]}>
          LOYALTY PROGRAM
        </Text>
        <View style={styles.pointsContainer}>
          <Text style={[styles.pointsText, cardText]}>
            {`${currentPoint}/${totalPoint}`}
          </Text>
        </View>
        <Text
          numberOfLines={1}
          ellipsizeMode='tail'
          style={[styles.pointsMessageText, cardText]}
        >
          {`${remainPoint} points remaining until ${offAmount}`}
        </Text>
        <ProgressBar
          style={styles.progress}
          value={currentPoint}
        />
        <TouchableOpacity
          style={styles.checkinButton}
          onPress={() => this.showCheckinDialog()}
        >
          <Text style={styles.buttonText}>
            CHECK-IN
          </Text>
        </TouchableOpacity>
      </Wrapper>
    );
  }
}

const mapDispatchToProps = {
  fetchRewards: () => rewardsActions.fetchRewards(),
  checkIn: id => rewardsActions.checkIn(id),
  updatePoints: point => rewardsActions.updatePoints(point),
  createLoyaltyReward: name => rewardsActions.createLoyaltyReward(name),
  //requestReward,
  //resetPoint,
  //redeemReward

  //BMS
  updateLastCheckInTime: date => rewardsActions.updateLastCheckInTime(date),
  updateTotalPointsEarned: () => rewardsActions.updateTotalPointsEarned(),
}

const mapStateToProps = state => ({
  userId: state.getIn(['auth', 'user', 'id']),
  backgroundImage: state.getIn(['auth', 'settings', 'loyaltyCardBackground']),
  rewards: state.getIn(['rewards', 'items']),
  currentPoint: Number(state.getIn(['auth', 'user', 'point']) || 0),
  totalPoint: Number(state.getIn(['auth', 'settings', 'points']) || 1),
  offAmount: state.getIn(['auth', 'settings', 'reward']) || '$2 OFF',
  checkinRadius: Number(state.getIn(['auth', 'settings', 'geoRadius']) || 0.1),
  delay: Number(state.getIn(['auth', 'settings', 'delay']) || 1),
  cardBg: getBg(state, 'loyal', 'background'),
  cardTitle: getColor(state, 'loyal', 'title'),
  cardText:  getColor(state, 'loyal', 'text'),
  appTitle: getColor(state, 'general', 'title'),
  buttonBg:  getBg(state, 'button', 'background'),
  buttonText: getColor(state, 'button', 'text'),
});


export default connect(mapStateToProps, mapDispatchToProps)(LoyaltyCard);
