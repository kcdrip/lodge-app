import React, { Component } from 'react';
import { View, StyleSheet, ScrollView, Dimensions } from 'react-native';
//import { Actions } from 'react-native-router-flux';
const Actions = {};

import TopNavBar from './top';
import BottomNavBar from './bottom';

const { height, width } = Dimensions.get('window');

export default class NavBar extends Component {

    constructor (props) {
        super(props)

        this.onMore = this.onMore.bind(this);
    }
    
    _onScrollToEnd() {

        this.scrollView.scrollTo(height-250);
    }

    onMore() {
        if (this.props.showBack === 'no') {
            this.scrollView.scrollToEnd({animated: true});
        } else {
            Actions.Home({scroll: 1});
        }
    }

    render() {
        return (
            <View style={{flex: 1}}>
                <View style={{height: 48}}><TopNavBar showBack={this.props.showBack}/></View>
                <ScrollView ref={ref=>this.scrollView = ref} style={{height: height - 124, backgroundColor: global.theme.general.background}}>
                    {this.props.children}
                </ScrollView>
                <View style={{height: 56}}><BottomNavBar onMore={this.onMore}/></View>
            </View>
        )
    }
}