import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import {  StyleSheet, Text, View, TouchableOpacity } from 'react-native';
import { Icon } from 'native-base';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

const homeIcon = <MaterialCommunityIcons name='view-dashboard-variant' style={{fontSize: 20, color: 'white'}}/>;
const notificationIcon = <MaterialIcons name="notifications" style={{fontSize: 20, color: 'white'}}/>
const rewardsIcon = <Entypo name={'star'} style={{fontSize: 20, color: 'white'}}/>;
const calendarIcon = <MaterialCommunityIcons name={'calendar-range'} style={{fontSize: 20, color: 'white'}}/>;
const moreIcon = <Entypo name={'dots-three-vertical'} style={{fontSize: 20, color: 'white'}}/>;

class NavBarItem extends Component {

	static propTypes = {
		onPressed: PropTypes.func,
		icon: PropTypes.object,
		name: PropTypes.string,
		id: PropTypes.number,
	};

	render() {
		const { icon, name, id, onPressed } = this.props;
		const itemStyle = styles.item;

		return (
			<TouchableOpacity style={itemStyle} onPress={onPressed}>
				{icon}
				<Text style={[styles.itemText, {color: global.theme.bottom.text}]}>{name}</Text>
			</TouchableOpacity>
		)
	}
}

export default class BottomNavBar extends Component {
	
	render() {

		return (
			<View style={[styles.bottomNavBar, {backgroundColor: global.theme.bottom.background}]}>
				<NavBarItem id={1} icon={homeIcon} name={'Home'} onPressed={()=>Actions.Home()}/>
				<NavBarItem id={2} icon={notificationIcon} name={'Notification'} onPressed={()=>Actions.Notifications()} />
				<NavBarItem id={3} icon={rewardsIcon} name={'Rewards'} onPressed={()=>Actions.Rewards()} />
				<NavBarItem id={4} icon={calendarIcon} name={'Calendar'} onPressed={()=>Actions.Calendar()}/>
				<NavBarItem id={5} icon={moreIcon} name={'More'} onPressed={()=>this.props.onMore()} />
			</View>
		)
	}
}

const styles = StyleSheet.create({
	bottomNavBar: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-around',
		alignItems: 'center',
		backgroundColor: '#2699FB',
	    overflow: 'hidden',
	    width: '100%',
	    height: 56,
	},
	item: {
		height: '100%',
		flex: 1,
		flexDirection: 'column',
		justifyContent: 'center',
		alignItems: 'center',
	},
	itemText: {
        color: 'white',
        fontSize: 9,
	}
});