import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, View, TouchableOpacity } from 'react-native';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { Icon } from 'native-base';
import Feather from 'react-native-vector-icons/Feather';

export default class TopNavBar extends Component {

	_onGotoProfile() {
		Actions.Profile();
	}

	render() {

		return (
			<View style={[styles.topNavbar, {backgroundColor: global.theme.title.background}]}>
                {this.props.showBack !== 'no' && <TouchableOpacity style={styles.backBtn} onPress={() => {Actions.pop();}}>
					<Feather name='arrow-left' style={{color: global.theme ? global.theme.title.text : 'white', fontSize: 20}}/>
				</TouchableOpacity>}
				<Text style={styles.logo}>LOGO</Text>
				<TouchableOpacity style={styles.avatar} onPress={this._onGotoProfile}>
					<Icon name='person' style={{fontSize: 20, color: 'white'}} />
				</TouchableOpacity>
			</View>
		);
	}
}


const styles = StyleSheet.create({
	topNavbar: {
		// backgroundColor: '#3366FF',
		backgroundColor: global.theme ? global.theme.title.background : '#3366FF',
		height: 48,
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'center',
		//padding: 10
	},
  	logo: {
		flex: 1,
		color: 'white',
		fontSize: 14,
		fontFamily: 'Arial',
		fontWeight: 'bold',
        textAlign: 'center',
    },
	backBtn: {
		// position: 'absolute',
		height: 30,
		width: 30,
		// top: 6,
		// left: 10,
		marginLeft: 10,
		justifyContent: 'center',
		alignItems: 'center'
	},
  	avatar: {
		position: 'absolute',
		height: 30,
		width: 30,
		top: 9,
  		right: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		borderRadius: 15,
  	}
});