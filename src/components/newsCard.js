import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Image,
  Text,
  View,
} from 'react-native';

import { connect } from 'react-redux';

import styles from '../styles/news';


import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

class NewsCard extends Component {
  render() {
    const {
      data,
      id,
      navigate,
      cardBg,
      cardTitle,
      cardSubtitle,
    } = this.props;

    const hasImage = data.has('image');

    const image = hasImage ? (
      <Image
        style={styles.newsCardImage}
        source={{ uri: data.get('image') }}
      />
    ) : null;


    return (
      <TouchableOpacity
        key={id}
        style={[
          styles.newsCard, 
          (data.get('subtitle') || '') !== '' ? null : styles.newsCardWithoutSubtitle,
          hasImage ? null : styles.newsCardWithoutImage, 
          cardBg,
          ]}
        onPress={() => navigate('NewsletterPost', { id })}
      >
        {image}
        <View style={styles.cardTitleContainer}>
          <Text style={[styles.newsCardTitle, cardTitle]}>
            {data.get('title')}
          </Text>
        </View>
        <Text
          style={[styles.newsCardSubtitle, cardSubtitle]}
          ellipsizeMode="tail"
          numberOfLines={4}
        >
          {data.get('subtitle')}
        </Text>
      </TouchableOpacity>
    );
  }

}

export default connect(
  state => ({
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubtitle: getColor(state, 'card', 'subtitle'),
  }),
)(NewsCard);
