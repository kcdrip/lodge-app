import React from 'react';
import PropTypes from 'prop-types';
import { Text, View } from 'react-native';
import { connect } from 'react-redux';

import moment from 'moment';

import styles from '../styles/common';

import {
  getColor,
  getBg,
} from '../utils/theme';

const DateCircle = ({
  date,
  small,
  style,
  background,
  color,
}) => (
  <View style={[
    style,
    styles.dateCircle,
    background,
    small ? styles.dateCircleSmall : null,
  ]}
  >
    <Text style={[
      styles.dateCircleText,
      color,
      small ? styles.dateCircleTextSmall : null,
    ]}
    >
      {moment(date).format('M/D')}
    </Text>
  </View>
);

DateCircle.propTypes = {
  date: PropTypes.string.isRequired,
  small: PropTypes.bool,
  style: PropTypes.any,
};

DateCircle.defaultProps = {
  small: false,
  style: null,
};


const mapStateToProps = state => ({
  background:  getBg(state, 'datecircle', 'background'),
  color:  getColor(state, 'datecircle', 'text'),
});

export default connect(mapStateToProps)(DateCircle);
