import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image, StyleSheet, Text, NavigatorIOS } from 'react-native';

import DateInCircle from '../common/date-in-circle';

export default class EventCard extends Component {

	render() {
		const { day, month, title, time, onPress } = this.props;

		return (
			<TouchableOpacity style={[styles.container, {backgroundColor: global.theme.box.background}]} onPress={onPress}>
				<DateInCircle isSmall={true} day={day} month={month} />
				<Text style={[styles.title, {color: global.theme.box.title}]}>{title}</Text>
				<Text style={[styles.time, {color: global.theme.box.title}]}>></Text>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		height: 57,
		backgroundColor: global.theme ? global.theme.box.background : '#F1F9FF',
		alignItems: 'center',
		padding: 20,
		marginTop: 10,
	},
	title: {
        color: global.theme ? global.theme.box.title : '#2699FB',
        fontSize: 20,
		width: '70%',
	},
	time: {
        color: global.theme ? global.theme.box.title : '#2699FB',
        fontSize: 20,
	}
})