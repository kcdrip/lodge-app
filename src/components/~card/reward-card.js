import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, View } from 'react-native';

export default class RewardCard extends Component {
    static propTypes = {
        title: PropTypes.string,
        onRedeem: PropTypes.func,
    }

    render() {
        const { title } = this.props;

        return (
            <View style={[styles.container, {backgroundColor: global.theme.card.background}]}>
                <Text style={[styles.title, {color: global.theme.card.title}]}>{title}</Text>
                <Text style={[styles.content, {color: global.theme.card.subtitle}]}>Please show your cashier/server to redeem</Text>
                <TouchableOpacity style={styles.button} onPress={this.props.onRedeem}>
                    <Text style={styles.buttonText}>REDEEM</Text>
                </TouchableOpacity>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'space-around',
        height: 160,
        backgroundColor: '#BCE0FD',
        alignItems: 'center',
        padding: 20,
        marginBottom: 15,
    },
    title: {
        color: '#2699FB',
        fontSize: 20,
    },
    content: {
        color: 'black',
        fontSize: 14,
    },
    button: {
        justifyContent: 'center',
        alignItems: 'center',
        width: '95%',
        height: 38,
        backgroundColor: '#2699FB',
        borderRadius: 5,
    },
    buttonText: {
        fontFamily: 'arial',
        fontWeight: 'bold',
        fontSize: 17,
        color: 'white',
    },
})