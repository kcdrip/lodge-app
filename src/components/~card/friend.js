import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, Image, StyleSheet, Text, View } from 'react-native';
import { connect } from 'react-redux';

import { gotoScreen } from '../../reducers/navigation';

const styles = StyleSheet.create({
	container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		height: 57,
		backgroundColor: '#F1F9FF',
		alignItems: 'center',
		padding: 20,
	},
	title: {
        color: '#2699FB',
        fontSize: 14,
		width: '70%',
	},
	time: {
        color: '#2699FB',
        fontSize: 20,
	}
});

export default class FriendCard extends Component {
	
	render() {
		const { onPress } = this.props;

		return (
            <View style={{marginBottom: 20}}>
                <View style={{height: 156, width: '100%', backgroundColor: '#BCE0FD'}} />
                <TouchableOpacity style={[styles.container, {backgroundColor: global.theme.box.background}]} onPress={onPress}>
                    <Text style={[styles.title, {color: global.theme.box.title}]}>{this.props.name}</Text>
                    <Text style={[styles.time, {color: global.theme.box.title}]}>></Text>
                </TouchableOpacity>
            </View>
		)
	}
}
