import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Image, StyleSheet, Text, TouchableOpacity } from 'react-native';

import DateInCircle from '../common/date-in-circle';

export default class TopEventCard extends Component {
	
	render() {
		const { day, month, title, time, imgSrc, onPress } = this.props;

		return (
			<TouchableOpacity style={[styles.container]} onPress={onPress}>
				{/* <Image style={{ width: '100%', height: 160 }} source={{ uri: imgSrc }} /> */}
				<DateInCircle isSmall={false} day={day} month={month} />
				<Text style={styles.title}>{title}</Text>
				<Text style={styles.time}>{time}</Text>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	container: {
		flex: 1,
		height: 160,
		backgroundColor: global.theme ? global.theme.card.background : '#D1D9DF',
		alignItems: 'center',
		padding: 20,
	},
	title: {
        color: global.theme ? global.theme.card.title : 'white',
        fontSize: 29,
	},
	time: {
        color: global.theme ? global.theme.card.subtitle : 'white',
        fontSize: 17,
	}
})