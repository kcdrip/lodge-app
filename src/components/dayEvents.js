import React, { Component } from 'react';
import PropTypes from 'prop-types';

import { TouchableOpacity, Text, View, Share, Image } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';

import styles from '../styles/dayEvents';
import calendarStyles from '../styles/calendar';
import commonStyles from '../styles/common';
import { getTimeFrame } from '../utils/selectors';

import { addToCalendar } from '../utils/events';

import { eventsActions } from '../redux/actions';

import {
  getColor,
  getBg,
} from '../utils/theme';

const EventCard = ({ event, onPress }) => {

  const timeLabel = getTimeFrame(event) || 'ALL-DAY';

  return (
    <View style={styles.eventCard}> 
      <View style={{width: 55}}>
        <Text style={styles.card_text}>
          {timeLabel}
        </Text>
      </View>
      <TouchableOpacity
        style={styles.eventCardButton}
        onPress={onPress}
      >
        <Text style={styles.title}>
          {event.get('title')}
        </Text>
      </TouchableOpacity>
    </View>
  );
};

class DayEvents extends Component {
  componentWillMount() {
    this.props.fetchSpecials();
  }

  handleAddToCalendar(event) {

    const {
      props: {
        viewRef,
        screenProps: { modal },
      },
    } = this;


    const toCalDate = v => `${moment(v).format(moment.HTML5_FMT.DATETIME_LOCAL_MS)}Z`;
    const toTime = v => moment.utc(v, 'HH:mmA').format('HH:mm');

    const startTime = event.get('from');
    const endTime = event.get('to');

    const allDay = startTime == null && endTime == null;

    let startDate;
    let endDate;

    if (allDay) {
      startDate = toCalDate(event.get('date'));
      endDate = startDate;
    } else {
      const date = moment(event.get('date')).format('YYYY-MM-DD');
      startDate = toCalDate(`${date} ${toTime(startTime)}`);
      endDate = toCalDate(`${date} ${toTime(endTime)}`);
    }

    const id = `${event.get('id')}`;

    console.log('addToCalendar de', {
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    });

    addToCalendar({
      id,
      title: event.get('title'),
      startDate,
      endDate,
      allDay,
    }).then(v => modal.setContent(
      <Text style={{color: 'white', fontSize: 20}}>
        Event "{event.get('title')}" added to calendar
      </Text>,
      undefined,
      viewRef
    ));
  }

  showEventModal(event, calendarShare = false) {
    const {
      props: {
        buttonBg,
        buttonText,
        title,
        subtitle,
        viewRef,
        screenProps: { modal } 
      },
      
    } = this;

    const timeFrame = getTimeFrame(event);
    const subTitle = timeFrame ? `(${timeFrame})` : moment(event.get('date')).format('l');

    const addToCalendar = calendarShare ? (
      <TouchableOpacity
        style={[commonStyles.modalButton, commonStyles.modalButtonCalendar, buttonBg]}
        onPress={() => this.handleAddToCalendar(event)}
      >
       <Text style={[commonStyles.modalButtonText, buttonText]}>
          ADD TO CALENDAR
        </Text>
      </TouchableOpacity>
    ) : null;

    const EventImage = event.has('image') ? (
      <Image 
        style={{
          left: 0,
          top: 0,
          right: 0,
          height: 150,
          borderRadius: 3,
          marginBottom: 5,
        }}
        source={{ uri: event.get('image') }}
      />
      ) : null;

    modal.setContent((
      <View style={styles.modalContainer}>
        <View style={styles.modal}>
          { EventImage }

          <Text style={[styles.modalTitle, title]}>
            {event.get('title')}
          </Text>
          <Text style={[styles.modalSubTitle, subtitle]}>
            {subTitle}
          </Text>
          <Text style={styles.modalText}>
            {event.get('details') || event.get('content')}
          </Text>
          <View style={styles.modalButtonContainer}>
            <TouchableOpacity
              style={[commonStyles.modalButton, commonStyles.modalButtonShare, buttonBg]}
              onPress={() => this.shareEvent(event)}
            >
              <Text style={[commonStyles.modalButtonText, buttonText]}>
                SHARE
              </Text>
            </TouchableOpacity>
            {addToCalendar}
          </View>
        </View>
      </View>
    ),
    'light', viewRef );
  }

  shareEvent(event) {
    const {
      props: {
        viewRef,
        screenProps: { modal },
      },
    } = this;

    modal.setContent(<View />, 'light', viewRef);

    Share.share({
      title: event.get('title'),
      message: event.get('details') || event.get('content') || event.get('title'),
    }).then(() => modal.clearContent());
  }

  render() {
    const {
      dayEvents,
      specials,
    } = this.props;

    const dailySpecial = specials.get(moment().isoWeekday() - 1);

    const dailySpecialComponent = dailySpecial ? (
      <TouchableOpacity
        onPress={() => this.showEventModal(dailySpecial)}
        style={[calendarStyles.dataBox, calendarStyles.dailySpecialBox, {marginLeft: 0, marginRight: 0}]}
      >
        <Text style={calendarStyles.dailySpecialTitle}>
          {dailySpecial.get('title')}
        </Text>
        <Text style={calendarStyles.dailySpecialText}>
          {dailySpecial.get('content')}
        </Text>
      </TouchableOpacity>
    ) : null;

    const dayEventsComponent = dayEvents.map((event, id) => (
      <EventCard
        key={id}
        event={event}
        onPress={() => this.showEventModal(event, true)}
      />
    )).toArray();

    return (
      <View style={styles.container}>
        {dailySpecialComponent}
        {dayEventsComponent}
      </View>
    );
  }
}

const today = moment().format('YYYY-MM-DD');

const parseTime = v => v.has('from') && v.get('from') !== '' ? moment.utc(v.get('from'), 'HH:mmA') : null;

export default connect(
  state => ({
    dayEvents: state.getIn(['events', 'items'])
      .filter(v => moment(v.get('date')).format('YYYY-MM-DD') === today)
      .sort((a, b) => {
        const ma = parseTime(a);
        const mb = parseTime(b);
        return ma == null && mb == null ? 0 :
          ma == null ? -1 :
          mb == null ? 1 :
          ma.diff(mb);
        }),
    specials: state.getIn(['events', 'specials']),
    buttonText: getColor(state, 'button', 'text'),
    buttonBg:  getBg(state, 'button', 'background'),
    title: getColor(state, 'general', 'title'),
    subtitle: getColor(state, 'general', 'subtitle'),
  }),
  {
    fetchEvents: () => eventsActions.fetchEvents(),
    fetchSpecials: () => eventsActions.fetchSpecials(),
  },
)(DayEvents);
