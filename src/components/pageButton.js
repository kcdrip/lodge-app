import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, Text, TouchableOpacity, Image } from 'react-native';

export default class PageButton extends Component {
  static propTypes = {
    name: PropTypes.string.isRequired,
    onPress: PropTypes.func
  }

  render() {
    const { name, onPress, left, style, textStyle, backgroundImage  } = this.props;

     const Background = backgroundImage ? (
      <Image
        style={{
          position: 'absolute',
          left: 0,
          top: 0,
          right: 0,
          bottom: 0,
        }}
        source={{ uri: backgroundImage }}
      />
      ) : null;

    return (
      <TouchableOpacity
        style={[styles.notification, left ? styles.notificationLeft : styles.notificationRight, style]}
        onPress={onPress}
      >
        { Background }
        <Text style={[styles.notificationText, textStyle]}>{name}</Text>

      </TouchableOpacity>
    )
  }
}
/*    adjustsFontSizeToFit: true,*/
const styles = StyleSheet.create({
  notification: {
    height: 120,
    flex: .5,
    backgroundColor: '#F1F9FF',
    justifyContent: 'center',
    alignItems: 'center',
  },
  notificationLeft: {
    marginRight: 8.5,
  },
  notificationRight: {
    marginLeft: 8.5,
  },
  notificationText: {
    margin: 10,
    fontSize: 15,
    fontFamily: 'arial',
    fontWeight: 'bold',
    color: '#2699FB',
    textAlign: 'center',
     textShadowOffset: {
      width: 2,
      height: 2,
    },
    textShadowRadius: 3,
    textShadowColor: '#333',
  },
});
