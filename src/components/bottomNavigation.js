import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {
  Text,
  View,
  TouchableOpacity,
} from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import { connect } from 'react-redux';

import moment from 'moment';

import styles from '../styles/navigation';

const NavBarItem = ({
  icon,
  name,
  onPress,
  color,
  dot,
}) => (
  <TouchableOpacity
    style={styles.item}
    onPress={onPress}
  >
    {icon}
    <Text style={[styles.itemText, { color }]}>{name}</Text>
    {dot ? <View style={styles.redDot} /> : null}
  </TouchableOpacity>
);

NavBarItem.propTypes = {
  onPress: PropTypes.func.isRequired,
  icon: PropTypes.object.isRequired,
  name: PropTypes.string.isRequired,
};

const BottomNavBar = ({
  onMore,
  navBg,
  navTextColor,
  hasNotifications,
  navigation: {
    navigate,
  },
  screenProps: {
    homePage: {
      scrollToBottom,
    },
  },
}) => (
  <View style={[styles.bottomNavBar, navBg]}>
    <NavBarItem
      icon={
        <MaterialCommunityIcons
          name="view-dashboard-variant"
          size={20}
          color={navTextColor}
        />
      }
      color={navTextColor}
      name="Home"
      onPress={() => navigate('Home')}
    />
    <NavBarItem
      icon={
        <MaterialIcons
          name="notifications"
          size={20}
          color={navTextColor}
        />
      }
      color={navTextColor}
      name="Notification"
      dot={hasNotifications}
      onPress={() => navigate('Notifications')}
    />
    <NavBarItem
      icon={
        <Entypo
          name="star"
          size={20}
          color={navTextColor}
        />
      }
      color={navTextColor}
      name="Rewards"
      onPress={() => navigate('Loyalty')}
    />
    <NavBarItem
      icon={
        <MaterialCommunityIcons
          name="calendar-range"
          size={20}
          color={navTextColor}
        />
      }
      color={navTextColor}
      name="Calendar"
      onPress={() => navigate('Calendar')}
    />
    <NavBarItem
      icon={
        <Entypo
          name="dots-three-vertical"
          size={20}
          color={navTextColor}
        />
      }
      color={navTextColor}
      name="More"
      onPress={() => {
        if (typeof onMore === 'function') {
          onMore();
        } else {
          navigate('Home');
          scrollToBottom();
        }
      }}
    />
  </View>
);

BottomNavBar.propTypes = {
  navigation: PropTypes.any.isRequired,
  onMore: PropTypes.func,
};

BottomNavBar.defaultProps = {
  onMore: null,
};

const mapDispatchToProps = {}

const mapStateToProps = state => {
  const notifications = state
    .getIn(['notifications', 'items'])
    .filter(v => moment().isAfter(moment(v.get('date'))));
  const userId = state.getIn(['auth', 'user', 'id']);
  const hasNotifications = notifications.reduce((p, n) => !n.hasIn(['readers', userId]) ? true : p, false);

  return {
    hasNotifications,
    navBg: state.hasIn(['auth', 'theme', 'bottom', 'background']) ? {
      backgroundColor: state.getIn(['auth', 'theme','bottom', 'background']) 
    } : null,
    navTextColor: state.hasIn(['auth', 'theme', 'bottom', 'text']) ? 
      state.getIn(['auth', 'theme','bottom', 'text']) : 'white',
    }
  };

export default connect(mapStateToProps, mapDispatchToProps)(BottomNavBar);

