import React, { Component } from 'react';
import {
  StyleSheet,
  View, 
  TouchableOpacity,
  Text,
} from 'react-native';


const styles = StyleSheet.create({
  modalContainer: {
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
    borderRadius: 10,
    height: 230,
    width: '100%',
    padding: 30,
  },
  innerContainer: {
    flexDirection: 'row',
    marginTop: 30,
    width: '100%',
    justifyContent: 'space-around',
  },
  title: {
    color: '#2699FB', 
    fontFamily: 'arial',
    fontSize: 16,
    fontWeight: 'bold'
  },
  button: {
    width: 96,
    height: 40,
    borderRadius: 4,        
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2699FB',
  }, 
  button_white: {
      borderColor: '#2699FB',
      borderWidth: 1,
      backgroundColor: 'white',
  },
  dialogButtonText: {
    fontSize: 10,
    fontWeight: 'bold',
    fontFamily: 'arial',
    color: 'white'
  },
  dialogButtonWhiteText: {
    color: '#2699FB'
  }
});

class TwoOptionsDialog extends Component {
  render() {
    const {
      title,
      buttonLeftText,
      buttonRightText,
      buttonLeftAction,
      buttonRightAction,
      appTitle,
      buttonBg,
      buttonText,
    } = this.props;

    const invButton = buttonBg != null && buttonText != null ? ({
      borderColor: buttonBg.backgroundColor,
      backgroundColor: buttonText.color,
    }) : null;

    const invButtonText = buttonBg != null && buttonText != null ? ({
      color: buttonBg.backgroundColor,
    }) : null;

    return (
      <View style={styles.modalContainer}>    
        <Text style={[styles.title, appTitle]}>
          {title}
        </Text>
          <View style={styles.innerContainer}>
            <TouchableOpacity
                style={[styles.button, buttonBg]}
                onPress={() => buttonLeftAction()} >
              <Text style={[styles.dialogButtonText, buttonText]}>
                {buttonLeftText}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity
                style={[styles.button, styles.button_white, invButton]}
                onPress={() => buttonRightAction()} >
              <Text style={[styles.dialogButtonText, styles.dialogButtonWhiteText, invButtonText]}>
                {buttonRightText}
              </Text>
            </TouchableOpacity>
          </View>
   
        </View>
    ) ;
  }
}

export default TwoOptionsDialog;
