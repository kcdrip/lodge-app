import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TextInput } from 'react-native';

export default class CustomInput extends Component {
 
    state = { text: '' };

    render() {
        const { width, height, placeholder, background, styles, password, multiline } = this.props;

        return (
            <TextInput
                style={[{padding: 10, paddingHorizontal: 30, margin: 6, borderRadius: 20, height: this.props.height, borderColor: '#BCE0FD', borderWidth: 1, width: this.props.width, color: '#2699FB'},
                        styles]}
                placeholder={placeholder}
                secureTextEntry={password==='yes'}
                multiline={multiline==='yes'?true:false}
                onChangeText={(text) => this.setState({text: text})}
                value={this.state.text}
            />
        )
    }
}

CustomInput.defaultProps = {
    width: '80%',
    height: 48,
    placeholder: '',
    background: false,
}