import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Image, Text, View, NavigatorIOS } from 'react-native';

export default class DateInCircle extends Component {

	render() {
		const { day, month, isSmall } = this.props;
		const size = isSmall ? 37 : 52;
		const fontSize = isSmall ? 12 : 18;

		return (
			<View style={{
				height: size,
				width: size,
				justifyContent: 'center',
				alignItems: 'center',
				backgroundColor: '#3366FF',
				borderRadius: size/2,
			}}>
				<Text style={{color: 'white', fontSize: fontSize}}>{month}/{day}</Text>
			</View>
		)
	}
}
