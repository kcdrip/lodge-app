import React, { Component } from 'react';
import { StyleSheet, View, Picker, TouchableOpacity, Text } from 'react-native';
import moment from 'moment';
import Modal from 'react-native-modal';

const styles = StyleSheet.create({
    modalContainer: {
		// justifyContent: 'center',
		// backgroundColor: 'white',
		borderRadius: 10,
		padding: 20,
		height: 250,
	},
	innerContainer: {
		position: 'relative',
		flexDirection: 'row',
		flex: 1,
		// alignItems: 'center',
		justifyContent: 'space-between'
	},
	button: {
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		height: 51,
		width: 67,
		marginTop: 80
	},
	buttonText: {
        color: 'white',
        fontSize: 8,
		fontFamily: 'Arial',
	},
});

const monthName = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

export default class YearMonthSelector extends Component {

	state = {
			year: moment(new Date()).year(),
			month: moment(new Date()).month()
	}

	render() {

		const { isVisible, onClose, setYearMonth } = this.props;

		return (
			<Modal isVisible={isVisible}
				backdropColor={'white'}
				backdropOpacity={0.8}
				onBackdropPress={onClose}
				>
				<View style={styles.modalContainer}>
					<View style={styles.innerContainer}>
						<View>
							<Picker
								selectedValue={this.state.year}
								style={{ height: 50, width: 100 }}
								onValueChange={(itemValue, itemIndex) => this.setState({year: itemValue})}>
								<Picker.Item label={'2017'} value={2017} />
								<Picker.Item label={'2018'} value={2018} />
								<Picker.Item label={'2019'} value={2019} />
							</Picker>
						</View>
						<View>
							<Picker
								selectedValue={this.state.month}
								style={{ height: 50, width: 100 }}
								onValueChange={(itemValue, itemIndex) => this.setState({month: itemValue})}>
								{
									monthName.map((month, id) => (
										<Picker.Item key={id} label={month} value={id} />
									))
								}
							</Picker>
						</View>
						<TouchableOpacity style={styles.button} onPress={(e) => setYearMonth(this.state.year, this.state.month)}>
							<Text style={styles.buttonText}>SELECT</Text>
						</TouchableOpacity>
					</View>
				</View>
			</Modal>            
		)
	}
}