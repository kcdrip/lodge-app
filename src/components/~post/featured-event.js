import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, StyleSheet, Text, NavigatorIOS } from 'react-native';
import { connect } from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';
import moment from 'moment';

import DateInCircle from '../common/date-in-circle';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20
    },
    reactangle: {
        position: 'relative',
        width: '100%',
        height: 180,
        backgroundColor: '#D1D9DF',
        justifyContent: 'center',
        alignItems: 'center',
    },

});

const getHourInString = (hh) => {
    if (hh < 12) {
        return hh + 'am';
    } else {
        if (hh === 12) return '12pm';
        else return (hh-12) + 'pm';
    }
};

export default class FeaturedEvent extends Component {
    
    render() {
        const { id, title, timeFrom, timeTo, month, day, content, onSelect } = this.props;
        var from = moment(timeFrom).format('hhA');
        var to = moment(timeTo).format('hhA');

        var period = from + ' - ' + to;

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.reactangle} onPress={onSelect}>
                    <Entypo name='image' style={{fontSize: 68, color: '#7FC4FD'}} />
                </TouchableOpacity>
                <View style={{flexDirection: 'row', paddingHorizontal: 20, paddingVertical: 10}}>
                    <DateInCircle day={day} month={month} isSmall={false} />
                    <View style={{marginLeft: 20}}>
                        <Text style={{fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: '#2699FB'}}>{title}</Text>
                        <Text style={{fontFamily: 'Arial', fontWeight: 'normal', fontSize: 14, color: '#2699FB'}}>({period})</Text>
                    </View>
                </View>
                <Text style={{fontFamily: 'Arial', fontWeight: 'normal', fontSize: 14, color: '#2699FB', paddingHorizontal: 20, paddingVertical: 10}}>
                    {content}
                </Text>
            </View>
        )
    }
}