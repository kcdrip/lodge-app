import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, TouchableOpacity, Image, StyleSheet, Text, NavigatorIOS } from 'react-native';
import { connect } from 'react-redux';
import Entypo from 'react-native-vector-icons/Entypo';

import DateInCircle from '../common/date-in-circle';
import { gotoScreen } from '../../reducers/navigation';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginBottom: 20
    },
    reactangle: {
        position: 'relative',
        width: '100%',
        height: 180,
        backgroundColor: '#D1D9DF',
        justifyContent: 'center',
        alignItems: 'center',
    },

});

class Newsletter extends Component {

    render() {
        const { id, subTitle, title, onSelect } = this.props;

        return (
            <View style={styles.container}>
                <TouchableOpacity style={styles.reactangle} onPress={onSelect}>
                    <Entypo name='image' style={{fontSize: 68, color: '#7FC4FD'}} />
                </TouchableOpacity>
                <View style={{padding: 20, backgroundColor: '#F1F9FF'}}>
                    <Text style={{fontFamily: 'Arial', fontWeight: 'bold', fontSize: 20, color: '#2699FB', paddingHorizontal: 20, paddingVertical: 10}}>
                        {title}
                    </Text>
                    <Text style={{fontFamily: 'Arial', fontWeight: 'normal', fontSize: 14, color: '#2699FB', paddingHorizontal: 20, paddingVertical: 10}}>
                        {subTitle}
                    </Text>
                </View>
            </View>
        )
    }
}

const mapDispatchToProps = {
    gotoScreen,
}

export default connect(null, mapDispatchToProps)(Newsletter);