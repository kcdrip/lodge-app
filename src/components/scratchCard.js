import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import scratchImage from '../assets/images/scratch.png';

import styles from '../styles/common';

import {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
} from '../utils/theme';

//react-native-scratch-card
//react-native-scratchcard

const REDEEM_QUESTION_TEXT = 'Are you sure you want to\nredeem your ';

class ScratchCard extends Component {
  
  showScratchOff() {
    const {
      props: {
        text,
        screenProps: { modal },
      },
    } = this;


    modal.setContent(
      <View style={[styles.modalContainer, styles.modalContainerScratch]}>
        <Text style={styles.modalText}>
          SCRATCH & WIN
        </Text>
        <Text style={styles.offerSubText}>
          {text}
        </Text>
        <TouchableOpacity
          style={styles.scratchCardContainer}
          onPress={() => this.showRewardRedeemed()}
        >
          <Image source={scratchImage} />
        </TouchableOpacity>          
      </View>,
      'light',
    );
  }

  showRewardRedeemed() {
    const {
      props: {
        data,
        screenProps: { modal },
      },
    } = this;

    modal.setContent(
      <View style={[styles.modalContainer, styles.modalContainerRedemption]}>
        <MaterialIcon
          name="check"
          size={130}
          color="#2699FB"
        />
        <Text style={styles.modalText}>
          Reward redeemed!
        </Text>
        <View style={styles.modalButtonsContainer}>
          <TouchableOpacity
            style={styles.modalRoundButton}
            onPress={() => modal.clearContent()}
          >
            <MaterialIcon
              name="close"
              size={20}
              color="#2699FB"
            />
          </TouchableOpacity>          
        </View>
      </View>,
      'light',
    );
  }
  
  render() {
    const {
      text,
      cardBg,
      cardTitle,
      cardSubitle,
    } = this.props;


    return (
      <View style={[styles.offerContainer, cardBg]}>
        <Text style={[styles.offerTitle, cardTitle]}>
          SCRATCH & WIN
        </Text>
        <Text style={[styles.comeInText, cardSubitle]}>
          {text}
        </Text>
        <TouchableOpacity
          style={styles.redeemButton}
          onPress={() => this.showScratchOff()}
        >
          <Text style={styles.redeemButtonText}>
            START
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}

ScratchCard.propTypes = {
  text: PropTypes.string.isRequired,
  won: PropTypes.bool.isRequired,
};

export default connect(
  state => ({
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubitle: getColor(state, 'card', 'subtitle'),
  }),
  {
    getRewardForOffer: data => rewardsActions.getRewardForOffer(data),
  },
)(ScratchCard);
