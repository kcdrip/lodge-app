import React from 'react';
import PropTypes from 'prop-types';
import {
  TouchableOpacity,
  Image,
  View,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { connect } from 'react-redux';

const HeaderLogoRaw = ({ logo }) => logo ? (
  <Image
    resizeMode='contain'
    source={{ uri: logo, width: 200, height: 30 }}
    style={{ width: 200, height: 30, backgroundColor: 'transparent' }}
  />
) : null;

const HeaderLogo = connect(
  state => ({
    logo: state.getIn(['auth', 'theme', 'title', 'logo']),
  })
)(HeaderLogoRaw);


const ArrowRaw = ({ color }) => (
  <Icon
    name="md-arrow-back"
    size={24}
    color={color}
  />
);

const Arrow = connect(
  state => ({
    color: state.getIn(['auth', 'theme', 'title', 'darkStatusBar']) ? 'black' : 'white',
  })
)(ArrowRaw);


const UserIconComponent = ({ image, navigation }) => (
  <TouchableOpacity
    onPress={() => navigation.navigate('Profile')}
    style={{
      marginRight: 10,
      backgroundColor: '#2699FB',
      width: 28,
      height: 28,
      borderRadius: 14,
      alignItems: 'center',
      justifyContent: 'center',
    }}
  >
    {image ? (
      <Image
        source={{ uri: image, width: 28, height: 28 }}
        style={{ width: 28, height: 28, borderRadius: 14 }}
      />
    ) : (
      <Icon
        name="md-person"
        size={18}
        color="white"
      />
    )}
  </TouchableOpacity>
);

UserIconComponent.propTypes = {
  image: PropTypes.string,
  navigation: PropTypes.shape({
    navigate: PropTypes.func.isRequired,
  }).isRequired,
};

UserIconComponent.defaultProps = {
  image: null,
};

const UserIcon = connect(
  state => ({
    image: state.getIn(['auth', 'user', 'profileImage']),
  }),
)(UserIconComponent);

export default ({ navigation }) => ({
  headerTitle: <HeaderLogo />,
  headerStyle: {
    backgroundColor: (navigation.state.params || {}).color || '#3366FF',
  },
  headerLeft: navigation.state.routeName !== 'Home' ? (
    <TouchableOpacity
      onPress={() => navigation.goBack()}
      style={{
        marginLeft: 10,
        width: 28,
        height: 28,
        borderRadius: 14,
        alignItems: 'center',
        justifyContent: 'center',
      }}
    >
      <Arrow />
    </TouchableOpacity>
  ) : <View />,
  headerRight: <UserIcon navigation={navigation} />,
});

