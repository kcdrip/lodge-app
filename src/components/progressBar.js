import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View } from 'react-native';
import { connect } from 'react-redux';

import { getBg } from '../utils/theme';

class ProgressBar extends Component {
  static propTypes = {
    value: PropTypes.number.isRequired,
  }

  render() {
    const {
      value,
      progressBg,
      progressBarColor,
    } = this.props;

    return (
      <View style={[ styles.component, progressBg ]}>
        <View style={[
          styles.bar,
          { width: `${value * 10}%` },
          progressBarColor,
          ]} 
        />
      </View>
    )
  }
}

const styles = StyleSheet.create({
  component: {
    height: 10,
    width: '90%',
    backgroundColor: 'white',
    borderRadius: 5,
    overflow: 'hidden'
  },
  bar: {
    height: 10, 
    borderRadius: 5,
    backgroundColor: '#2699FB',
  },
})


const mapStateToProps = state => ({
  progressBg:  getBg(state, 'loyal', 'text'),
  progressBarColor:  getBg(state, 'loyal', 'progress'),
});

export default connect(mapStateToProps)(ProgressBar);