import React, { Component } from 'react';
import {
  StyleSheet,
  View, 
  Text,
  ActivityIndicator
} from 'react-native';

const styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },  
  text: {
    color: '#ffffff',
    fontWeight: 'bold',
    fontSize: 24,
    fontFamily: 'arial'
  }
});

class Spinner extends Component {
  render() {
    return (
      <View style={styles.container}>
        <ActivityIndicator 
            size="large"
            color="#ffffff" />
        <Text style={styles.text}>
          {this.props.text}
        </Text>
      </View>
    );
  }
}

export default Spinner
