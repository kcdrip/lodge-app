import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Button, Image, StyleSheet, Text, TouchableOpacity, NavigatorIOS } from 'react-native';

export default class Notification extends Component {
	static propTypes = {
		name: PropTypes.string.isRequired,
		onPress: PropTypes.func
	}

	render() {
		const { name, onPress } = this.props;

		return (
			<TouchableOpacity style={[styles.notification, {backgroundColor: global.theme.box.background}]} onPress={onPress}>
				<Text style={[styles.notificationText, {color: global.theme.box.title}]}>{name}</Text>
			</TouchableOpacity>
		)
	}
}

const styles = StyleSheet.create({
	notification: {
		width: 	'48%',
		height: 120,
		backgroundColor: global.theme ? global.theme.box.background : '#F1F9FF',
		justifyContent: 'center',
		alignItems: 'center',
		marginBottom: 15,
		marginRight: '4%',
	},
	notificationText: {
		width: '50%',
		fontSize: 15,
		fontFamily: 'arial',
		fontWeight: 'bold',
        color: global.theme ? global.theme.box.title : '#2699FB',
	},
});