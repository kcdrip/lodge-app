import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';

import { connect } from 'react-redux';

import MaterialIcon from 'react-native-vector-icons/MaterialIcons';

import styles from '../styles/common';

import { rewardsActions } from '../redux/actions';

import {
  getColor,
  getBg,
} from '../utils/theme';

const REDEEM_QUESTION_TEXT = 'Are you sure you want to\nredeem your ';

class OfferCard extends Component {
  showRewardRedemption() {
    const {
      props: {
        data,
        getRewardForOffer,
        viewRef,
        screenProps: { modal },
      },
    } = this;

    const offerName = data.get('offerName') || 'offer';

    const offerSubText = data.get('offerSubText') || 'Please show your server to redeem.';

    modal.setContent(
      <View style={[styles.modalContainer, styles.modalContainerRedemption]}>
        <MaterialIcon
          name="star"
          size={130}
          color="#2699FB"
        />
        <Text style={styles.modalText}>
          {REDEEM_QUESTION_TEXT}
          <Text style={styles.bold}>
            {offerName}
          </Text>
          ?
        </Text>
        <Text style={styles.offerSubText}>
          {offerSubText}
        </Text>

        <View style={styles.modalButtonsContainer}>
          <TouchableOpacity
            style={[styles.modalRoundButton, styles.modalRoundSelected]}
            onPress={() => {
              this.showRewardRedeemed();
              getRewardForOffer(data.toJS());
            }}
          >
            <MaterialIcon
              name="check"
              size={20}
              color="#2699FB"
            />
          </TouchableOpacity>
          <TouchableOpacity
            style={styles.modalRoundButton}
            onPress={() => modal.clearContent()}
          >
            <MaterialIcon
              name="close"
              size={20}
              color="#2699FB"
            />
          </TouchableOpacity>
        </View>
      </View>,
      'light',
      viewRef,
    );
  }

  showRewardRedeemed() {
    const {
      props: {
        data,
        viewRef,
        screenProps: { modal },
      },
    } = this;

    modal.setContent(
      <View style={[styles.modalContainer, styles.modalContainerRedemption]}>
        <MaterialIcon
          name="check"
          size={130}
          color="#2699FB"
        />
        <Text style={styles.modalText}>
          Reward redeemed!
        </Text>
        <View style={styles.modalButtonsContainer}>
          <TouchableOpacity
            style={styles.modalRoundButton}
            onPress={() => modal.clearContent()}
          >
            <MaterialIcon
              name="close"
              size={20}
              color="#2699FB"
            />
          </TouchableOpacity>
        </View>
      </View>,
      'light',
      viewRef,
    );
  }
  
  render() {
    const {
      data,
      rewards,
      cardBg,
      cardTitle,
      cardSubitle,
      buttonBg,
      buttonText,
    } = this.props;

    console.log('Roc', data.toJS());

    if (!data || !data.has('offerId')) return null;


    const offerId = data.get('offerId');
    const title = data.get('offerTitle');
    const subTitle = data.get('offerSubTitle');


    if (rewards && rewards.has(offerId)) return null;

    return (
      <View style={[styles.offerContainer, cardBg]}>
        <Text style={[styles.offerTitle, cardTitle]}>
          {title}
        </Text>
        <Text style={[styles.comeInText, cardSubitle]}>
          {subTitle}
        </Text>
        <TouchableOpacity
          style={[styles.redeemButton, buttonBg]}
          onPress={() => this.showRewardRedemption()}
        >
          <Text style={[styles.redeemButtonText, buttonText]}>
            REDEEM
          </Text>
        </TouchableOpacity>    
      </View>
    );
  }
}

OfferCard.propTypes = {
  data: PropTypes.object.isRequired,
};

export default connect(
  state => ({
    rewards: state.getIn(['auth', 'user', 'rewards']),
    cardBg: getBg(state, 'card', 'background'),
    cardTitle: getColor(state, 'card', 'title'),
    cardSubitle: getColor(state, 'card', 'subtitle'),
    buttonBg:  getBg(state, 'button', 'background'),
    buttonText: getColor(state, 'button', 'text'),
  }),
  {
    getRewardForOffer: data => rewardsActions.getRewardForOffer(data),
  },
)(OfferCard);
