import React, { Component } from 'react';
import { TouchableOpacity, Text, View, StyleSheet, Button, Modal, Alert, Image, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import moment from 'moment';

import TopEventCard from '../../components/card/top-event-card';
import EventCard from '../../components/card/event-card';

import { loadFeaturedEvents } from './reducer/actions';
import * as eventUtils from '../../utils/events';

const styles = StyleSheet.create({
	featuredEvents: {
		padding: 20,
		flex: 1,
		justifyContent: 'space-around',
	},
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: 25,
		marginBottom: 10
	},
	card: {
		marginBottom: 10,
	},
	button: {
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		height: 21,
		width: 67,
	},
	buttonText: {
        color: 'white',
        fontSize: 8,
		fontFamily: 'arial',
	},
	title: {
		color: '#767676',
		fontSize: 23,
		fontWeight: 'bold',
	},
});

const getHourInString = (hh) => {
    if (hh < 12) {
        return hh + 'am';
    } else {
        if (hh === 12) return '12pm';
        else return (hh-12) + 'pm';
    }
};

class FeaturedEvents extends Component {

	state = {
		isLoading: false
	};

	constructor(props) {
		super(props)

		this._onViewAll = this._onViewAll.bind(this)
	}

	componentWillMount() {
		this.setState({ isLoading: true });
		this.props.loadFeaturedEvents(this._onSuccess.bind(this), this._onError.bind(this));
	}

	_onSuccess() {
		this.setState({ isLoading: false });
	}

	_onError(error) {
		switch (error.code) {
			case 401: 
				Alert.alert('401', error.message);
				Actions.Login();
				break;
		}
		this.setState({ isLoading: false });
	}

	_onViewAll() {
		Actions.FEList();
	}

	render() {
		
		if (this.state.isLoading) {
			return (
				<View style={styles.featuredEvents}>
				<View><View style={styles.header}>
						<Text style={styles.title}>Upcoming Events</Text>
						<TouchableOpacity style={styles.button} onPress={this._onViewAll}>
							<Text style={styles.buttonText}>VIEW ALL</Text>
						</TouchableOpacity>
					</View>
					<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
						<ActivityIndicator />
					</View>
				</View></View>
			)
		}

		const { featuredEvents } = this.props;
		const events = eventUtils.filterEvents(featuredEvents);
		const count = events.length;
		var t = 0;

		if (count > 0) {
			var from = moment(events[0].from).format('hhA');
			var to = moment(events[0].to).format('hhA');

			var period = from + ' - ' + to;
		}

		return (
			<View style={styles.featuredEvents}>
				{/* <View> */}
					<View style={styles.header}>
						<Text style={styles.title}>Upcoming Events</Text>
						<TouchableOpacity style={styles.button} onPress={this._onViewAll}>
							<Text style={styles.buttonText}>VIEW ALL</Text>
						</TouchableOpacity>
					</View>
				{/* <Image style={{ width: '100%', height: 160 }} source={{ uri: events[0].imgSrc }} /> */}
					
					{count>0&&<TopEventCard day={events[0].day} month={events[0].month} title={events[0].title} time={period} imgSrc={events[0].imgSrc} onPress={()=>{Actions.FEDetail({eventId: 0});}}/>}
					{count>1&&<EventCard style={styles.card} day={events[1].day} month={events[1].month} title={events[1].title} time={`(${moment(events[1].from).hours()} - ${moment(events[1].to).hours()})`} onPress={()=>{Actions.FEDetail({eventId: 1});}}/>}
					{count>2&&<EventCard day={events[2].day} month={events[2].month} title={events[2].title} time={`(${moment(events[2].from).hours()} - ${moment(events[2].to).hours()})`} onPress={()=>{Actions.FEDetail({eventId: 2});}}/>}
				{/* </View> */}
			</View>
		);
	}
}

const mapDispatchToProps = {
	loadFeaturedEvents,
};

const mapStateToProps = state => ({
	featuredEvents: [],//state.homeReducer.featuredEvents,
});

export default connect(mapStateToProps, mapDispatchToProps)(FeaturedEvents);