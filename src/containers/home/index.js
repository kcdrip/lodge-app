import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, TouchableOpacity, Image, StyleSheet, Text, View, Dimensions, StatusBar } from 'react-native';
import Modal from 'react-native-modal';

import NavBarContainer from '../../components/navbar';
import LoyaltyCard from '../loyalty/loyalty-card';
import NotificationArea from './notification-area';
import FeaturedEvents from './featured-events';
import DayEvents from './day-events';

export default class Home extends Component {


	constructor(props) {
		super(props)
		
		this.state = {
			oneTimeScroll: false
		}

		this.onCheckIn = this.onCheckIn.bind(this)
	}

	onCheckIn() {
	}

	componentDidMount() {
		if (this.props.scroll === 1) {
			this.navBar._onScrollToEnd();
		}
	}

	componentWillReceiveProps(nextProps) {
		this.navBar._onScrollToEnd();
	}

	render() {

		const oneTimeScroll = this.state.oneTimeScroll ? 'yes' : 'no';

		return (
			<NavBarContainer ref={ref=> this.navBar = ref} scrollBottom={oneTimeScroll} showBack={'no'}><StatusBar hidden={true} />
				<LoyaltyCard onCheckIn={this.openModal} totalPoint={10} offAmount={2} />
				<FeaturedEvents />
				<DayEvents />
				<NotificationArea/>
			</NavBarContainer>
		);
	}
}



const styles = StyleSheet.create({
	container: {
		flex: 1,
	},
});