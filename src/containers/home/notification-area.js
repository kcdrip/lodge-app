import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ActivityIndicator, Button, Image, StyleSheet, Text, View, NavigatorIOS } from 'react-native';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { isEmpty } from 'lodash';

import Notification from '../../components/notification/notification'
import { loadArticles } from '../other/reducer/actions';

const styles = StyleSheet.create({
	row: {
		flex: 1,
		flexDirection: 'row',
		width: '100%'
	},
	notificationArea: {
		padding: 20,		
		marginBottom: 56,
	},
});

class NotificationArea extends Component {

    componentWillMount() {
        this.props.loadArticles();
    }

	render() {

		return (
			<View style={styles.notificationArea}>
				<View style={styles.row}>
					<Notification name={'Newsletter'} onPress={()=>{Actions.News();}}/>
					<Notification  name={'Women of the Moose'} onPress={()=>{Actions.WomenOfMoose();}}/>
				</View>
				<View style={styles.row}>
					<Notification  name={'Pay Dues'} />
					<Notification  name={'Friends of the Moose'} onPress={()=>{Actions.Friends();}}/>
				</View>
				<View style={styles.row}>
					{this.props.isLoading &&
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
							<ActivityIndicator />
						</View>
					}
					{!this.props.isLoading &&
						!isEmpty(this.props.articles) &&
						this.props.articles.map((article, id) => {
							return <Notification  key={id} name={article.title} onPress={()=>{Actions.WomenOfMoose({no: id});}}/>
						})
					}
				</View>
			</View>
		)
	}
}

const mapStateToProps = state => ({
    articles: state.otherReducer.articles,
    isLoading: state.otherReducer.isLoading
});

const mapDispatchToProps = {
    loadArticles
};

export default connect(mapStateToProps, mapDispatchToProps)(NotificationArea);