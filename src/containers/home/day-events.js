import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import Modal from 'react-native-modal';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
import moment from 'moment';

import { loadSpecials } from './reducer/actions';

class EventCard extends Component {
	static propTypes = {
		from: PropTypes.string.isRequired,
		to: PropTypes.string.isRequired,
		title: PropTypes.string.isRequired,
	}

	render() {
		const { from, to, title } = this.props;

		return (
			<View style={styles.eventCard}>
				{
					from === 'ALL' && <Text>ALL-DAY</Text>
				}
                {
                    from !== 'ALL' && <View style={{width: '19%'}}>
						<Text style={styles.card_text}>{from}-{to}</Text>
					</View>
                }
				<TouchableOpacity style={styles.eventCardButton} onPress={this.props.onClick}>
					<Text>{title}</Text>
				</TouchableOpacity>
			</View>
		)
	}
}

const dayNames = ['Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday'];

class DayEvents extends Component {

	constructor (props) {
		super(props)

		this.state = {
			showDailySpecial: false,
			showEventDetail: false,
			selEventId: 0
		};

		this._onShowDailySpecial = this._onShowDailySpecial.bind(this);
		this._onShowEventDetail = this._onShowEventDetail.bind(this);
		this._onCloseModal = this._onCloseModal.bind(this);
	}

	componentWillMount() {
		this.props.loadSpecials();
	}

	_onShowDailySpecial() {
		this.setState({ showDailySpecial: true });
	}

	_onShowEventDetail(id) {		
		this.setState({ showEventDetail: true, selEventId: id });
	}

	_onCloseModal() {
		this.setState({ showEventDetail: false, showDailySpecial: false });
	}

	render() {
		const { events, specials } = this.props;
		const selEventId = this.state.selEventId;

		const dayOfWeek = dayNames[moment().day()-1];

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity onPress={(e) => this._onShowEventDetail(0)}>
						<Text style={styles.title}>What's Happening</Text>
						<Text style={styles.day}>{dayOfWeek}</Text>
					</TouchableOpacity>
					<TouchableOpacity style={styles.button} onPress={()=>Actions.Calendar()}>
						<Text style={styles.buttonText}>VIEW CALENDAR</Text>
					</TouchableOpacity>
				</View>
				<TouchableOpacity onPress={this._onShowDailySpecial} style={styles.special}>
					<Text style={{marginBottom: 10}}>Daily Special</Text>
					{
						this.props.specialLoading && <Text> Loading ... </Text>
					}
					{
						!this.props.specialLoading && isEmpty(specials) && <Text> No events.</Text>
					}
					{
						!this.props.specialLoading && !isEmpty(specials) && specials[moment().day()-1] === undefined && <Text> No events. </Text>
					}
					{
					//	!this.props.specialLoading && !isEmpty(specials) && specials[moment().day()-1] !== undefined && <Text> {specials[moment().day()-1].title} </Text>
					}
				</TouchableOpacity>
				{
					events.map((event, id) => {
						let from = moment(event.from).format('hh:mmA');
						let to = moment(event.to).format('hh:mmA');


						console.log('e1', event);

						return <EventCard key={id} from={from} to={to} title={event.title} onClick={(e) => this._onShowEventDetail(id)}/>;
					})
				}
				<Modal isVisible={this.state.showEventDetail || this.state.showDailySpecial}
					onBackdropPress={this._onCloseModal}
					>
					<View style={styles.modalContainer}>
						{this.state.showDailySpecial && specials[moment().day() - 1] !== undefined && <View style={styles.innerContainer}>
							<Text style={{fontSize: 14, fontWeight: 'bold', color: '#2699FB', marginBottom: 10}}>Daily Special</Text>
							<Text style={{fontSize: 14, color: '#2699FB', marginBottom: 10}}>{/*{specials[moment().day() - 1].title}*/}e2</Text>
							<Text style={{lineHeight: 20}}>Daily Special Details - {specials[moment().day() - 1].content} </Text>
							<View style={{marginTop: 30, flexDirection: 'row'}}>
								<TouchableOpacity 
									style={{marginRight: 20, width: 96, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
									onPress={this._onCloseModal}>
									<Text style={{color: 'white', fontSize: 10}}>SHARE</Text>
								</TouchableOpacity>
							</View>
						</View>}
						{this.state.showEventDetail && !isEmpty(events) && <View style={styles.innerContainer}>
							<Text style={{fontSize: 14, fontWeight: 'bold', color: '#2699FB', marginBottom: 10}}>{/*{events[selEventId].title}*/}e3</Text>
							<Text style={{fontSize: 14, color: '#2699FB', marginBottom: 10}}>
							{
								(() => {
									let fH = moment(events[selEventId].from).hours();
									let fM = moment(events[selEventId].from).minutes();
									let tH = moment(events[selEventId].to).hours();
									let tM = moment(events[selEventId].to).minutes();

									return ('(' + fH + ':' + fM + ' - ' + tH + ':' + tM + ')');
								})
							}
							</Text>
							<Text style={{lineHeight: 20}}>{events[selEventId].detail}</Text>
							<View style={{marginTop: 30, flexDirection: 'row'}}>
								<TouchableOpacity 
									style={{marginRight: 20, width: 96, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
									onPress={this._onCloseModal}>
									<Text style={{color: 'white', fontSize: 10}}>SHARE</Text>
								</TouchableOpacity>
								<TouchableOpacity 
									style={{width: 125, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
									onPress={this._onCloseModal}>
									<Text style={{color: 'white', fontSize: 10}}>ADD TO CALENDAR</Text>
								</TouchableOpacity>
							</View>
						</View>}
						{this.state.showEventDetail && isEmpty(events) && <View style={styles.innerContainer}>
								<Text>No Events today.</Text>
							</View>}
					</View>
				</Modal>
			</View>
		);
	}
}

const styles = StyleSheet.create({
	container: {
		padding: 20,
		borderRadius: 4
	},
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	special: {
		height: 85,
		padding: 10,
		backgroundColor: '#F8F8F8',
		marginTop: 20,
	},
	eventCard: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginTop: 15,
	},
	eventCardButton: {
		height: 43,
		width: '80%',
		backgroundColor: '#F8F8F8',
		alignItems: 'center',
		justifyContent: 'center',
		borderRadius: 5,	
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 2},
		shadowOpacity: 0.8,
		shadowRadius: 2,
	},
	card_text: {
		fontFamily: 'Lato-Black',
		fontSize: 11,
		marginRight: 20,
	},
	button: {
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		height: 21,
		width: 92,
	},
	buttonText: {
        color: 'white',
        fontSize: 8,
		fontFamily: 'arial',
	},
	title: {
        color: '#767676',
        fontSize: 23,
        fontWeight: 'bold',
    },
	day: {
        color: '#767676',
        fontSize: 16,
        fontWeight: 'bold',
    },
    modalContainer: {
		justifyContent: 'center',
		backgroundColor: 'white',
		borderRadius: 10,
		padding: 30,
	},
	innerContainer: {
		position: 'relative',
	},
});

const getTodayEvents = (events) => {

	let todayEvents = events.filter((event) => {
		return moment().diff(event.date, 'days') == 0;
	});

	return todayEvents;
}

const mapDispatchToProps = {
	loadSpecials
};

const mapStateToProps = state => ({
	events: getTodayEvents(state.homeReducer.events),
	specials: state.homeReducer.dailySpecials,
	specialLoading: state.homeReducer.specialLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(DayEvents);