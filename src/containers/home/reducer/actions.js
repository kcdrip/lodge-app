import * as t from './actionTypes';
import * as api from './api';

export function loadFeaturedEvents(successCB, errorCB) {
    return (dispatch) => {
        api.getFeaturedEvents(function (success, events, error) {
            if (success) {
                dispatch({ type: t.FE_LOAD_SUCCESS, events: events});
                successCB();
            } else if (error) 
                errorCB(error);
        })
    }
}

export function loadSpecials() {
    return (dispatch) => {
        dispatch({ type: t.SPECIAL_SET_LOADING });

        api.loadSpecials(function(success, data, error) {
            if (success) {
                dispatch({ type: t.SPECIAL_SAVE, payload: data });
            } else {
                console.log(error);
            }

            dispatch({ type: t.SPECIAL_UNSET_LOADING });
        })
    }
}