import * as t from './actionTypes';
import moment from 'moment';

const sort = (event1, event2) => {
    return (moment(event1.date).diff(event2.date, 'minutes') > 0);
};

const initialState = { specialLoading: false, events: [], featuredEvents: [], dailySpecials: [] };

const homeReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.FE_LOAD_SUCCESS:
            var events = action.events;
            var eventsArr = [];

            for (var event in events) {
                var date = events[event].date;

                eventsArr.push(events[event]);
            }

            var featuredEvs = eventsArr.filter((ev) => {
                var date = ev.date;

                return ev.isFeatured && (moment().diff(date, 'minutes') <= 0);
            });

            eventsArr.sort(sort);
            featuredEvs.sort(sort);
            
            state = Object.assign({}, state, { events: eventsArr, featuredEvents: featuredEvs });

            return state;
        case t.SPECIAL_SAVE:            
    
                state = Object.assign({}, state, { dailySpecials: action.payload });
    
                return state;        
        case t.SPECIAL_SET_LOADING:
            
            return Object.assign({}, state, { specialLoading: true });
        case t.SPECIAL_UNSET_LOADING:
        
            return Object.assign({}, state, { specialLoading: false });
        default:
            return state;
    }
};

export default homeReducer;
