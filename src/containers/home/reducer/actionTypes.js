export const FE_LOAD_SUCCESS = 'fe/LOAD_SUCCESS';
export const SPECIAL_SAVE = 'special/SAVE';
export const SPECIAL_SET_LOADING = 'special/LOADING';
export const SPECIAL_UNSET_LOADING = 'special/UNSET_LOADING';
