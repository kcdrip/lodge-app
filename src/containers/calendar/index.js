import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ScrollView, Button, Image, StyleSheet, Text, View, Dimensions, TouchableOpacity, StatusBar } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Modal from 'react-native-modal';
import moment from 'moment';
import { isEmpty } from 'lodash';

import NavBarContainer from '../../components/navbar';
import YearMonthSelector from '../../components/common/year-month';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		alignItems: 'center',
		justifyContent: 'center',
		paddingHorizontal: 20,
	},
	year_month: {
		marginVertical: 20,
		fontFamily: 'Baloo-Regular',
		fontSize: 20,
	},
	calendar: {
		flex: 1,
		flexDirection: 'row',
		height: 65,
		width: width,
		overflow: 'hidden',
		backgroundColor: 'white',
		borderRadius: 5
	},
	dayIndicator: {
		width: 50,
		height: 65,
		justifyContent: 'center',
		alignItems: 'center',
		borderRadius: 5,
	},
	special: {
		padding: 10,
		height: 85,
		// boxShadow: '0px 1px 2px 0px rgba(9, 30, 66, 0.25)',
	},
	specialButton: {
		marginTop: 20,
		width: 300,
		height: 50,
		padding: 10,
		textAlign: 'center',
		backgroundColor: 'white',
		borderRadius: 4,
	},
	card_container: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
	},
	card_text: {
		fontFamily: 'Lato-Black',
		fontSize: 11,
		marginRight: 20,
	},
	featured: {
		backgroundColor: '#2699FB',
		justifyContent: 'space-between',	
	},
	card: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center',
		width: '80%',
		marginVertical: 10,
		height: 55,
		paddingHorizontal: 20,
		shadowColor: '#000',
		shadowOffset: {width: 0, height: 2},
		shadowOpacity: 0.8,
		shadowRadius: 2,
		borderRadius: 5,	
	},
	title: {
		fontFamily: 'Lato-Regular',
		fontSize: 12,
	},
    modalContainer: {
		justifyContent: 'center',
		backgroundColor: 'white',
		borderRadius: 10,
		padding: 30,
	},
	innerContainer: {
		position: 'relative',
	},
});

class DayIndicator extends Component {
	static propTypes = {
		day: PropTypes.number.isRequired,
		dayOfWeek: PropTypes.string.isRequired,
		hasEvent: PropTypes.bool.isRequired,
		isSelected: PropTypes.bool.isRequired,
		onSelect: PropTypes.func.isRequired,
	}

	constructor(props) {
		super(props);

		this.state = {
			isSelected: false,
		}

	}

	render() {
		const { day, dayOfWeek, hasEvent, isSelected } = this.props;
		const exStyle_CI = isSelected ? {backgroundColor: '#2699FB'} : null;
		const exStyle_Text = isSelected ? {color: 'white'} : null;
		return (
			<TouchableOpacity style={[styles.dayIndicator, exStyle_CI]} onPress={(evt) => this.props.onSelect(day)}>
				<Text style={[{fontFamily: 'Baloo-Regular', fontSize: 16}, exStyle_Text]}>{day}</Text>
				<Text style={[{fontFamily: 'Lato-Regular', fontSize: 14}, exStyle_Text]}>{dayOfWeek}</Text>
				<View style={{height: 20}}>
					{
						hasEvent && <Entypo name="dot-single" style={{fontSize: 20, color: '#FE1E9A'}}/>
					}
				</View>
			</TouchableOpacity>
		)
	}
}

const monthName = [
	'JANUARY', 'FEBRUARY', 'MARCH', 'APRIL', 'MAY', 'JUNE', 'JULY', 'AUGUST', 'SEPTEMBER', 'OCTOBER', 'NOVEMBER', 'DECEMBER'
];

class EventCalendar extends Component {
	static propTypes = {
		year: PropTypes.number,
		month: PropTypes.number,
		day: PropTypes.number,
		monthEventState:PropTypes.array,
		onSelectYearMonth: PropTypes.func,
		onSelectDay: PropTypes.func,
	}

	constructor(props) {
		super(props)

		this.state = {
			month: props.month,
			year: props.year,
			day: props.day,
			isVisibleYM: false,
		}

		this.showYM = this.showYM.bind(this);
		this._onSelectYM = this._onSelectYM.bind(this);
		this._onSelectDate = this._onSelectDate.bind(this);
	}

	showYM () {
		this.setState({ isVisibleYM: !this.state.isVisibleYM });
	}

	onCloseYM() {
		this.setState({ isVisibleYM: false });
	}

	_onSelectYM(yy, mm) {
		this.setState({ month: mm, year: yy, isVisibleYM: false });
		this.props.onSelectYearMonth(yy, mm);
	}

	_onSelectDate(day) {
		this.setState({ day: day });
		this.props.onSelectDay(day);
	}

	render() {
		const { year, month, day, onSelectYearMonth, monthEventState } = this.props;
		var daysOfWeek=['Mo', 'Tu', 'We', 'Th', 'Fr', 'Sa', 'Su'];
		
		return (
			<View style={{flex: 1, alignItems: 'center', marginBottom: 30}}>
				<YearMonthSelector isVisible={this.state.isVisibleYM} setYearMonth={this._onSelectYM} onClose={this.onCloseYM.bind(this)}/>
			 	<TouchableOpacity onPress={this.showYM}><Text style={styles.year_month}>{monthName[this.state.month]} {this.state.year}</Text></TouchableOpacity>
				<ScrollView showsHorizontalScrollIndicator={true} horizontal={true} style={styles.calendar}>
				{
					(() => {
						var days = new Array();
						var isSelected = false;
						var curDay = moment(new Date(this.state.year, this.state.month, this.state.day)).day() - 1;

						for (var i = 0; i < monthEventState.length; i ++) {
							if ( i + 1 === this.state.day ) {
								isSelected = true;
							} else {
								isSelected = false;
							}

							var day = <DayIndicator key={i} onSelect={this._onSelectDate} isSelected={isSelected} day={i+1} dayOfWeek={daysOfWeek[curDay%7]} hasEvent={monthEventState[i]} />
							days.push(day);
							curDay ++;
						}
						return days;
					})()
				}
				</ScrollView>
				<Text>(Scroll to select date.)</Text>
			</View>
		)
	}
}

class EventCard extends Component {
	static propTypes = {
		hour: PropTypes.number.isRequired,
		minute: PropTypes.number.isRequired,
		title: PropTypes.string.isRequired,
		isFeatured: PropTypes.bool,
		onClick: PropTypes.func,
	};

	render() {
		const { hour, minute, title, isFeatured, onClick } = this.props;

		return (
			<View style={styles.card_container}>
				
				<Text style={styles.card_text}>{hour} : {minute}</Text>
				{
					isFeatured && <TouchableOpacity style={[styles.card, styles.featured]} onPress={onClick}>
						<Text style={[{color: 'white'}, styles.title]}>{title}</Text>
							<Entypo name={'star'} style={{fontSize: 20, color: 'white'}}/>					
						</TouchableOpacity>
				}
				{
					!isFeatured && <TouchableOpacity style={[styles.card, {backgroundColor: 'white'}]} onPress={onClick}>
						<Text style={styles.title}>{title}</Text>
					</TouchableOpacity>
				}
			</View>
		)
	}
}

class CalendarView extends Component {

	constructor(props) {
		super(props);

		this.state = {
			month: moment(new Date()).month(),
			year: moment(new Date()).year(),
			day: moment(new Date()).date(),
			isVisibleYM: false,
			showEventDetail: false,
			currentEventNo: 0,
			showDailySpecial: false,
			monthlyEvents: [],
			dailyEvents: [],
			results: '',
		}

		this._onSelectYearMonth = this._onSelectYearMonth.bind(this);
		this._onSelectDay = this._onSelectDay.bind(this);
		this._onShowEventDetail = this._onShowEventDetail.bind(this);
		this._onShowDailySpecial = this._onShowDailySpecial.bind(this);
		this._onCloseModal = this._onCloseModal.bind(this);
	}

	componentDidMount() {
		this._onSelectYearMonth(this.state.year, this.state.month);
		// this._requestCalendarPermission();
		// console.log('cal types: ', Expo.Calendar.getCalendarsAsync())
	}
	
	_requestCalendarPermission = async () => {
		// TODO
	/*	const { status } = await Permissions.askAsync(Permissions.CALENDAR);
		this.setState({
		  errMsg: status === 'granted',
		}); */
		this.myCalendar();
	}

	myCalendar() {
		let details = {
			title: 'myCalendar',
			color: 'blue',
	//		entityType: Calendar.EntityTypes.REMINDER, TODO FIXME
			sourceId: 'my_calendar_1'
		};

		// TODO:
		
		/*
		Calendar.createCalendarAsync(details)
			.then( event => {
				this.setState({ results: event });
			})
			.catch( error => {
				this.setState({ results: error });
			}); */
	}

	_onShowEventDetail(id) {
		
		this.setState({ currentEventNo: id, showEventDetail: true });
	}

	_onShowDailySpecial() {
		this.setState({ showDailySpecial: true });
	}

	_onAddEventToCal() {
		// TODO Calendar.createEventAsync(calendarId, details)
	}

	_onCloseModal() {
		this.setState({ showEventDetail: false, showDailySpecial: false });
	}

	_onSelectYearMonth(yy, mm) {

		console.log('filtereing sis: ', this.props.events)
		// Filter month events
		var filteredEvents = this.props.events.filter((event) => {
			let month = moment(event.date).month();
			let year = moment(event.date).year();

			return year == yy && month == mm;
		});
		console.log('filtered events: ', filteredEvents)
		this.setState({ year: yy, month: mm, monthlyEvents: filteredEvents });
	}

	_onSelectDay(dd) {
		this.setState({ day: dd });
	}

	fillArray(value, len) {
		if (len === 0) return [];
		var a = [value];
		while (a.length * 2 <= len) a = a.concat(a);
		if (a.length < len) a = a.concat(a.slice(0, len - a.length));
		return a;
	}

	render() {
		var events = new Array();
		var dayInMonth = moment(this.state.year + '-' + (this.state.month+1), 'YYYY-MM').daysInMonth();

		var arrEventState = this.fillArray(false, dayInMonth);

		for (var i = 0; i < this.state.monthlyEvents.length; i ++) {
			let day = moment(this.state.monthlyEvents[i].date).date();

			if (day == this.state.day) {
				events.push(this.state.monthlyEvents[i]);
			}

			arrEventState[day-1] = true;
		}

		const { specials } = this.props;
		var curDay = moment(new Date(this.state.year, this.state.month, this.state.day)).day() - 1;
		console.log(curDay)
		return (
			<NavBarContainer><StatusBar hidden={true}/>
				<View style={styles.container}>
					<EventCalendar year={this.state.year} month={this.state.month} day={this.state.day} monthEventState={arrEventState} onSelectYearMonth={this._onSelectYearMonth} onSelectDay={this._onSelectDay} />
					
					<View style={styles.special}>
						<Text style={{fontFamily: 'arial', fontWeight: 'bold', textAlign: 'center', fontSize: 12}}>Daily Special</Text>
						{(!isEmpty(specials) && specials[curDay] !== undefined ) && <TouchableOpacity style={styles.specialButton} onPress={this._onShowDailySpecial}>
							<Text style={{fontFamily: 'Lato-Regular', fontSize: 12, textAlign: 'center', flexWrap: 'wrap'}}>{specials[curDay].title}</Text>								
						</TouchableOpacity>}
					</View>
					{
						!isEmpty(events) && (<View style={{flex: 1,
							alignItems: 'center',
							justifyContent: 'center',}}>
							{
								events.map((event, id) => {
									let hours = moment(event.from).hours();
									let minutes = moment(event.from).minutes();
									return <EventCard onClick={(e) => this._onShowEventDetail(id)} key={id} hour={hours} minute={minutes} title={event.title} isFeatured={event.isFeatured} />
								})
							}
						</View>)		
					}
					<Modal isVisible={this.state.showDailySpecial || this.state.showEventDetail}
						onBackdropPress={this._onCloseModal}
						>
						<View style={styles.modalContainer}>
							{this.state.showDailySpecial && <View style={styles.innerContainer}>
								<Text style={{fontSize: 14, fontWeight: 'bold', color: '#2699FB', marginBottom: 10}}>Daily Special</Text>
								<Text style={{fontSize: 14, color: '#2699FB', marginBottom: 10}}>{this.state.day+'/'+this.state.month+'/'+this.state.year}</Text>
								<Text style={{lineHeight: 20}}>Daily Special Details - {specials[curDay].content} </Text>
								<View style={{marginTop: 30, flexDirection: 'row'}}>
									<TouchableOpacity 
										style={{marginRight: 20, width: 96, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
										onPress={this._onCloseModal}>
										<Text style={{color: 'white', fontSize: 10}}>SHARE</Text>
									</TouchableOpacity>
								</View>
							</View>}
							{this.state.showEventDetail && <View style={styles.innerContainer}>
							<Text style={{fontSize: 14, fontWeight: 'bold', color: '#2699FB', marginBottom: 10}}>{events[this.state.currentEventNo].title}</Text>
								<Text style={{fontSize: 14, color: '#2699FB', marginBottom: 10}}>
								{
									(() => {
										let fH = moment(events[this.state.currentEventNo].from).hours();
										let fM = moment(events[this.state.currentEventNo].from).minutes();
										let tH = moment(events[this.state.currentEventNo].to).hours();
										let tM = moment(events[this.state.currentEventNo].to).minutes();

										return ('(' + fH + ':' + fM + ' - ' + tH + ':' + tM + ')');
									})
								}
								</Text>
								<Text style={{lineHeight: 20}}>{events[this.state.currentEventNo].detail}</Text>
								<View style={{marginTop: 30, flexDirection: 'row'}}>
									<TouchableOpacity 
										style={{marginRight: 20, width: 96, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
										onPress={this._onCloseModal}>
										<Text style={{color: 'white', fontSize: 10}}>SHARE</Text>
									</TouchableOpacity>
									<TouchableOpacity 
										style={{width: 125, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}}
										onPress={this._onCloseModal}>
										<Text style={{color: 'white', fontSize: 10}}>ADD TO CALENDAR</Text>
									</TouchableOpacity>
								</View>
							</View>}
						</View>
					</Modal>
				</View>
			</NavBarContainer>
		);
	}
}

const mapStateToProps = state => ({
	events: state.homeReducer.events,
	specials: state.homeReducer.dailySpecials
});

export default connect(mapStateToProps)(CalendarView);