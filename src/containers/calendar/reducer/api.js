import firebase from '../../../config/firebase';

const auth = firebase.auth();
const database = firebase.database();

/**
 * Get Featured Events
 */
export function getFeaturedEvents (callback) {
    // if (auth.currentUser) {
        database.ref('events').once('value')
            .then((snapshot) => {
                const events = snapshot.val();
                callback(true, events, null);
            })
        
    // } else {
    //     callback(false, null, {code: 401, message: 'Session expired.'});
    // }
}