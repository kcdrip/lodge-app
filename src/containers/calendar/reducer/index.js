import * as t from './actionTypes';
import moment from 'moment';

const initialState = { events: [], featuredEvents: [] };

const calendarReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.CE_LOAD_SUCCESS:
            var events = action.events;
            var eventsArr = [];

            for (var event in events) {
                var date = events[event].date;

                if (moment().diff(date, 'minutes') > 0) continue;

                eventsArr.push(events[event]);
            }

            var featuredEvs = eventsArr.filter((ev) => {
                return ev.isFeatured;
            });

            state = Object.assign({}, state, { events: eventsArr, featuredEvents: featuredEvs });

            return state;
        
        default:
            return state;
    }
};

export default calendarReducer;
