import * as t from './actionTypes';
import * as api from './api';

export function loadFeaturedEvents(successCB, errorCB) {
    return (dispatch) => {
        api.getFeaturedEvents(function (success, events, error) {
            if (success) {
                dispatch({ type: t.CE_LOAD_SUCCESS, events: events});
                successCB();
            } else if (error) 
                errorCB(error);
        })
    }
}