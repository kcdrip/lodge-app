import React, { Component } from 'react';
import { StyleSheet, View, Text, TouchableOpacity } from 'react-native';
import Modal from 'react-native-modal';
import { connect } from 'react-redux';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Entypo from 'react-native-vector-icons/Entypo';

import NavBarContainer from '../../components/navbar';
import RewardCard from '../../components/card/reward-card';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
    modalContainer: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
  },
  innerContainer: {
    alignItems: 'center',
  },
  button: {
        width: '25%',
        height: 48,
        backgroundColor: '#F1F9FF',
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    button_white: {
        width: '25%',
        height: 48,
        borderRadius: 4,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
    medium_text: {
        color: '#2699FB',
        fontFamily: 'arial',
        fontSize: 14,
  },
  location: {
    width: '70%',
  },
    small_text: {
        color: '#2699FB',
        fontFamily: 'arial',
        fontSize: 10,
  },
  item_box: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    height: 57,
    backgroundColor: '#F1F9FF',
    alignItems: 'center',
    padding: 20,
    marginTop: 20,
  }
});

class FriendOfMoose extends Component {

    state = {
    confirmModalVisible: false,
    resultModalVisible: false,
  };

  constructor(props) {
    super(props)

    this.closeModal = this.closeModal.bind(this)
    this._onRedeem = this._onRedeem.bind(this)
    this._onConfirm = this._onConfirm.bind(this)
  }
  
  _onRedeem() {
    this.setState({confirmModalVisible: true});
  }

  closeModal() {
    this.setState({confirmModalVisible:false, resultModalVisible: false});
  }
  
  _onConfirm() {
    this.setState({confirmModalVisible:false, resultModalVisible: true});
  }

    render() {
    const { about, email, location, name, phone, site } = this.props.friends[this.props.fId];
        return (
      <NavBarContainer>
            <View style={styles.container}>
                <View style={{marginBottom: 20, backgroundColor: '#BCE0FD', height: 208}}></View>
        <Text style={{marginBottom: 20, color: '#2699FB', fontFamily: 'Georgia', fontSize: 40}}>{ name }</Text>
        <Modal
                    isVisible={this.state.confirmModalVisible || this.state.resultModalVisible}
                    onRequestClose={this.closeModal}
        >
          <View style={styles.modalContainer}>
            {this.state.confirmModalVisible && <View style={styles.innerContainer}>
              <Entypo name={'star'} style={{fontSize: 150, color: '#2699FB'}}/>
              <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14}}>Are you sure you want to</Text>
              <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14,}}>redeem your ...?</Text>              
              <View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
                <TouchableOpacity
                  style={[styles.button, {borderRadius: 28, width: 56, height: 56}]}
                  onPress={this._onConfirm}
                >
                  <Feather name="check" style={{fontSize: 20, color:'#2699FB'}} />
                </TouchableOpacity>
                <TouchableOpacity
                  style={[styles.button_white, {borderRadius: 28, width: 56, height: 56}]}
                  onPress={this.closeModal}
                >
                  <Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
                </TouchableOpacity>
              </View>
            </View>}
            {this.state.resultModalVisible && <View style={styles.innerContainer}>
              <Feather name="check" style={{fontSize: 150, color:'#2699FB'}} />
              <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16, marginTop: -20}}>Reward Redeemed!</Text>
              <TouchableOpacity
                style={[styles.button_white, {borderRadius: 28, marginTop: 20, width: 56, height: 56}]}
                onPress={this.closeModal}
              >
                <Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
              </TouchableOpacity>
            </View>}
                    </View>
                </Modal>
                <RewardCard onRedeem={this._onRedeem} title={'reward.title'} />
                <View style={{height: 109, backgroundColor: global.theme.box.background, justifyContent: 'center', alignItems: 'center'}}>
                    <Text style={[styles.medium_text, {color: global.theme.box.title}]}>ABOUT LOCATION: {about} </Text>
                </View>
        <TouchableOpacity style={[styles.item_box, {backgroundColor: global.theme.box.background}]}>
          <View style={{flexDirection: 'row'}}>
            <FontAwesome name='phone' style={{marginRight: 20, fontSize: 20, color: global.theme.box.title}}/>
                      <Text style={[styles.medium_text, {color: global.theme.box.title}]}>+123 {phone}</Text>
          </View>
          <Text style={[styles.medium_text, {color: global.theme.box.title}]}>></Text>
                </TouchableOpacity>
        <TouchableOpacity style={[styles.item_box, {backgroundColor: global.theme.box.background}]}>
          <View style={{flexDirection: 'row'}}>
            <Entypo name='mail' style={{marginRight: 20, fontSize: 20, color: global.theme.box.title}}/>
            <Text style={[styles.medium_text, {color: global.theme.box.title}]}>{email}</Text>
          </View>
          <Text style={[styles.medium_text, {color: global.theme.box.title}]}>></Text>
                </TouchableOpacity>
        <TouchableOpacity style={[styles.item_box, {backgroundColor: global.theme.box.background}]}>
          <View style={{flexDirection: 'row'}}>
            <Entypo name='mail' style={{marginRight: 20, fontSize: 20, color: global.theme.box.title}}/>
            <Text style={[styles.medium_text, {color: global.theme.box.title}]}>{site}</Text>
          </View>
          <Text style={[styles.medium_text, {color: global.theme.box.title}]}>></Text>
                </TouchableOpacity>
        <TouchableOpacity style={[styles.item_box, {backgroundColor: global.theme.box.background}]}>
          <View style={{flexDirection: 'row'}}>
            <Entypo name='location-pin' style={{marginRight: 20, fontSize: 20, color: global.theme.box.title}}/>
            <Text numberOfLines={1} ellipsizeMode={'tail'} style={[styles.medium_text, styles.location, {color: global.theme.box.title}]}>{location}</Text>
          </View>
          <Text style={[styles.medium_text, {color: global.theme.box.title}]}>></Text>
                </TouchableOpacity>
        {/*<MapView
          style={{ flex: 1, marginTop: 20, height: 200, alignSelf: 'stretch' }}
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />*/}
        <Text>Map here TODO</Text>
                
            </View>
      </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    friends: state.otherReducer.friends,
});

export default connect(mapStateToProps)(FriendOfMoose);