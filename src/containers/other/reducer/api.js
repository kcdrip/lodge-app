import firebase from '../../../config/firebase';

const database = firebase.database();

export function getNews (callback) {
    database.ref('news').once('value')
        .then((sn) => {
            const news = sn.val();
            callback(true, news, null);
        });
}

export function getFriends (callback) {
    database.ref('friends').once('value')
        .then((sn) => {
            const friends = sn.val();
            callback(true, friends, null);
        });
}

export function getArticles (callback) {

    database.ref('articles').once('value')
        .then((sn) => {
            const articles = sn.val();

            callback(true, articles, null);
        }).catch(error=>{console.log(error)});
}