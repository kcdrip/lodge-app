import * as t from './actionTypes';

const initialState = { isLoading: false, news: [], articles: [], friends: [] };

const otherReducer = (state = initialState, action) => {

    switch(action.type) {
        case t.SET_BUSY:
            return Object.assign({}, state, { isLoading: true });

        case t.UNSET_BUSY:
            return Object.assign({}, state, { isLoading: false });

        case t.NEWS_LOAD_SUCCESS:
            var news = action.news;
            var newsArr = [];

            for (var anews in news) {
                newsArr.push(news[anews]);
            }
            state = Object.assign({}, state, { news: newsArr });

            return state;

        case t.ARTICLES_LOAD_SUCCESS:
            var articles = action.articles;
            var articleArr = [];

            for (var article in articles) {
                articleArr.push(articles[article]);
            }
            state = Object.assign({}, state, { articles: articleArr });

            return state;

        case t.FRIENDS_LOAD_SUCCESS:
            var friends = action.friends;
            var friendArr = [];

            for (var friend in friends) {
                friendArr.push(friends[friend]);
            }
            state = Object.assign({}, state, { friends: friendArr });

            return state;
        
        default: 
            return state;
    }
};

export default otherReducer;