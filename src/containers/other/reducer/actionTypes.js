export const NEWS_LOAD_SUCCESS = 'news/LOAD_SUCCESS';
export const ARTICLES_LOAD_SUCCESS = 'articles/LOAD_SUCCESS';
export const FRIENDS_LOAD_SUCCESS = 'friends/LOAD_SUCCESS';
export const SET_BUSY = 'other/SET_BUSY';
export const UNSET_BUSY = 'other/UNSET_BUSY';