import * as t from './actionTypes';
import * as api from './api';

export function loadNews() {
    return dispatch => {
        dispatch({ type: t.SET_BUSY});
        api.getNews(function (success, news, error) {
            if (success) {
                dispatch({ type: t.NEWS_LOAD_SUCCESS, news: news });
                dispatch({ type: t.UNSET_BUSY });
            } else if (error) {
                dispatch({ type: t.UNSET_BUSY });
            }
        });
    }
}

export function loadFriends() {
    return dispatch => {
        dispatch({ type: t.SET_BUSY});
        api.getFriends(function (success, friends, error) {
            if (success) {
                dispatch({ type: t.FRIENDS_LOAD_SUCCESS, friends: friends });
                dispatch({ type: t.UNSET_BUSY });
            } else if (error) {
                dispatch({ type: t.UNSET_BUSY });
            }
        });
    }
}

export function loadArticles() {
    return dispatch => {
        dispatch({ type: t.SET_BUSY});

        api.getArticles(function (success, articles, error) {
            if (success) {
                dispatch({ type: t.ARTICLES_LOAD_SUCCESS, articles: articles });
                dispatch({ type: t.UNSET_BUSY });
            } else if (error) {
                dispatch({ type: t.UNSET_BUSY });
            }
        });
    }
}