import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import Feather from 'react-native-vector-icons/Feather';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';

import DateInCircle from '../../components/common/date-in-circle';
import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rectangle: {
        width: '100%',
        height: 208,
        backgroundColor: '#D1D9DF',
        position: 'relative',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    iconButtonText: {
		fontFamily: 'arial',
		fontWeight: 'bold',
		fontSize: 14,
		color: '#2699FB',
	},
});

class NewsletterDetail extends Component {
    
    render() {
        const { title, subTitle, detail } = this.props.newsletter[this.props.newsId];

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <View style={styles.rectangle}>
                        <Entypo name='image' style={{fontSize: 68, color: '#7FC4FD'}} />
                        <View style={{position: 'absolute', top: 135, width: '100%', height: 57, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <Feather name='clock' style={{fontSize: 20, color: '#2699FB'}} />
                            <Text style={{color: '#2699FB', marginLeft: 20, marginRight: 100, fontFamily: 'arial', fontSize: 17}}>June 2, 2017</Text>
                            <TouchableOpacity style={{backgroundColor: '#2699FB', justifyContent: 'center', alignItems: 'center', height: 40, width: 72, borderRadius: 4}}>
                                <Text style={{color: 'white', fontWeight: 'bold'}}>
                                    SHARE
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{padding: 20}}>
                        <Text style={{marginBottom: 20, color: '#2699FB', fontFamily: 'Georgia', fontSize: 40}}>{ title }</Text>
                        <Text style={{marginBottom: 10, color: '#2699FB', fontFamily: 'arial', fontSize: 20}}>{ subTitle }</Text>
                        <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14}}>{ detail }</Text>
                        <TouchableOpacity style={{marginTop: 30, flexDirection: 'row'}}>
                            <MaterialIcons name="comment" style={{fontSize: 20, color: '#2699FB', marginRight: 10}}/>
                            <Text style={styles.iconButtonText}>120</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    newsletter: state.otherReducer.news,
});

export default connect(mapStateToProps)(NewsletterDetail);