import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, Text, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
//import { Actions } from 'react-native-router-flux';
const Actions = {};

import Newsletter from '../../components/post/newsletter';
import NavBarContainer from '../../components/navbar';
import { loadNews } from './reducer/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
})

class NewsletterList extends Component {

    constructor(props) {
        super(props)

        this._onSelect = this._onSelect.bind(this);
    }

    componentWillMount() {
        this.props.loadNews();
    }

    _onSelect(id) {
    }

    render() {
        const { isLoading, news } = this.props;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                {
                    !isLoading && !isEmpty(news) && news.map((newsletter, id) => {
                        return <Newsletter
                            key={id}
                            id={newsletter.id}
                            subTitle={newsletter.subTitle}
                            title={newsletter.title} 
                            onSelect={() => {Actions.NewsDetail({newsId: id})}}
                            />
                    })
                }
                {isLoading && <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}><ActivityIndicator /></View>}
                </View>
            </NavBarContainer>
        )
    }
}

const mapDispatchToProps = {
    loadNews
};

const mapStateToProps = state => ({
    news: state.otherReducer.news,
    isLoading: state.otherReducer.isLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(NewsletterList)