import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { View, StyleSheet, ActivityIndicator } from 'react-native';
import { connect } from 'react-redux';
import { isEmpty } from 'lodash';
//import { Actions } from 'react-native-router-flux';
const Actions = {};

import FriendCard from '../../components/card/friend';
import NavBarContainer from '../../components/navbar';
import { loadFriends } from './reducer/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
})

class FriendList extends Component {

    constructor(props) {
        super(props)
    }

    componentWillMount () {
        this.props.loadFriends();
    }

    render() {

        const { isLoading, friends } = this.props;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                {
                    !isLoading && !isEmpty(friends) && friends.map((friend, id) => {
                        return <FriendCard key={id} name={friend.name} onPress={()=>{Actions.FriendOfMoose({fId: id})}} />
                    })
                }
                {isLoading && <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}><ActivityIndicator /></View>}
                </View>
            </NavBarContainer>
        )
    }
}

const mapDispatchToProps = {
    loadFriends
};

const mapStateToProps = state => ({
    friends: state.otherReducer.friends,
    isLoading: state.otherReducer.isLoading,
});

export default connect(mapStateToProps, mapDispatchToProps)(FriendList)