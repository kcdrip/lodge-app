import NewsView from './newsletter';
import NewsDetailView from './newsletter-detail';
import WomenOfMoose from './women';

import * as actions from './reducer/actions';
import * as actionTypes from './reducer/actionTypes';
import reducer from './reducer';

// import * as theme from '../../styles/theme';

export { actions, actionTypes, reducer, NewsView, NewsDetailView, WomenOfMoose };