import React, { Component } from 'react';
import { connect } from 'react-redux';
import { View, Text, TouchableOpacity, Button, ImageBackground, StyleSheet, ActivityIndicator, WebView, Dimensions } from 'react-native';
import { Icon } from 'native-base';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { isEmpty } from 'lodash';
import HTMLView from 'react-native-render-html';

import NavBarContainer from '../../components/navbar';
import { loadArticles } from './reducer/actions';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20
    },
});

class WomenOfMoose extends Component {

    constructor(props) {
        super(props);

    }

    render() {
        var no = this.props.no;

        if (no === undefined) no = 0;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    {!isEmpty(this.props.articles) &&
                        <HTMLView html={this.props.articles[no].content} imagesMaxWidth={Dimensions.get('window').width-40} />
                    }
                    {
                    /* <View style={{position: 'relative', height: 136, width: '100%', backgroundColor: '#BCE0FD', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon name='person' style={{fontSize: 78, color: 'white'}} />
                    </View>
                    <View style={{paddingHorizontal: 20, paddingVertical: 10, justifyContent: 'center'}}>
                        <Text style={{ color: '#2699FB', fontSize: 20, fontWeight: 'bold' }}>
                            Women of the Moose Chapter 1835
                        </Text>
                        <View style={{ padding: 20, flexDirection: 'row', justifyContent: 'center', alignItems: 'center' }}>
                            <TouchableOpacity style={{ marginRight: 20, height: 40, width: 96, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'white' }}>Email</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={{ height: 40, width: 96, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center' }}>
                                <Text style={{ fontSize: 10, fontWeight: 'bold', color: 'white' }}>Call</Text>
                            </TouchableOpacity>
                        </View>
                        <Text style={{ fontSize: 14, color: '#2699FB' }}>Latest Update: Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt.</Text>
                        <View style={{ marginTop: 20 }}>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#2699FB' }}>Meeting Times:</Text>
                            <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#6E6565' }}>Every 1st & 3rd Tuesday @ 7pm</Text>
                        </View>
                    </View>
                    <View style={{ flex: 1, backgroundColor: '#F1F9FF', padding: 30 }}>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#2699FB' }}>OFFICERS:</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#6E6565' }}>Name - Title</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#6E6565' }}>Name - Title</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#6E6565' }}>Name - Title</Text>
                        <Text style={{ fontSize: 14, fontWeight: 'bold', color: '#6E6565' }}>Name - Title</Text>
                    </View> */}
                </View>
            </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    articles: state.otherReducer.articles,
    isLoading: state.otherReducer.isLoading
});

const mapDispatchToProps = {
    loadArticles
};

export default connect(mapStateToProps, mapDispatchToProps)(WomenOfMoose);