import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { StyleSheet, View, TouchableOpacity, Text } from 'react-native';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';
import moment from 'moment';

import DateInCircle from '../../components/common/date-in-circle';
import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    rectangle: {
        width: '100%',
        height: 208,
        backgroundColor: '#D1D9DF',
        position: 'relative',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

const getHourInString = (hh) => {
    if (hh < 12) {
        return hh + 'am';
    } else {
        if (hh === 12) return '12pm';
        else return (hh-12) + 'pm';
    }
};

class FeaturedEventDetail extends Component {
    
    render() {
        const { featuredEvents } = this.props;
        const featuredEvent = featuredEvents[this.props.eventId];

        var from = moment(featuredEvent.from).format('hhA');
        var to = moment(featuredEvent.to).format('hhA');

        var period = from + ' - ' + to;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <View style={styles.rectangle}>
                        <Entypo name='image' style={{fontSize: 68, color: '#7FC4FD'}} />
                        <View style={{position: 'absolute', top: 125, width: '100%', height: 57, flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                            <DateInCircle month={featuredEvent.month} day={featuredEvent.day} isSmall={false} />
                            <Text style={{color: 'white', marginLeft: 20, marginRight: 100, fontFamily: 'arial', fontSize: 17}}>{period}</Text>
                            <TouchableOpacity style={{backgroundColor: '#2699FB', justifyContent: 'center', alignItems: 'center', height: 40, width: 72, borderRadius: 4}}>
                                <Text style={{color: 'white', fontWeight: 'bold'}}>
                                    SHARE
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{padding: 20}}>
                        <Text style={{marginBottom: 20, color: '#2699FB', fontFamily: 'Georgia', fontSize: 40}}>{featuredEvent.title}</Text>
                        <Text style={{marginBottom: 10, color: '#2699FB', fontFamily: 'arial', fontSize: 20}}>Event Details</Text>
                        <Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14}}>{featuredEvent.detail}</Text>
                        <TouchableOpacity style={{marginTop: 30, backgroundColor: '#2699FB', justifyContent: 'center', alignItems: 'center', height: 48, width: 148, borderRadius: 4}}>
                            <Text style={{color: 'white', fontWeight: 'bold'}}>ADD TO CALENDAR</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    featuredEvents: state.homeReducer.featuredEvents,
});

export default connect(mapStateToProps)(FeaturedEventDetail);