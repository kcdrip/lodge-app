import React, { Component } from 'react';
import PropTypes from 'prop-types';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { View, StyleSheet, Text, StatusBar } from 'react-native';
import { connect } from 'react-redux';

import FeaturedEvent from '../../components/post/featured-event';
import NavBarContainer from '../../components/navbar';

import * as eventUtils from '../../utils/events';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        padding: 20,
    },
});

class FeaturedEventList extends Component {

    constructor(props) {
        super(props)

        this._onSelectFeaturedEvent = this._onSelectFeaturedEvent.bind(this);
    }

    _onSelectFeaturedEvent(id) {
    }

    render() {
        
		if (false) {
			return (
				<View style={styles.featuredEvents}>
				<View><View style={styles.header}>
						<Text style={styles.title}>Upcoming Events</Text>
						<TouchableOpacity style={styles.button} onPress={this._onViewAll}>
							<Text style={styles.buttonText}>VIEW ALL</Text>
						</TouchableOpacity>
					</View>
					<Text>Loading...</Text>
				</View></View>
			)
        }
        
        const { events } = this.props;
        const featuredEvents = eventUtils.filterEvents(events);
        const count = featuredEvents.length;

        return (
            <NavBarContainer>
                <View style={styles.container}>            
                { count === 0 && <Text>No featured events to show.</Text>}
                { count > 0 && featuredEvents.map((event, id) => {
                        return (
                            <FeaturedEvent
                                key={id}
                                id={event.id}
                                title={event.title}
                                timeFrom={event.from}
                                timeTo={event.to}
                                month={event.month}
                                day={event.day}
                                content={event.detail} 
                                onSelect={() => {Actions.FEDetail({eventId: id});}}
                                />
                        );
                    })
                }                
                </View>
            </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    events: state.homeReducer.featuredEvents,
});

export default connect(mapStateToProps)(FeaturedEventList);
