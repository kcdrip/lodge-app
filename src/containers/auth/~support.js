import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { connect } from 'react-redux';

import CustomInput from '../../components/common/custome-input';
import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    small_text: {
        fontSize: 15,
        fontFamily: 'Lato-Bold',
    },
    inputbox: {
        justifyContent: 'center', 
        alignItems: 'center',
        width: '100%',
        marginBottom: 10,
    },
    button: {
        width: '80%',
        height: 48,
        borderRadius: 20,
        backgroundColor: '#2699FB',
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    text: {
        color: 'white',
    },
});

export default class Support extends Component {

    constructor(props) {
        super(props)

        this._onSave = this._onSend.bind(this);
    }
    
    _onSend() {
    }

    render() {

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <Text style={{color: '#7F7F7F', fontSize: 30, marginBottom: 25}}>Support</Text>
                    <Text style={{color: '#7F7F7F', fontSize: 14}}>Having an issue with the app?</Text>
                    <Text style={{color: '#7F7F7F', fontSize: 14}}>Please fill out the form below and</Text>
                    <Text style={{color: '#7F7F7F', fontSize: 14}}>a member of our team will get in touch ASAP</Text>
                    <CustomInput placeholder={'Name'}/>
                    <CustomInput placeholder={'Email'}/>               
                    <CustomInput placeholder={'Phone'}/> 
                    <CustomInput placeholder={'iOS or Android'}/>                              
                    <CustomInput multiline={'yes'} styles={{height: 200}} placeholder={'Please Describe your issue…'}/>
                    <TouchableOpacity style={[styles.button, {marginTop: 40, marginBottom: 12}]} onPress={this._onSend}>
                        <Text style={styles.text}>SEND</Text>
                    </TouchableOpacity>
                </View>
            </NavBarContainer>
        )
    }
}