import * as t from './actionTypes';
import * as cardActions from '../../loyalty/reducer/actionTypes';
import firebase from '../../../config/firebase';

const database = firebase.database();


import * as api from './api';

export function register(data, successCB, errorCB) {

    console.log('REG', data);
    return (dispatch) => {
        dispatch({ type: t.SET_BUSY });
        api.createUserWithEmailAndPassword(data, function (success, user, error) {
            console.log('creat_hujo_majo',data, successCB, errorCB);
            if (success) {
                // dispatch({type: t.LOGIN_SUCCESS, user: user});
                dispatch({type: t.UNSET_BUSY });
                successCB();
            } else if (error) {
                dispatch({type: t.UNSET_BUSY });
                errorCB(error);
            }
        });
    };
}

registerForPushNotificationsAsync = async (userId) => {
    // TODO: Permissions, notif
    return;

    const { status: existingStatus } = await Permissions.getAsync(
        Permissions.NOTIFICATIONS
    );
    let finalStatus = existingStatus;

    // only ask if permissions have not already been determined, because
    // iOS won't necessarily prompt the user a second time.
    if (existingStatus !== 'granted') {
        // Android remote notification permissions are granted during the app
        // install, so this will only ask on iOS
        const { status } = await Permissions.askAsync(Permissions.NOTIFICATIONS);
        finalStatus = status;
    }

    // Stop here if the user did not grant permissions
    if (finalStatus !== 'granted') {
        return;
    }

    // Get the token that uniquely identifies this device
    let token = await Notifications.getExpoPushTokenAsync();

    var updates = {};
    updates['/expoToken'] = token;
    database.ref('users').child(userId).update(updates);

}

async function _onRegister (userId) {
    await registerForPushNotificationsAsync(userId)
}

export function login(data, successCB, errorCB) {
    return (dispatch) => {
        console.log('actions login');
        dispatch({ type: t.SET_BUSY });
        api.signInWithEmailAndPassword(data, function (success, data, error) {
            console.log('actions login callback', success, data, error);
            if (success) {
                //Register expo token
                _onRegister(data.user.id);

                console.log('USERLOGIN: ', data.user.checked_date)
                dispatch({type: t.LOGIN_SUCCESS, user: data});
                dispatch({type: cardActions.LC_SET_POINT, userPoint: data.user.point, checkedDate: data.user.checked_date});
                dispatch({type: t.UNSET_BUSY });
                
                successCB();
            } else if (error) {
                console.log('error, have to unset it');
                dispatch({type: t.UNSET_BUSY });
                errorCB(error);
            } 
        });
    };
}

export function logout(successCB, errorCB) {
    return (dispatch) => {
        api.signOut(function (success, data, error) {
            if (success) {
                dispatch({type: t.LOGGED_OUT});
                successCB();
            }else if (error) errorCB(error)
        });
    };
}