import { AsyncStorage } from 'react-native';

import * as t from './actionTypes';

const initialState = { 
  isBusy: false, 
  loggedIn: false, 
  user: {
    id: '', 
    email: '...', 
    password: 'XXX', 
    point: 0, 
    maxCount: 0, 
    rewards: [],
    rewardhistory: []
  } 
};

const authReducer = (state = initialState, action) => {
  switch (action.type) {
    case t.SET_BUSY:
      console.log('reducer is busy');
      return {
        ...state,
        isBusy: true 
      };
    
    case t.UNSET_BUSY:
      console.log('reducer unset busy');
      return {
        ...state,
        isBusy: false 
      };
    
    case t.LOGIN_SUCCESS:
      var user = action.user.user;
      var refreshToken = user.refreshToken;
      // AsyncStorage.setItem('refreshToken', refreshToken);
      // AsyncStorage.setItem('user', JSON.stringify(user));
      var rewardHistories = user.rewardhistory;
      var rdHistory = [];

      for (var rh in rewardHistories) {

        rdHistory.push(rewardHistories[rh]);
      }

      user["rewardhistory"] = rdHistory;
      state = Object.assign({}, state, {loggedIn: true, user: user });

      return state;

    case t.LOGGED_OUT:
      AsyncStorage.removeItem('refreshToken');
      AsyncStorage.removeItem('user');
      state = Object.assign({}, state, {loggedIn: false });

      return state;

    case t.ADD_REWARD_SUCCESS:
      const { newRewards, maxCount } = action.data;
      console.log('RESTULT: ', newRewards, maxCount)
      state = Object.assign({}, state, { user: { ...state.user, rewards: newRewards, maxCount: maxCount, point: 0 } });

      return state;

    case t.ADD_REWARD_HISTORY:
      var history = action.history;

      if (history.state === 'earned') return state;

      var historyArr = state.rewardhistory;
      if (!historyArr) historyArr = new Array();
      historyArr.push(history);

      var newRewards = state.user.rewards.filter((rwd) => (rwd.id != history.reward.id));

      state = Object.assign({}, state, { user: { ...state.user, rewardhistory: historyArr, rewards: newRewards } });

      return state;

    default:
      return state;
  }
};

export default authReducer;