export const LOGGED_IN = 'auth/LOGGED_IN';
export const LOGGED_OUT = 'auth/LOGGED_OUT';
export const LOGIN_SUCCESS = 'auth/LOGIN_SUCCESS';
export const ADD_REWARD_SUCCESS = 'user/ADD_REWARD_SUCCESS';
export const ADD_REWARD_HISTORY = 'user/ADD_REWARD_HISTORY';
export const SET_BUSY = 'auth/SET_BUSY';
export const UNSET_BUSY = 'auth/UNSET_BUSY';
