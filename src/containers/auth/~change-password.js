import React, { Component } from 'react';
import { View, Text, TouchableOpacity, StyleSheet, TextInput } from 'react-native';
import { connect } from 'react-redux';
//import { Actions } from 'react-native-router-flux';
const Actions = {};

import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        paddingVertical: 40,
        justifyContent: 'center',
        alignItems: 'center',
    },
    small_text: {
        fontSize: 15,
        fontFamily: 'Lato-Bold',
    },
    inputbox: {
        justifyContent: 'center', 
        alignItems: 'center',
        width: '100%',
        marginBottom: 10,
    },
    button: {
        width: '80%',
        height: 48,
        borderRadius: 20,
        backgroundColor: '#2699FB',
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    text: {
        color: 'white',
    },
    textInput: {
        padding: 10, 
        paddingHorizontal: 30, 
        margin: 6, 
        borderRadius: 20, 
        width: '80%',
        height: 48, 
        borderColor: '#BCE0FD', 
        borderWidth: 1, 
        color: '#2699FB'
    }
})

export default class ChangePassword extends Component {

    constructor(props) {
        super(props)

        this.state = {
            old: '',
            new: '',
            confirm: ''
        }

        this._onContinue = this._onContinue.bind(this)
    }
    
    _onContinue() {
        Actions.Profile();
    }

    render() {

        return (
            <NavBarContainer>
                <View style={styles.container}>                
                    <Text style={{color: '#7F7F7F', fontSize: 30, marginBottom: 45}}>Change Password</Text>
                    <View style={styles.inputbox}>
                        <Text style={styles.small_text}>Old Password</Text>
                        <TextInput
                            style={styles.textInput}
                            placeholder={'Old Password'}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({old: text})}
                            value={this.state.old}
                        />
                    </View>
                    <View style={styles.inputbox}>
                        <Text style={styles.small_text}>New Password</Text>
                        <TextInput
                            style={styles.textInput}
                            placeholder={'New Password'}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({new: text})}
                            value={this.state.new}
                        />
                    </View>
                    <View style={styles.inputbox}>
                        <Text style={styles.small_text}>Confirm Password</Text>
                        <TextInput
                            style={styles.textInput}
                            placeholder={'Confirm Password'}
                            secureTextEntry={true}
                            onChangeText={(text) => this.setState({confirm: text})}
                            value={this.state.confirm}
                        />
                    </View>
                    <TouchableOpacity style={[styles.button, {marginTop: 12, marginBottom: 12}]} onPress={this._onContinue}>
                        <Text style={styles.text}>CONTINUE</Text>
                    </TouchableOpacity>
                </View>
            </NavBarContainer>
        )
    }
}
