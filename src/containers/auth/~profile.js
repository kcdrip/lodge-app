import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Button, ImageBackground, StyleSheet, Alert } from 'react-native';
import { Icon } from 'native-base';
import Spinner from '../../components/spinner';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import Entypo from 'react-native-vector-icons/Entypo';
import { connect } from 'react-redux';

import { logout } from './reducer/actions';
import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    change_photo: {
        position: 'absolute', 
        right: 20, 
        bottom: 20, 
        backgroundColor: '#7F7F7F', 
        width: 82, 
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
        borderRadius: 4,
    },
    largeText: {
        color: '#2699FB',
        fontFamily: 'arial',
        fontWeight: 'bold',
        fontSize: 20,
    },
    smallText: {
        color: '#2699FB',
        fontFamily: 'arial',
        fontSize: 14,
    },
});

class Profile extends Component {

    state = {
        spinner: false
    }

    constructor(props) {
        super(props);

        this._onChangePassword = this._onChangePassword.bind(this);
        this._onEditProfile = this._onEditProfile.bind(this);
        this._onLogout = this._onLogout.bind(this);
        this._onSupport = this._onSupport.bind(this);
    }

    _onChangePassword() {
        Actions.ChangePassword();
    }

    _onEditProfile() {
        Actions.EditProfile();
    }

    _onSupport() {
        Actions.Support();
    }

    _onLogout() {
        this.setState({spinner: true});
        this.props.logout(this._onSuccess.bind(this), this._onError.bind(this));
    }

    _onSuccess() {
        this.setState({spinner: false});
        Actions.Auth();        
    }

    _onError(error) {
        Alert.alert('Oops!', error.message);
    }

    render() {
        if (!this.props.user) {
            return (
                <View style={styles.container}>
                    <Spinner
                        visible={this.state.spinner}
                        text={'One moment...'} />
                </View>
            )
        }

        const { name, email } = this.props.user;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <View style={{position: 'relative', height: 136, width: '100%', backgroundColor: '#BCE0FD', justifyContent: 'center', alignItems: 'center'}}>
                        <Icon name='person' style={{fontSize: 68, color: '#7FC4FD'}} />
                        <TouchableOpacity style={styles.change_photo}>
                            <Text style={{color: 'white', fontSize: 10}}>Change Photo</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{height: 160, backgroundColor: '#F1F9FF', flexDirection: 'row', justifyContent: 'space-around', alignItems: 'center'}}>
                        <View>
                            <Text style={styles.largeText}>{name}</Text>
                            <Text style={styles.smallText}>{email}</Text>
                            <Text style={styles.smallText}>(555) 555-5555</Text>
                            <Text style={styles.smallText}>Birthday: 1/1/1970</Text>
                        </View>
                        <TouchableOpacity style={{width: 96, height: 40, backgroundColor: '#2699FB', borderRadius: 4, justifyContent: 'center', alignItems: 'center'}} onPress={this._onEditProfile}>
                            <Text style={{color: 'white', fontSize: 10}}>EDIT</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{padding: 40}}>
                        <TouchableOpacity style={{flexDirection: 'row', marginBottom: 10}} onPress={this._onChangePassword}>
                            <MaterialIcons name="settings"  style={{color: '#2699FB', fontSize: 20, marginRight: 15}} />
                            <Text style={{color: '#2699FB', fontSize: 12}} >Change Password</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flexDirection: 'row', marginBottom: 10}} onPress={this._onSupport}>
                            <Entypo name="help-with-circle"  style={{color: '#2699FB', fontSize: 20, marginRight: 15}} />
                            <Text style={{color: '#2699FB', fontSize: 12}} >Support</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={{flexDirection: 'row', marginBottom: 10}} onPress={this._onLogout}>
                            <Entypo name="log-out" style={{color: '#2699FB', fontSize: 20, marginRight: 15}} />
                            <Text style={{color: '#2699FB', fontSize: 12}} >Log Out</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </NavBarContainer>
        )
    }
}

const mapDispatchToProps = {
    logout,
};

function mapStateToProps(state, props) {
    return { 
        user: state.authReducer.user
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Profile);