import React, { Component } from 'react';
import Spinner from '../../components/spinner';
import { View, Text, TextInput, TouchableOpacity, StyleSheet, Alert } from 'react-native';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../../components/logo';

import { color } from '../../styles/Theme';

import { register } from './reducer/actions';

import styles from '../../styles/login';

class Signup extends Component
{  
  constructor(props) {
    super(props)
    this.state = {
      forceCheck: false,
      name: '',
      email: '',
      password: '',
      conPassword: '',
      spinner: false,
    };
  }

  emailIsValid() {
    return /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(this.state.email);
  }
  
  onSuccess() {
    Actions.Main();
  }

  onError(error) {
    Alert.alert('Oops!', error.message);
  }

  handleRegister() {
    const {
      name,
      email,
      password
    } = this.state;
    this.props.register({
      name,
      email,
      password,
      point: 0
    }, 
    this.onSuccess.bind(this), 
    this.onError.bind(this));
  }

  render() {
     const {
      forceCheck,
      name,
      email,
      password,
      conPassword
    } = this.state;

    const nameLongEnough = name.length > 2;
    const emailValid = this.emailIsValid();
    const passwordLongEnough = password.length > 5;
    const passwordEqual = password == conPassword;

    const registerEnabled = nameLongEnough && emailValid && passwordLongEnough && passwordEqual;

    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Logo />
          <Text style={{color: 'white', fontSize: 23, fontFamily: 'Lato-Bold', marginTop: 20, marginBottom: -10}}>
            Create your account
          </Text>
        </View>
        <TextInput
          style={[styles.input, forceCheck && !nameLongEnough ? styles.failedInput : null]}
          autoFocus
          placeholderTextColor={color.INPUT_TEXT}
          placeholder={'Name'}
          onChangeText={name => this.setState({ name })}
          autoCorrect={false}
          autoCapitalize='words'
          value={ name }
        />
        <TextInput
          style={[styles.input, forceCheck && !emailValid ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          placeholder={'Email'}
          autoCapitalize='none'
          keyboardType='email-address'
          autoCorrect={false}
          onChangeText={email => this.setState({ email })}
          value={ email }
        />
        <TextInput
          style={[styles.input, forceCheck && !passwordLongEnough ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize='none'
          placeholder={'Password'}
          secureTextEntry
          onChangeText={password => this.setState({ password })}
          value={ password }
        />
        <TextInput
          style={[styles.input, styles.mb_min, forceCheck && (conPassword.length > 0) && !passwordEqual ? styles.failedInput : null]}
          placeholderTextColor={color.INPUT_TEXT}
          autoCapitalize='none'
          placeholder={'Confirm Password'}
          secureTextEntry
          onChangeText={conPassword => this.setState({ conPassword, forceCheck: true })}
          value={ conPassword }
        />
        <View style={styles.spacer} />
        <TouchableOpacity
            style={[styles.button, styles.buttonPrimary, registerEnabled ? null : styles.buttonDisabled]}
            onPress={() => registerEnabled ? this.handleRegister() : this.setState({ forceCheck: true }) }>
          <Text style={styles.buttonPrimaryText}>CONTINUE</Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={[styles.button, styles.buttonSecondary]}
            onPress={() => this.handleFbLogin()}>
          <Icon
              name="facebook"
              style={styles.facebookIcon}
              size={18} 
              color="#2699FB" />
          <Text style={styles.buttonSecondaryText}>
            SIGN UP WITH FACEBOOK
          </Text>
        </TouchableOpacity>
        <Spinner
          visible={this.props.isBusy}
          text={'One moment...'} />
      </View>
    )
  }
}

const mapDispatchToProps = {
  register,
};

const mapStateToProps = state => ({
  isBusy: state.authReducer.isBusy
});

export default connect(mapStateToProps, mapDispatchToProps)(Signup);