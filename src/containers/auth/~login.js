/*import React, { Component } from 'react';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { View, Text, TouchableOpacity, StyleSheet, TextInput, Alert } from 'react-native';
import { connect } from 'react-redux';
import { LoginManager } from 'react-native-fbsdk';
import Icon from 'react-native-vector-icons/FontAwesome';
import Logo from '../../components/logo';
import Spinner from '../../components/spinner';

import { login } from './reducer/actions';

import styles from '../../styles/login';
import { color } from '../../styles/Theme';

class Login extends Component {  
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      spinner: false
    }
  }

  handleLogin() {
    this.props.login({
      email: this.state.email,
      password: this.state.password,
    },
    () => this.onSuccess(),
    err => this.onError(err)
    )

  }

  handleFbLogin() {
    LoginManager.logInWithReadPermissions(['public_profile', 'email'])
      .then(result => {
        if (result.isCancelled) {
          console.log('Login cancelled');
        } else {
          console.log('Login success with permissions: ' + result.grantedPermissions.toString());
          FBSDK.AccessToken.getCurrentAccessToken().then(
            (data) => {
              console.log('AccessToken', data.accessToken.toString());
              // this.performFBLogin(data.accessToken.toString());
            },
          );
        }
      },
      error => {
        console.log('Login fail with error: ' + error);
      });
  }

  onSuccess() {
    // this.setState({spinner: false});
    Actions.Main();
  }

  onError(error) {
    // this.setState({spinner: false});
    Alert.alert('Oops!', error.message);
  }

  handleSignup() {
    Actions.SignUp();
  }

  render() {
    const {
      email,
      password
    } = this.state;

    const loginEnabled = email != '' && password != '';

    console.log('ISBUSY', this.props.isBusy);

    return (
      <View style={styles.container}>
        <View style={styles.logoContainer}>
          <Logo />
        </View>
        <TextInput
            style={styles.input}
            autoFocus
            autoCorrect={false}
            placeholder={'Email'}
            autoCapitalize='none'
            keyboardType='email-address'
            placeholderTextColor={color.INPUT_TEXT}
            onChangeText={email => this.setState({ email })}
            value={ email }
        />
        <TextInput
            style={styles.input}
            autoCorrect={false}
            placeholder={'Password'}
            secureTextEntry
            onChangeText={password => this.setState({ password })}
            value={ password }
        />
        <TouchableOpacity 
            style={[styles.button, styles.buttonPrimary, loginEnabled ? null : styles.buttonDisabled]}
            onPress={() => loginEnabled ? this.handleLogin() : null}>
          <Text style={styles.buttonPrimaryText}>
            LOGIN
          </Text>
        </TouchableOpacity>
        <TouchableOpacity 
            style={[styles.button, styles.buttonSecondary]}
            onPress={() => this.handleFbLogin()}>
          <Icon
              name="facebook"
              style={styles.facebookIcon}
              size={18} 
              color="#2699FB" />
          <Text style={styles.buttonSecondaryText}>
            LOGIN WITH FACEBOOK
          </Text>
        </TouchableOpacity>
        <View style={styles.spacer} />
        <TouchableOpacity 
            style={[styles.button, styles.buttonPrimary]} 
            onPress={() => this.handleSignup()}>
          <Text style={styles.buttonPrimaryText}>
            SIGN UP
          </Text>
        </TouchableOpacity>
        <Spinner
            visible={this.props.isBusy}
            text='One moment...' />
      </View>
    )
  }
}

const mapDispatchToProps = {
  login,
};

const mapStateToProps = state => ({
  isBusy: state.authReducer.isBusy,
  user: state.authReducer.user
});

export default connect(mapStateToProps, mapDispatchToProps)(Login);

*/