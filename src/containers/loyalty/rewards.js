import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { TouchableOpacity, Text, View, StyleSheet } from 'react-native';
import Spinner from '../../components/spinner';
import Modal from 'react-native-modal';
import Feather from 'react-native-vector-icons/Feather'
import Entypo from 'react-native-vector-icons/Entypo';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import { isEmpty } from 'lodash';

import RewardCard from '../../components/card/reward-card';
import { redeemReward } from './reducer/actions';

const styles = StyleSheet.create({
	container: {
		padding: 20,
		flex: 1,
		justifyContent: 'space-around',
	},
	header: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		height: 25,
		marginBottom: 10
	},
	card: {
		marginBottom: 10,
	},
	button_nor: {
		borderRadius: 10,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		height: 21,
		width: 67,
	},
	buttonText: {
        color: 'white',
        fontSize: 8,
		fontFamily: 'arial',
	},
	title: {
		color: '#767676',
		fontSize: 23,
		fontWeight: 'bold',
	},
    modalContainer: {
		flex: 1,
		justifyContent: 'center',
		backgroundColor: 'white',
		height: 233,
		borderRadius: 10,
	},
	innerContainer: {
		alignItems: 'center',
	},
	button: {
        width: '25%',
        height: 48,
        backgroundColor: '#F1F9FF',
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    button_white: {
        width: '25%',
        height: 48,
        borderRadius: 4,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
});

class RewardsView extends Component {
	
	state = {
		confirmModalVisible: false,
		resultModalVisible: false,
		redeemId: '',
		isBusy: false,
	};

	constructor(props) {
		super(props)

		this.closeModal = this.closeModal.bind(this)
		this._onRedeem = this._onRedeem.bind(this)
		this._onConfirm = this._onConfirm.bind(this)
	}
  
	_onRedeem(id) {
		this.setState({confirmModalVisible: true, redeemId: id});
	}

	closeModal() {
		this.setState({confirmModalVisible:false, resultModalVisible: false});
	}
	
	onSuccess() {
		this.setState({confirmModalVisible:false, isBusy: false, resultModalVisible: true});
	}

	onError(error) {
		console.log(error);
		this.setState({ isBusy: false, confirmModalVisible: false });
	}

	_onConfirm() {
		this.setState({ isBusy: true });

		this.props.redeemReward({
			uid: this.props.uid,
			reward: this.props.rewards[this.state.redeemId],
			rewards: this.props.rewards,
			state: 'redeemed'
		}, this.onSuccess.bind(this), this.onError.bind(this));
	}

	render() {
		const { rewards } = this.props;

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<Text style={styles.title}>Rewards</Text>
					<TouchableOpacity style={styles.button_nor} onPress={(evt) => {Actions.RewardHistory()}}>
						<Text style={styles.buttonText}>HISTORY</Text>
					</TouchableOpacity>
				</View>
				<Modal
                    isVisible={this.state.confirmModalVisible || this.state.resultModalVisible}
                    onRequestClose={this.closeModal}
				>
					<View style={styles.modalContainer}>
						{this.state.confirmModalVisible && <View style={styles.innerContainer}>
							<Entypo name={'star'} style={{fontSize: 150, color: '#2699FB'}}/>
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14}}>Are you sure you want to</Text>
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 14}}>
								redeem your&nbsp;
								<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16, fontWeight: 'bold'}}>$2.00 OFF Reward?</Text>
							</Text>							
							<View style={{flexDirection: 'row', marginTop: 20, justifyContent: 'center', alignItems: 'center'}}>
								<TouchableOpacity
									style={[styles.button, {borderRadius: 28, width: 56, height: 56}]}
									onPress={this._onConfirm}
								>
									<Feather name="check" style={{fontSize: 20, color:'#2699FB'}} />
								</TouchableOpacity>
								<TouchableOpacity
									style={[styles.button_white, {borderRadius: 28, width: 56, height: 56}]}
									onPress={this.closeModal}
								>
									<Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
								</TouchableOpacity>
							</View>
							<Spinner visible={this.state.isBusy} />
						</View>}
						{this.state.resultModalVisible && <View style={styles.innerContainer}>
							<Feather name="check" style={{fontSize: 150, color:'#2699FB'}} />
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16, marginTop: -20}}>Reward Redeemed!</Text>
							<TouchableOpacity
								style={[styles.button_white, {borderRadius: 28, marginTop: 20, width: 56, height: 56}]}
								onPress={this.closeModal}
							>
								<Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
							</TouchableOpacity>
						</View>}
                    </View>
                </Modal>
				{
					!isEmpty(rewards) && rewards.map((reward, id) => {
						return (
							<RewardCard key={id} onRedeem={(e) => this._onRedeem(id)} title={reward.title} />
						)
					})
				}
				{
					isEmpty(rewards) && <Text> No Rewards. </Text>
				}
			</View>
		);
	}
}

const mapStateToProps = state => ({
	rewards: state.authReducer.user.rewards,
	uid: state.authReducer.user.id
});

const mapDispatchToProps = {
	redeemReward
};

export default connect(mapStateToProps, mapDispatchToProps)(RewardsView);