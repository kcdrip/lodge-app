import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { ScrollView, TouchableOpacity, Image, StyleSheet, Text, View, Dimensions, Modal } from 'react-native';

import NavBarContainer from '../../components/navbar';
import LoyaltyCard from './loyalty-card';
import Rewards from './rewards';

const { height, width } = Dimensions.get('window');

export default class Loyalty extends Component {
 
    render() {
        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <LoyaltyCard totalPoint={10} offAmount={2} />
                    <Rewards />
                </View>
            </NavBarContainer>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    main: {
        height: height - 124,
    },
});