import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { TouchableOpacity, StyleSheet, Text, View } from 'react-native';
import Modal from 'react-native-modal';
//import { Actions } from 'react-native-router-flux';
const Actions = {};
import Feather from 'react-native-vector-icons/Feather'
import { connect } from 'react-redux';
import moment from 'moment';

import ProgressBar from '../../components/common/progress-bar';
//import { requestReward, resetPoint, redeemReward } from './reducer/actions';

class LoyaltyCard extends Component {

	state = {
		chkinModalVisible: false,
		resultModalVisible: false,
		chkinWarnModalVisible: false,
		remainMins: 0
	};

	constructor(props) {
		super(props)

		this.openModal = this.openModal.bind(this)
		this.closeModal = this.closeModal.bind(this)
		this._onCheckIn = this._onCheckIn.bind(this)
	}

	componentWillReceiveProps(nextProps) {
		console.log(this.props.currentPoint, this.props.totalPoint, this.props.rewardCount);
		if (this.props.currentPoint != nextProps.currentPoint && nextProps.currentPoint == this.props.totalPoint) {

		/*	nextProps.requestReward({
				uid: nextProps.uid,
				rc: nextProps.rewardCount,
				rewards: nextProps.rewards
			}, this.onSuccess.bind(this), this.onError.bind(this)); */
		}
	}

	onSuccessRedeem () {
		Actions.Rewards();
	}

	onSuccess (reward) {

	/*	this.props.resetPoint({uid: this.props.uid});
		this.props.redeemReward({
			uid: this.props.uid,
			reward: reward,
			state: 'earned'
		}, this.onSuccessRedeem.bind(this), this.onError.bind(this));		*/
	}

	onError(error) {
		console.log(error.message);
	}

	openModal() {
		var date = moment(new Date()).format();
		var elapsedMins = moment(date).diff(this.props.checkedDate, 'minutes');

		// Calculate elapsed time since last checkin
		if (elapsedMins > global.settings.rewards.delay * 60) {
			this.setState({chkinModalVisible: true});
		} else {
			this.setState({chkinWarnModalVisible: true, remainMins: global.settings.rewards.delay * 60 - elapsedMins});
		}
	}

	closeModal() {
		this.setState({chkinModalVisible:false, resultModalVisible: false, chkinWarnModalVisible: false});
	}

	_onCheckIn() {
		this.setState({chkinModalVisible:false, resultModalVisible: true});
	}

	render() {
		const { currentPoint, totalPoint, offAmount } = this.props;
		const remainPoint = totalPoint - currentPoint;

		return (
			<View style={[styles.loyaltyCard/*, {backgroundColor: global.theme.loyal.background}*/]}>
				<Text style={[styles.title/*, {color: global.theme.loyal.text }*/]}>LOYALTY PROGRAM</Text>
				<View style={{flex: 1, flexDirection: 'row', marginTop: -7}}>
					<Text style={[styles.status,/*{color: global.theme.loyal.text}*/]}>{currentPoint}</Text>
					<Text style={[styles.totalText, /*{color: global.theme.loyal.text}*/]}>/{totalPoint}</Text>
				</View>
				<Text style={[styles.text, styles.message, /*{color: global.theme.loyal.text}*/]}>{remainPoint}	points remaining until ${offAmount} OFF</Text>
				<ProgressBar style={styles.progress} value={currentPoint} />
				<TouchableOpacity style={styles.button_nor} onPress={this.openModal}>
					<Text style={styles.buttonText}>CHECK-IN</Text>
				</TouchableOpacity>

				<Modal
					isVisible={this.state.chkinModalVisible || this.state.resultModalVisible || this.state.chkinWarnModalVisible}
					onBackdropPress={() => this.closeModal()}
				>
					<View style={styles.modalContainer}>
						{this.state.chkinModalVisible && <View style={styles.innerContainer}>
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16}}>CHECK IN TO RECEIVE A POINT</Text>
							<View style={{flexDirection: 'row', marginTop: 20}}>
								<TouchableOpacity
									style={[styles.button, {marginRight: 27}]}
									onPress={() => {this.closeModal(); Actions.GeoFencing();}}
								>
									<Text style={{fontSize: 10, color: 'white'}}>CHECK IN</Text>
								</TouchableOpacity>
								<TouchableOpacity
									style={styles.button_white}
									onPress={() => {this.closeModal(); Actions.ScanQR();}}
								>
									<Text style={{fontSize: 10, color: '#2699FB'}}>SCAN QR</Text>
								</TouchableOpacity>
							</View>
						</View>}
						{this.state.resultModalVisible && <View style={styles.innerContainer}>
							<Feather name="check" style={{fontSize: 150, color:'#2699FB'}} />
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16, marginTop: -20}}>POINT VALIDATED</Text>
							<TouchableOpacity
								style={[styles.button_white, {borderRadius: 28, marginTop: 20, width: 56, height: 56}]}
								onPress={() => this.closeModal()}
							>
								<Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
							</TouchableOpacity>
						</View>}
						{this.state.chkinWarnModalVisible && <View style={styles.innerContainer}>
							<Text style={{color: '#2699FB', fontFamily: 'arial', fontSize: 16}}>Time Left for Next Check-in: {this.state.remainMins} mins</Text>
							<TouchableOpacity
								style={[styles.button_white, {borderRadius: 28, marginTop: 20, width: 56, height: 56}]}
								onPress={() => this.closeModal()}
							>
								<Text style={{fontSize: 20, color: '#2699FB'}}>X</Text>
							</TouchableOpacity>
						</View>}
					</View>
				</Modal>
			</View>
		);
	}
}

const mapDispatchToProps = {
	//requestReward,
	//resetPoint,
	//redeemReward
}

const mapStateToProps = state => ({
	currentPoint: 1,// state.cardReducer.currentPoint,
	uid: 1,// state.authReducer.user.id,
	rewardCount: 1,//state.authReducer.user.maxCount,
	rewards: 1,//state.authReducer.user.rewards,
	checkedDate: 1,//state.cardReducer.checkedDate
});

export default connect(mapStateToProps, mapDispatchToProps)(LoyaltyCard);

const styles = StyleSheet.create({
	loyaltyCard: {
		backgroundColor: global.theme ? global.theme.loyal.background : '#537EFF',
		alignItems: 'center',
		justifyContent: 'space-around',
		height: 175,
		padding: 15,
	},
	text: {
        fontFamily: 'Lato-Regular',
		color: global.theme ? global.theme.loyal.text : 'white',
    },
	title: {
		marginTop: -3,
		fontSize: 14,
        fontFamily: 'Lato-Regular',
        color: global.theme ? global.theme.loyal.title : 'white',
	},
	message: {
        //marginTop: 17,
        marginBottom: 8,
	},
	status: {
		fontSize: 45,
        fontFamily: 'Lato-Bold',
        color: 'white',
	},
	totalText: {
        fontSize: 45,
        fontFamily: 'Lato-Black',
        color: global.theme ? global.theme.loyal.text : 'white',
	},
	buttonText: {
		fontSize: 7,
		fontFamily: 'arial',
		fontWeight: 'bold',
		color: 'white',
	},
	button_nor: {
		marginTop: 12,
		height: 35,
		width: 99, 
		borderStyle: 'solid',
		borderWidth: 3,
		borderColor: 'white',
		borderRadius: 15,
		justifyContent: 'center',
		alignItems: 'center',
	}, 	
	modalContainer: {
		justifyContent: 'center',
		backgroundColor: 'white',
		padding: 20,
		borderRadius: 10,
	},
	innerContainer: {
		alignItems: 'center',
	},
	button: {
        width: '25%',
        height: 48,
        borderRadius: 4,
        backgroundColor: '#2699FB',
        justifyContent: 'center',
        alignItems: 'center',
    }, 
    button_white: {
        width: '25%',
        height: 48,
        borderRadius: 4,
        borderColor: '#2699FB',
        borderWidth: 1,
        backgroundColor: 'white',
        justifyContent: 'center',
        alignItems: 'center',
    },
});