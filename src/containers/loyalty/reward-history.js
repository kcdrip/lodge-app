import React, { Component } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';
import { isEmpty } from 'lodash';

import NavBarContainer from '../../components/navbar';

const styles = StyleSheet.create({
    container: {
        flex: 1,
    }
    
});

class RewardHistory extends Component {

    render() {
        const { rewardHistory } = this.props;

        return (
            <NavBarContainer>
                <View style={styles.container}>
                    <View style={{flex: 1, padding: 30}}>
                        <Text style={{marginBottom: 20, textDecorationLine: 'underline', fontFamily: 'Lato-Bold', fontSize: 23, color: '#767676'}}>
                            Reward History
                        </Text>
                        {
                            !isEmpty(rewardHistory) && rewardHistory.map((rh, id) =>{
                                var month = moment(rh.date).month();
                                var date = moment(rh.date).date();

                                return <Text key={id} style={{marginBottom: 10, textDecorationLine: 'underline', fontSize: 14, color: '#7F7F7F'}}>
                                    {month}/{date}: $2 OFF Reward {rh.state}
                                </Text>
                            })
                        }
                        {
                            isEmpty(rewardHistory) && <Text key={id} style={{marginBottom: 10, textDecorationLine: 'underline', fontSize: 14, color: '#7F7F7F'}}>
                                No reward history.
                            </Text>
                        }
                    </View>
                </View>
            </NavBarContainer>
        )
    }
}

const mapStateToProps = state => ({
    rewardHistory: state.authReducer.user.rewardhistory,
});

export default connect(mapStateToProps)(RewardHistory);
  