import * as t from './actionTypes';

const initialState = { currentPoint: 0, checkedDate: '', rewards: [], maxCount: 0, rewardHistory: [] };

const cardReducer = (state = initialState, action) => {
    switch (action.type) {
        case t.LC_ADD_POINT_SUCCESS:
            var point = action.userPoint;
            var date = action.checkedDate;

            state = Object.assign({}, state, { currentPoint: point, checkedDate: date });

            return state;
        
        case t.LC_RESET_POINT:

            state = Object.assign({}, state, { currentPoint: 0 });

            return state;

        case t.LC_SET_POINT:
            var point = action.userPoint;
            var date = action.checkedDate;

            state = Object.assign({}, state, { currentPoint: point, checkedDate: date });

            return state;
        
        case t.LC_ADD_REWARD_SUCCESS:
            var rewards = action.data.newRewards;
            var maxCount = action.data.maxCount;
            console.log(rewards, maxCount)
            state = Object.assign({}, state, { rewards: rewards, maxCount: maxCount });

            return state;

        default:
            return state;
    }
};

export default cardReducer;
