export const LC_ADD_POINT_SUCCESS = 'lc/ADD_POINT_SUCCESS';
export const LC_ADD_REWARD_SUCCESS = 'lc/ADD_REWARD_SUCCESS';
export const LC_SET_POINT = 'lc/SET_POINT';
export const LC_RESET_POINT = 'lc/RESET_POINT';