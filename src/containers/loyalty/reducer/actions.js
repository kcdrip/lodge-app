import * as t from './actionTypes';
import * as at from '../../auth/reducer/actionTypes';
import * as api from './api';

export function addPoint(data) {
    return (dispatch) => {
        api.addPoint(data, function (success, userPoint, date, error) {
            if (success) {
                dispatch({ type: t.LC_ADD_POINT_SUCCESS, userPoint: userPoint, checkedDate: date });
            } else if (error) {
                console.log(error);
            }
        })
    }
}

export function redeemReward(data, successCB, ErrorCB) {
    return (dispatch) => {
        api.redeemReward(data, function (success, history, error) {
            if (success) {
                dispatch({ type: at.ADD_REWARD_HISTORY, history: history});
                successCB();
            } else if (error) {
                errorCB(error);
            }
        })
    }
}

export function resetPoint(data) {
    return (dispatch) => {
        api.resetPoint(data, function (success, userPoint, error) {
            if (success) {
                dispatch({ type: t.LC_RESET_POINT, userPoint: 0});
                // successCB();
            } else if (error) {
                // errorCB(error);
            }
        })
    }
}

export function requestReward(data, successCB, errorCB) {
    return (dispatch) => {
        api.addReward(data, function (success, newRewards, maxCount, error) {
            if (success) {
                dispatch({ type: at.ADD_REWARD_SUCCESS, data: {newRewards, maxCount}});
                successCB(newRewards[newRewards.length-1]);
            } else if (error) {
                errorCB(error)
            }
        })
    }
}