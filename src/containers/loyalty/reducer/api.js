import firebase from '../../../config/firebase';
import moment from 'moment';

const auth = firebase.auth();
const database = firebase.database();

/**
 * Add point
 */
export function addPoint (data, callback) {
    const { uid, curPoint } = data;

    var date = moment(new Date()).format();
    database.ref('users/' + uid + '/checked_date').set(date);
    database.ref('users/' + uid + '/point').set(curPoint + 1, function(error) {
        if (error) {
            callback(false, null, null, error);
        } else {
            callback(true, curPoint + 1, date, null);
        }
    });
}

/**
 * Reset point
 */
export function resetPoint (data, callback) {
    const { uid } = data;
    database.ref('users/' + uid + '/point').set(0, function(error) {
        if (error) {
            callback(false, null, error);
        } else {
            callback(true, 0, null);
        }
    });
}

/**
 * Get new reward, add reward to user's
 */
export function addReward (data, callback) {
    const { uid, rc, rewards } = data;
    database.ref('rewards').once('value')
        .then((snapshot) => {
            const allRewards = snapshot.val();

            const rewardKeys = Object.keys(allRewards);
            const count = rewardKeys.length;

            if (count) {
                var maxCnt = rc;

                if (rc === undefined) maxCnt = 0;

                var rewardId = global.settings ? global.settings.rewards.default : 0;
                let newRwd = allRewards[rewardKeys[rewardId]];
                let newArr = rewards;
                if (rewards === undefined) newArr = new Array();

                newArr.push(newRwd);
                database.ref('users/' + uid + '/rewards').set(newArr, function(error) {
                    if (error) {
                        callback(false, null, rc, error);
                    } else {
                        database.ref('users/' + uid + '/maxCount').set(maxCnt + 1, function(error) {
                            if (error) {
                                callback(false, null, maxCnt, error);
                            } else {
                                callback(true, newArr, maxCnt + 1, false);
                            }
                        });
                    }
                });
            } else {
                callback(false, null, rc, {message: 'no rewards'});
            }
        })
}

/**
 * Add new Redeem History
 */
export function redeemReward (data, callback) {
    const { uid, reward, rewards, state } = data;

    if (state === 'redeemed') {
        var newArr = rewards.filter((rwd) => (rwd.id != reward.id));
        database.ref('users/' + uid + '/rewards').set(newArr, function(error) {
            if (error) {
                callback(false, null, rc, error);
            } else {
                var newId = database.ref('users/' + uid).child('rewardhistory').push().key;
                data["id"] = newId;
                data["date"] = moment().format();
                database.ref('users/' + uid + '/rewardhistory/' + newId).set({
                    id: data.id,
                    uid: data.uid,
                    date: data.date,
                    reward: data.reward,
                    state: data.state
                }, function(error) {
                    if (error) {
                        callback(false, null, error);
                    } else {
                        callback(true, data, null);
                    }
                });
            }
        });
    } else {
        var newId = database.ref('users/' + uid).child('rewardhistory').push().key;
        data["id"] = newId;
        data["date"] = moment().format();
        database.ref('users/' + uid + '/rewardhistory/' + newId).set({
            id: data.id,
            uid: data.uid,
            date: data.date,
            reward: data.reward,
            state: data.state
        }, function(error) {
            if (error) {
                callback(false, null, error);
            } else {
                callback(true, data, null);
            }
        });
    }
}