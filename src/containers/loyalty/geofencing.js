import React, { Component } from 'react';
import { Platform, Text, View, StyleSheet } from 'react-native';
import { connect } from 'react-redux';
//import { Actions } from 'react-native-router-flux';
const Actions = {};

import NavBarContainer from '../../components/navbar';
import { addPoint } from './reducer/actions';

function distance(lat1, lon1, lat2, lon2, unit) {
    var radlat1 = Math.PI * lat1/180
    var radlat2 = Math.PI * lat2/180
    var theta = lon1-lon2
    var radtheta = Math.PI * theta/180
    var dist = Math.sin(radlat1) * Math.sin(radlat2) + Math.cos(radlat1) * Math.cos(radlat2) * Math.cos(radtheta);
    dist = Math.acos(dist)
    dist = dist * 180/Math.PI
    dist = dist * 60 * 1.1515
    if (unit=="K") { dist = dist * 1.609344 }
    if (unit=="N") { dist = dist * 0.8684 }
    return dist
}

class GeoFencing extends Component {
  state = {
    location: null,
    errorMessage: null,
  };

  componentWillMount() {
    this._getLocationAsync();
  }

  _getLocationAsync = async () => {

   // let { status } = await Permissions.askAsync(Permissions.LOCATION);
   // TODO: permission
   let status = 'not';
    if (status !== 'granted') {
      this.setState({
        errorMessage: 'Permission to access location was denied',
      });
    }

    return;

    //let location = await Location.getCurrentPositionAsync({});

    //geolocation.getCurrentPosition(geo_success);
    
    // Calculate geo fencing
    let distance = distance(global.settings.geo.lat, global.settings.geo.lng, location.lat, location.lng, 'K');
    console.log('distace: ', distance)
    if (distance < 0.2) // Distance < 200m
      this.onAddPoint();
    this.setState({ location });
  };

  onSuccess() {
    console.log('successs s sksks')
    Actions.pop();
  }

  onError(error) {
    console.log('Error in adding new point ', error);
  }

  onAddPoint() {
    this.props.addPoint(
      {uid: this.props.uid, curPoint: this.props.curPoint}, this.onSuccess.bind(this), this.onError.bind(this));
  }

  render() {
    let text = 'Waiting..';
    if (this.state.errorMessage) {
      text = this.state.errorMessage;
    } else if (this.state.location) {
      text = JSON.stringify(this.state.location);
    }

    return (
        <NavBarContainer>
            <View style={styles.container}>
                <Text style={styles.paragraph}>{text}</Text>
            </View>
        </NavBarContainer>
    );
  }
}

const mapStateToProps = state => ({
  uid: state.authReducer.user.id,
  curPoint: state.cardReducer.currentPoint
});

const mapDispatchToProps = {
  addPoint
};

export default connect(mapStateToProps, mapDispatchToProps)(GeoFencing);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    paddingTop: 50,// Constants.statusBarHeight, TODO: fixme
    backgroundColor: '#ecf0f1',
  },
  paragraph: {
    margin: 24,
    fontSize: 18,
    textAlign: 'center',
  },
});