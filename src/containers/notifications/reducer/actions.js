import * as t from './actionTypes';
import * as api from './api';
import firebase from '../../../config/firebase';

const database = firebase.database();

export function loadNotifications(successCB, errorCB) {
    return (dispatch) => {
        dispatch({ type: t.NOTIFICATION_SET_BUSY });

        api.loadNotifications(function (success, notifies, error) {
            if (success) {
                dispatch({ type: t.NOTIFICATION_LOAD_SUCCESS, notifications: notifies});
                dispatch({ type: t.NOTIFICATION_UNSET_BUSY });
                successCB();
            } else if (error) {
                dispatch({ type: t.NOTIFICATION_UNSET_BUSY });
                errorCB(error);
            }
        })
    }
}

/**
 * Leave commenet on notification
 */
export function leaveComment(data) {
    return (dispatch) => {
        const { notId, userName, comment } = data;
        
        var newComment = {};        
        var newId = database.ref('notifications').child(notId).child('comments').push().key;

        newComment['id'] = newId;
        newComment['name'] = userName;
        newComment['content'] = comment;
        newComment['time'] = (new Date()).toString();
        
        database.ref('notifications').child(notId).child('comments/'+newId).set(newComment, function(error) {
            if (error) {
                console.log('error saving: ', newComment);
            } else {
                console.log('success')
            }
        });
    }
}