import firebase from '../../../config/firebase';

const auth = firebase.auth();
const database = firebase.database();

export function loadNotifications (callback) {
    // if (auth.currentUser) {
        database.ref('notifications').once('value')
            .then((snapshot) => {
                const notifies = snapshot.val();
                callback(true, notifies, null);
            })
        
    // } else {
    //     callback(false, null, {code: 401, message: 'Session expired.'});
    // }
}