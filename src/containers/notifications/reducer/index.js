import * as t from './actionTypes';

const initialState = { isLoading: false, notifications: [] };

const notificationReducer = (state = initialState, action) => {
	switch (action.type) {
		case t.NOTIFICATION_LOAD_SUCCESS:
			var notifications = action.notifications;
			var notArr = [];

			for (var notification in notifications) {
					notArr.push(notifications[notification]);
			}

			state = Object.assign({}, state, { notifications: notArr });

			return state;
	
		case t.NOTIFICATION_SET_BUSY:
		
			return Object.assign({}, state, { isLoading: true });

		case t.NOTIFICATION_UNSET_BUSY:
		
			return Object.assign({}, state, { isLoading: false });

		default:
		
			return state;
	}
};

export default notificationReducer;
