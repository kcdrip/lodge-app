import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { Platform, ScrollView, Button, Image, StyleSheet, Text, TextInput, View, Dimensions, TouchableOpacity, ActivityIndicator, StatusBar } from 'react-native';
import { Title, Icon } from 'native-base';
import Entypo from 'react-native-vector-icons/Entypo';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import moment from 'moment';

import { loadNotifications, leaveComment } from './reducer/actions';
import NavBarContainer from '../../components/navbar';

const { height, width } = Dimensions.get('window');

const styles = StyleSheet.create({
	container: {
		flex: 1,
		// alignItems: 'center',
		padding: 10,
	},
	main: {
        height: height - 124,
	},
	title: {
		color: '#767676',
		textAlign: 'center',
		fontSize: 23,
		fontWeight: 'bold',
	},
	header: {
		flexDirection: 'row',
		alignItems: 'center',
	},
	content: {
		backgroundColor: '#F1F9FF',
		padding: 20,
		marginLeft: 40,
		justifyContent: 'space-between',
	},
	contentText: {
		flexWrap: 'wrap',
		fontFamily: 'arial',
		fontSize: 14,
		textAlign: 'justify',
		color: '#2699FB',
    	lineHeight: 25,
	},
	icon: {
		height: 40,
		width: 40,
		justifyContent: 'center',
		alignItems: 'center',
		backgroundColor: '#2699FB',
		borderRadius: 20,
		marginRight: 10,
	},
	bottom: {
		flex: 1,
		flexDirection: 'row',
		justifyContent: 'space-between',
		marginTop: 20,
	},
	iconButton: {
		flexDirection: 'row',
		justifyContent: 'center',
		alignItems: 'center',
		marginRight: 20,
		height: 30,
		width: 72,
		borderRadius: 4,
	},
	iconButtonText: {
		fontFamily: 'arial',
		fontWeight: 'bold',
		fontSize: 14,
		color: '#2699FB',
	},
	iconReversed: {
		backgroundColor: '#2699FB',
	},
});

class Comment extends Component {
	static propTypes = {
		name: PropTypes.string.isRequired,
		comment: PropTypes.string.isRequired,
		time: PropTypes.object,	
	}

	constructor(props) {
		super(props);

	}

	render() {
		const { name, comment, time } = this.props;

		const relativeTime = moment(time).fromNow();
		
		return (
			<View style={styles.container}>
				<View style={[styles.header, {justifyContent: 'space-between'}]}>
					<View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
						<View style={styles.icon}>
							<Icon name="person" style={{fontSize: 20, color: 'white'}}/>
						</View>
						<Text style={{fontFamily: 'arial', fontSize: 14, color: '#2699FB'}}>{name}</Text>
					</View>
					<Text style={{fontFamily: 'arial', fontSize: 14, color: '#2699FB'}}>{relativeTime}</Text>
				</View>
				<View style={{marginLeft: 40}}>
					<Text style={styles.contentText}>{comment}</Text>
				</View>
			</View>
		)
	}
}

class Notification extends Component {
	static propTypes = {
		content: PropTypes.string.isRequired,
	}

	constructor(props) {
		super(props);

		var keys = Object.keys(props.comments);
		var comments = new Array();
		for (var i = 0; i < keys.length; i ++) {
			comments.push(props.comments[keys[i]]);
		}

		this.state = {
			isCommentOn: false,
			newComment: '',
			comments: comments
		};

		this._onCommentOn = this._onCommentOn.bind(this);
		this._onAddComment = this._onAddComment.bind(this);
	}

	_onCommentOn() {
		this.setState({ isCommentOn: !this.state.isCommentOn });
	}

	_onAddComment() {
		var cmts = this.state.comments;
		var newCmt = {
			name: 'You',
			content: this.state.newComment,
		}

		if (!this.state.comments) {
			cmts = new Array();
		}

		cmts.push(newCmt);
		this.setState({ comments: cmts, newComment: '' });

		this.props.leaveComment({
			notId: this.props.id,
			userName: 'Anymous',
			comment: this.state.newComment
		});
	}

	render() {
		const { content } = this.props;
		const exStyle_CI = this.state.isCommentOn ? styles.iconReversed : null;
		const exStile_icon = this.state.isCommentOn ? { color: 'white' } : null;
		const commentCount = this.state.comments.length;

		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<View style={styles.icon}>
						<MaterialIcons name="notifications" style={{fontSize: 20, color: 'white'}}/>
					</View>
					<Text style={{fontFamily: 'arial', fontSize: 14, color: global.theme.box.text}}> 1h ago </Text>
				</View>
				<View style={[styles.content, {backgroundColor: global.theme.box.background}]}>
					<ScrollView><Text style={[styles.contentText, {color: global.theme.box.text}]}>{content}</Text></ScrollView>
					<View style={styles.bottom}>
						<View style={{flexDirection: 'row'}}>
							<TouchableOpacity style={styles.iconButton}>
								<Entypo name="heart" style={{fontSize: 20, color: global.theme.box.text, marginRight: 10}}/>
								<Text style={[styles.iconButtonText, {color: global.theme.box.text}]}>609</Text>
							</TouchableOpacity>
							<TouchableOpacity style={[styles.iconButton, exStyle_CI]} onPress={this._onCommentOn}>
								<MaterialIcons name="comment" style={[{fontSize: 20, color: global.theme.box.text, marginRight: 10}, exStile_icon]}/>
								<Text style={[styles.iconButtonText, {color: global.theme.box.text}, exStile_icon]}>{`${commentCount}`}</Text>
							</TouchableOpacity>
						</View>
						<TouchableOpacity style={styles.iconButton}>
							<Entypo name={'dots-three-vertical'} style={{fontSize: 20, color: global.theme.box.text}}/>
						</TouchableOpacity>
					</View>
				</View>
				{
					this.state.isCommentOn && <View style={{marginBottom: 30}}>
						<Text style={[styles.title, {marginTop: 10, fontSize: 14}]}>Comments...</Text>
						{
							commentCount > 0 && this.state.comments.map((comment, id) => {
								return <Comment key={id} name={comment.name} comment={comment.content} time={comment.time}/>
							})
						}
						<View style={{flexDirection: 'row', justifyContent: 'space-between'}}>
							<TextInput
								style={{padding: 10, marginRight: 10, borderRadius: 20, height: 48, borderColor: 'gray', borderWidth: 1, width: '85%'}}
								underlineColorAndroid="transparent"
								multiline={true}
								onChangeText={(text) => this.setState({newComment: text})}
								value={this.state.newComment}
							/>
							<TouchableOpacity style={[styles.iconButton, {height: 48, width: 48, borderRadius: 24, backgroundColor: '#BCE0FD'}]} onPress={this._onAddComment}>
								<MaterialIcons name="send" style={{fontSize: 24, color: '#2699FB'}}/>
							</TouchableOpacity>
						</View>
					</View>
				}
			</View>
		)
	}
}

class NotificationView extends Component {

	constructor (props) {
		super(props)

	}

	componentWillMount() {
		this.props.loadNotifications(this.onSuccess.bind(this), this.onError.bind(this));
	}

	onSuccess() {

	}

	onError() {

	}

	render() {
		
		const notifications = this.props.notifications;
		return (
			<NavBarContainer><StatusBar hidden={true}/>
				<View style={styles.container}>
					<Text style={styles.title}>Notifications</Text>
					{this.props.isLoading &&
						<View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
							<ActivityIndicator />
						</View>
					}
					{!this.props.isLoading &&
						notifications.map((not, id) => {
							return <Notification key={id} id={not.id} content={not.content} comments={not.comments ? not.comments : []} leaveComment={this.props.leaveComment}/>
						})
					}
				</View>
			</NavBarContainer>
		);
	}
}

const mapDispatchToProps = {
	loadNotifications,
	leaveComment
};

const mapStateToProps = state => ({
	user: state.authReducer.user,
	notifications: state.notificationReducer.notifications,
	isLoading: state.notificationReducer.isLoading
});

export default connect(mapStateToProps, mapDispatchToProps)(NotificationView)