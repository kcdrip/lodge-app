import phoneFormatter from 'phone-formatter';
import moment from 'moment';

const formatPhone = phone => 
  phone ? 
    phone.length >= 10 ? 
      phoneFormatter.format(phone, '(NNN) NNN-NNNN') :
      phone : 
    null;

const formatDate = date => date ? moment(date).format('l') : '';

export {
  formatPhone,
  formatDate
}