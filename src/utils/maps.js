const getStaticStyle = styles => styles.map(({
  stylers = [],
  featureType = 'all',
  elementType = 'all',
}) => `style=${encodeURIComponent(stylers.reduce((p, v) => {
  const propertyName = Object.keys(v)[0];
  const propertyVal = v[propertyName].toString().replace('#', '0x');
  return `${p}${propertyName}:${propertyVal}|`;
}, `feature:${featureType}|element:${elementType}|`))}`).join('&');

export { getStaticStyle };
