const getTimeFrame = (event) => {
  const startTime = event.get('from');
  const endTime = event.get('to');
  const allDay = startTime == null || endTime == null;
  return allDay ? null : `${startTime} to ${endTime}`;
};

export {
  getTimeFrame,
};
