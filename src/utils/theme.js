const getItem = (property, state, groupName, itemName) => 
  state.hasIn(['auth', 'theme', groupName, itemName]) ? {
    [property]: state.getIn(['auth', 'theme', groupName, itemName]) 
  } : null;

const getColor = (...params) => getItem('color', ...params);
const getBg = (...params) => getItem('backgroundColor', ...params);
const getNavBarBgColor = state => state.getIn(['auth', 'theme', 'title', 'background']);
const getBarStyle = state => state.getIn(['auth', 'theme', 'title', 'darkStatusBar']) ? 'dark-content' : 'light-content';

export {
  getColor,
  getBg,
  getNavBarBgColor,
  getBarStyle,
}
