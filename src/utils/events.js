import RNCalendarEvents from 'react-native-calendar-events';
import { Platform } from 'react-native';
import moment from 'moment';


String.prototype.hashCode = function() {
  let hash = 0, i, chr;
  if (this.length === 0) return hash;
  for (i = 0; i < this.length; i++) {
    chr   = this.charCodeAt(i);
    hash  = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash.toString();
};

const addToCalendar = ({
  id,
  title,
  startDate,
  endDate: rawEndDate,
  allDay,
}) => {

  const endDate = moment(rawEndDate).isBefore(moment(startDate)) ? moment(rawEndDate).add(1, 'day').toISOString() : rawEndDate;

  console.log('adding', {
      startDate,
      endDate,
      allDay,
     // id: Platform.OS === 'ios' ? id : id.hashCode(),
    });

  const save = () => RNCalendarEvents.saveEvent(
    title,
    {
      startDate,
      endDate,
      allDay,
     // id: Platform.OS === 'ios' ? id : id.hashCode(),
    }, 
    {},
  );

  return RNCalendarEvents.authorizationStatus()
    .then(status => {
      console.log('status', status);
      return status === 'authorized' ? save() :
        RNCalendarEvents.authorizeEventStore()
          .then(status => (
            status === 'authorized' ? save() : null
          ))});
};

export { addToCalendar }
