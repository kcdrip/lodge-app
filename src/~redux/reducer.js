import { combineReducers } from 'redux';

import authReducer from '../containers/auth/reducer';
import homeReducer from '../containers/home/reducer';
import cardReducer from '../containers/loyalty/reducer';
import otherReducer from '../containers/other/reducer';
import notificationReducer from '../containers/notifications/reducer';
// Combine all the reducers
const rootReducer = combineReducers({ 
    authReducer, 
    homeReducer, 
    cardReducer, 
    otherReducer,
    notificationReducer
});

export default rootReducer;