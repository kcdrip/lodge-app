import { createAction, handleActions } from 'redux-actions';

export const actions = {
    setRewardsLoading: 'REWARD/LOADING_REWARDS',
    unsetRewardsLoading: 'REWARD/LOADED_REWARDS',
    loadRewards: 'REWARD/LOAD_REWARDS',
    setRewards: 'REWARD/SET_REWARDS',
};

export const setRewardsLoading = createAction(actions.setRewardsLoading);
export const unsetRewardsLoading = createAction(actions.unsetRewardsLoading);
export const loadRewards = createAction(actions.loadRewards);
export const setRewards = createAction(actions.setRewards);

const defaultState = { rewards: [], isLoadingRewards: false };

export default handleActions({
    [actions.setRewardsLoading]: (state, action) => ({ ...state, isLoadingRewards: true }),
    [actions.unsetRewardsLoading]: (state, action) => ({ ...state, isLoadingRewards: false }),
    [actions.setRewards]: (state, action) => ({ ...state, rewards: action.payload }),
}, defaultState);
