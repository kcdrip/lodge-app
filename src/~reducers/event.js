import { createAction, handleActions } from 'redux-actions';

export const actions = {
    loadFeaturedEvents: 'event/LOAD_FEATURED_EVENTS',
    loadDailyEvents: 'event/LOAD_DAILY_EVENTS',
    setDailyEvents: 'event/SET_DAILY_EVENTS',
    setFeaturedEvents: 'event/SET_FEATURED_EVENTS',
    selectDailyEvent: 'event/SELECT_DAILY_EVENT',
    selectFeaturedEvent: 'event/SELECT_FEATURED_EVENT',
    setSelectedFE: 'event/SET_SELECTED_FE',
    setSelectedDE: 'event/SET_SELECTED_DE',
    setFELoading: 'event/SET_FE_LOADING',
    setDELoading: 'event/SET_DE_LOADING',
    unsetDELoading: 'event/UNSET_DE_LOADING',
    unsetFELoading: 'event/UNSET_FE_LOADING',
    setEventsLoading: 'event/SET_EVENTS_LOADING',
};

export const loadFeaturedEvents = createAction(actions.loadFeaturedEvents);
export const loadDailyEvents = createAction(actions.loadDailyEvents);
export const setDailyEvents = createAction(actions.setDailyEvents);
export const setFeaturedEvents = createAction(actions.setFeaturedEvents);
export const selectDailyEvent = createAction(actions.selectDailyEvent);
export const selectFeaturedEvent = createAction(actions.selectFeaturedEvent);
export const setSelectedFE = createAction(actions.setSelectedFE);
export const setSelectedDE = createAction(actions.setSelectedDE);
export const setFELoading = createAction(actions.setFELoading);
export const unsetFELoading = createAction(actions.unsetFELoading);
export const setDELoading = createAction(actions.setDELoading);
export const unsetDELoading = createAction(actions.unsetDELoading);
export const setEventsLoading = createAction(actions.setEventsLoading);

const defaultState = { featuredEvents: [], selectedFE: {}, special: '', isLoadingFE: false, dailyEvents: {}, selectedDE: {}, isLoadingDE: false };

export default handleActions({
    [actions.setSelectedFE]: (state, action) => {
        const id = action.payload;

        var selectedFE;
        // selectedFE = state.featuredEvents.Find(s => s.id === id);
        for (var i = 0; i < state.featuredEvents.length; i ++) {
            if (state.featuredEvents[i].id === id) {
                selectedFE = state.featuredEvents[i];
                break;
            }
        }

        return { ...state, selectedFE: selectedFE };
    },
    [actions.setFELoading]: (state, action) => ({ ...state, isLoadingFE: true }),
    [actions.unsetFELoading]: (state, action) => ({ ...state, isLoadingFE: false }),
    [actions.setSelectedDE]: (state, action) => {
        const id = action.payload;

        var selectedDE;
        for (var i = 0; i < state.dailyEvents.events.length; i ++) {
            if (state.dailyEvents.events[i].id === id) {
                selectedDE = state.dailyEvents.events[i];
            }
        }

        return { ...state, selectedDE: selectedDE };
    },
    [actions.setDELoading]: (state, action) => ({ ...state, isLoadingDE: true }),
    [actions.unsetDELoading]: (state, action) => ({ ...state, special: state.dailyEvents.special, isLoadingDE: false }),
    [actions.setFeaturedEvents]: (state, action) => ({ ...state, featuredEvents: action.payload }),
}, defaultState);