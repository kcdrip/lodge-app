import { call, put, takeEvery } from 'redux-saga/effects';

import { actions as eventActions } from '../event';
import { actions as navActions } from '../navigation';

import { getFeaturedEvents, getDailyEvents } from '../../api/event';

export function* selectFeaturedEvent(action) {
    const { id } = action.payload;

    yield put({ type: eventActions.setSelectedFE, payload: id });
    yield put({ type: navActions.gotoScreen, payload: 10});
}

export function* loadFeaturedEvents(action) {
    yield put({ type: eventActions.setFELoading });
    const events = yield call(getFeaturedEvents);

    yield put({ type: eventActions.setFeaturedEvents, payload: events });
    yield put({ type: eventActions.unsetFELoading });
}

export function* selectDailyEvent(action) {
    const { id } = action.payload;

    yield put({ type: eventActions.setSelectedDE, payload: id });
    yield put({ type: navActions.gotoScreen, payload: 10});
}

export function* loadDailyEvents(action) {
    yield put({ type: eventActions.setDELoading });
    const events = yield call(getDailyEvents);

    yield put({ type: eventActions.setDailyEvents, payload: events });
    yield put({ type: eventActions.unsetFELoading });
}

export default function* watchEventSaga() {
    yield takeEvery(eventActions.selectFeaturedEvent, selectFeaturedEvent)
    yield takeEvery(eventActions.loadFeaturedEvents, loadFeaturedEvents)
    yield takeEvery(eventActions.selectDailyEvent, selectDailyEvent)
    yield takeEvery(eventActions.loadDailyEvents, loadDailyEvents)
}