import { call, put, takeEvery } from 'redux-saga/effects';
import { actions as navActions } from '../navigation';
import { actions as authActions } from '../auth';
import { login } from '../../api/auth';
import store from '../../store';

export function* loginUser(action) {
    const { email, password } = action.payload;

    yield call(login, {email: email, password: password}, function(success, data, error) {
        if (success && data.exists)
            store.dispatch({ type: navActions.gotoScreen, payload: 1 });
    });
//    if ( email === 'test123@test.com' && password === 'test123' )
        
}

export default function* watchAuthSaga() {
    yield takeEvery(authActions.login, loginUser)
}