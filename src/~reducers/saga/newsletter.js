import { call, put, takeEvery } from 'redux-saga/effects';

import { actions as newsActions } from '../newsletter';
import { actions as navActions } from '../navigation';

import { getNewsletters } from '../../api/newsletter';

export function* loadNewsletters(action) {
    yield put({ type: newsActions.setNewsLoading });
    const data = yield call(getNewsletters);

    yield put({ type: newsActions.setNewsletters, payload: data });
    yield put({ type: newsActions.unsetNewsLoading });
}

export function* selectNewsletter(action) {
    const { id } = action.payload;

    yield put({ type: newsActions.setSelectedNews, payload: id });
    yield put({ type: navActions.gotoScreen, payload: 15 });
}

export default function* watchNewsletterSaga() {
    yield takeEvery(newsActions.loadNewsletters, loadNewsletters)
    yield takeEvery(newsActions.selectNewsletter, selectNewsletter)
}