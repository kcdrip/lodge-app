import { call, put, takeEvery } from 'redux-saga/effects';

import { actions as rewardActions } from '../reward';
import { actions as navActions } from '../navigation';

import { getRewards } from '../../api/reward';

export function* loadRewards(action) {
    yield put({ type: rewardActions.setRewardsLoading });
    const data = yield call(getRewards);

    yield put({ type: rewardActions.setRewards, payload: data });
    yield put({ type: rewardActions.unsetRewardsLoading });
}

export default function* watchRewardSaga() {
    yield takeEvery(rewardActions.loadRewards, loadRewards)
}