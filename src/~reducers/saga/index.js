import { all } from 'redux-saga/effects';
import watchAuthSaga from './auth';
import watchEventSaga from './event';
import watchRewardSaga from './reward';
import watchNewsletterSaga from './newsletter';

export default function* rootSaga() {
    yield all([
        watchAuthSaga(),
        watchEventSaga(),
        watchRewardSaga(),
        watchNewsletterSaga(),
    ]);
};