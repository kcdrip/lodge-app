import { combineReducers } from 'redux';

import navigation from './navigation';
import event from './event';
import reward from './reward';
import newsletter from './newsletter';

const combinedReducers = combineReducers({
    navigation,
    event,
    reward,
    newsletter,
});

export default combinedReducers;