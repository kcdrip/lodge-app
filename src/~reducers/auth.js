import { createAction, handleActions } from 'redux-actions';

export const actions = {
    login: 'AUTH/LOGIN',
}

export const login = createAction(actions.login);

const defaultState = {};

export default handleActions({
}, defaultState);