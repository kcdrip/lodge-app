import { createAction, handleActions } from 'redux-actions';

export const actions = {
    setNewsLoading: 'NEWSLETTER/LOADING_NEWSLETTERS',
    unsetNewsLoading: 'NEWSLETTER/LOADED_NEWSLETTERS',
    loadNewsletters: 'NEWSLETTER/LOAD_NEWSLETTERS',
    setNewsletters: 'NEWSLETTER/SET_NEWSLETTERS',
    selectNewsletter: 'NEWSLETTER/SELECT_NEWSLETTER',
    setSelectedNews: 'NEWSLETTER/SET_SELECTED_NEWSLETTER',
};

export const setNewsLoading = createAction(actions.setNewsLoading);
export const unsetNewsLoading = createAction(actions.unsetNewsLoading);
export const loadNewsletters = createAction(actions.loadNewsletters);
export const setNewsletters = createAction(actions.setNewsletters);
export const selectNewsletter = createAction(actions.selectNewsletter);
export const setSelectedNews = createAction(actions.setSelectedNews);

const defaultState = { newsletters: [], selectedNews: {}, isLoadingNewsletters: false };

export default handleActions({
    [actions.setSelectedNews]: (state, action) => {
        const id = action.payload;

        var selectedNews;
        for (var i = 0; i < state.newsletters.length; i ++) {
            if (state.newsletters[i].id === id) {
                selectedNews = state.newsletters[i];
                break;
            }
        }

        return { ...state, selectedNews: selectedNews };
    },
    [actions.setNewsLoading]: (state, action) => ({ ...state, isLoadingNewsletters: true }),
    [actions.unsetNewsLoading]: (state, action) => ({ ...state, isLoadingNewsletters: false }),
    [actions.setNewsletters]: (state, action) => ({ ...state, newsletters: action.payload }),
}, defaultState);
