import { createAction, handleActions } from 'redux-actions';

export const actions = {
    gotoScreen: 'navigation/GOTO_SCREEN',
}

export const gotoScreen = createAction(actions.gotoScreen);

const defaultState = { currentScreen: 0, prevScreen: 0 };

export default handleActions({
    [actions.gotoScreen]: (state, action) => ({ ...state, prevScreen: state.currentScreen, currentScreen: action.payload }),
}, defaultState);