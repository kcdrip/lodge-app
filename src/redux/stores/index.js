import auth from './auth';
import events from './events';
import news from './news';
import friends from './friends';
import articles from './articles';
import notifications from './notifications';
import rewards from './rewards';

export default {
  auth,
  events,
  news,
  friends,
  articles,
  notifications,
  rewards,
};
