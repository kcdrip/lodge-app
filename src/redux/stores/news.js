import Immutable, { Map, OrderedMap } from 'immutable';
import createAsyncStores from 'cat-stores';
import moment from 'moment';

export default createAsyncStores({
  fetchNews: {
    complete: (state, { payload }) => state
      .set(
        'items',
        Object.keys(payload)
          .reduce((p, id) => p.set(id, Immutable.fromJS(payload[id])), OrderedMap())
          .sort((a, b) => (moment(a.get('date')).isAfter(moment(b.get('date'))) ? 1 : -1)),
      ),
  },
},
Map({
  items: OrderedMap(),
}));
