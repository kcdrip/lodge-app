import Immutable, { Map } from 'immutable';
import createAsyncStores from 'cat-stores';

const RESTORE_TEXT = 'Password restore email was sent.'

export default createAsyncStores({
  auth: {
    begin: state => state
      .set('isBusy', true),
    complete: (state, { payload }) => state
      .set('isBusy', false)
      .set('user', Immutable.fromJS(payload)),
    error: {
      default: (state, { payload }) => state
        .set('error', payload.message)
        .set('isBusy', false)
        .set('user', null),
    },
  },

  restorePassword: {
    begin: state => state
      .set('isBusy', true),
    complete: state => state
      .set('isBusy', false)
      .set('error', RESTORE_TEXT),
    error: {
      default: state => state
        .set('error', RESTORE_TEXT)
        .set('isBusy', false),
    },
  },

  fetchUserInfo: {
    complete: (state, { payload }) => state
      .set('user', Immutable.fromJS(payload)),
  },

  updateProfile: {
    begin: (state, { meta: { newData } }) => state
      .mergeIn(['user'], Map(newData)),
  },

  uploadImage: {
    begin: (state, { meta: { image } }) => state
      .setIn(['user', 'profileImage'], image),
    complete: (state, { payload }) => state
      .setIn(['user', 'profileImage'], payload),
  },

  updatePassword: {
    begin: state => state
      .set('isBusy', true),
    complete: state => state
      .set('isBusy', false)
      .set('error', 'Password successfully changed')
      .set('redirectTo', 'Profile'),
    error: {
      default: (state, { payload }) => state
        .set('isBusy', false)
        .set('error', payload.message === 'The password is invalid or the user does not have a password.' ? 'Old password is not correct' : payload.message),
    },
  },

  logout: {
    begin: state => state.set('user', null),
  },

  getRewardForOffer: {
    complete: (state, { payload }) => state
      .set('user', Immutable.fromJS(payload)),
  },

  updatePoints: {
    complete: (state, { payload }) => state
      .set('user', Immutable.fromJS(payload)),
  },

  loadTheme: {
    complete: (state, { payload }) => state
      .set('themeIsLoaded', true)
      .set('theme', Immutable.fromJS(payload)),
  },

  loadSettings: {
    complete: (state, { payload }) => state
      .set('settingsAreLoaded', true)
      .set('settings', Immutable.fromJS(payload)),
  },

  dismissError: state => state.set('error', null),
  clearRedirect: state => state.set('redirectTo', null),
},
Map({
  isBusy: false,
  error: null,
  user: null,
  redirectTo: null,
  theme: Map(),
  settings: Map(),
  themeIsLoaded: false,
  settingsAreLoaded: false,
}));
