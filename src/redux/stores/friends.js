import Immutable, { Map, OrderedMap } from 'immutable';
import createAsyncStores from 'cat-stores';

export default createAsyncStores({
  fetchFriends: {
    complete: (state, { payload }) => state
      .set(
        'items',
        Object.keys(payload)
          .reduce((p, id) => p.set(id, Immutable.fromJS(payload[id])), OrderedMap())
          .sort((a, b) => ((a.get('name') > b.get('name')) ? 1 : -1)),
      ),
  },
},
Map({
  items: OrderedMap(),
}));
