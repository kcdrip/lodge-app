import Immutable, { Map, OrderedMap, List } from 'immutable';
import createAsyncStores from 'cat-stores';
import moment from 'moment';

export default createAsyncStores({
  fetchEvents: {
    complete: (state, { payload }) => {
      let events = OrderedMap();

      const lastDate = moment().add(4, 'months');
      const yesterday = moment().subtract(1, 'day');

      Object.keys(payload).forEach(key => {
        const event = payload[key];

        const {repeat} = event;

        const initialDate = moment(event.date);

        let date = initialDate;

        const addEvent = date => {
          if (date.isAfter(yesterday)) {
            const id = `${key}:${date.format('YYYY-MM-DD')}`;
            events = events.set(
              id, 
              Immutable.fromJS({
                ...event,
                id,
                originalId: key,
                date: date.format('YYYY-MM-DD'),
                timestamp: date.unix(),
                initialDate: initialDate.format('YYYY-MM-DD'),
            }));
          }
        }

        addEvent(date);

        if (repeat !== 'once') {
          do {
            switch(repeat) {
              case 'day':
                date = date.add(1, 'day');
                break;
              case 'week':
                date = date.add(1, 'week');
                break;
              case '2weeks':
                date = date.add(2, 'weeks');
                break;
              case 'month':
                date = date.add(1, 'month');
                break;
              case 'year':
                date = date.add(1, 'year');
                break;
            }

            addEvent(date);          
   
          } while (date.isBefore(lastDate));
        }
      });

      const newState = state.set(
        'items', events.sort((a, b) => a.get('timestamp') - b.get('timestamp')),
      )

      return newState;
    },
  },
  fetchSpecials: {
    complete: (state, { payload }) => state
      .set(
        'specials',
        Immutable.fromJS(payload),
      ),
  },
},
Map({
  items: OrderedMap(),
  specials: List(), 
}));
