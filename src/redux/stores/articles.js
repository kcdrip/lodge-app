import Immutable, { Map, OrderedMap } from 'immutable';
import createAsyncStores from 'cat-stores';

export default createAsyncStores({
  fetchArticles: {
    complete: (state, { payload }) => state
      .set(
        'items',
        Object.keys(payload)
          .reduce((p, id) => p.set(id, Immutable.fromJS(payload[id])), OrderedMap())
          .sort((a, b) => (parseInt(a.get('order'), 10) || 0) - (parseInt(b.get('order'), 10) || 0)),
      ),
  },
},
Map({
  items: OrderedMap(),
}));
