import Immutable, { Map, OrderedMap } from 'immutable';
import createAsyncStores from 'cat-stores';
import moment from 'moment';
import uuid from 'uuid';

export default createAsyncStores({
  fetchNotifications: {
    complete: (state, { payload }) => state
      .set(
        'items',
        Object.keys(payload)
          .reduce((p, id) => p.set(id, Immutable.fromJS(payload[id])), OrderedMap())
          .sort((a, b) => (moment(a.get('date')).isAfter(moment(b.get('date'))) ? -1 : 1)),
      ),
  },
  setLikeFor: {
    begin: (state, { meta: { id, userId } }) => state
      .mergeIn(['items', id, 'likes'], Map({ [userId]: true })),
  },
  setDislikeFor: {
    begin: (state, { meta: { id, userId } }) => state
      .deleteIn(['items', id, 'likes', userId]),
  },
  markRead: {
    begin: (state, { meta: { id, userId } }) => state
      .mergeIn(['items', id, 'readers'], Map({ [userId]: true })),
  },
  sendComment: {
    begin: (state, { meta } ) => state
      .mergeIn(['items', meta.id, 'comments'], Immutable.fromJS({ [uuid.v4()]: {
        ...meta,
        date: moment().toISOString(),
      },
    })),
  }

},
Map({
  items: OrderedMap(),
}));
