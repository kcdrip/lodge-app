import Immutable, { Map } from 'immutable';
import createAsyncStores from 'cat-stores';

export default createAsyncStores({
  fetchRewards: {
    complete: (state, { payload }) => state
      .set(
        'items',
        Object.keys(payload)
          .reduce((p, id) => p.set(id, Immutable.fromJS(payload[id])), Map()),
      ),
  },
  checkIn: {
    complete: (state, { payload }) => state
      .setIn(['items', payload.id], Immutable.fromJS(payload)),
  },
},
Map({
  items: Map(),
}));
