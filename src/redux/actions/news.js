import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';

const newsDB = firebase.database().ref('news');

export default createActions({
  fetchNews: () => newsDB
    .once('value')
    .then(res => (res ? res.val() : [])),
});
