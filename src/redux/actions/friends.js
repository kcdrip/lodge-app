import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';

const friendsDB = firebase.database().ref('friends');

export default createActions({
  fetchFriends: () => friendsDB
    .once('value')
    .then(res => (res ? res.val() : [])),
});
