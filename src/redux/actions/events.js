import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';

const eventsDB = firebase.database().ref('events');
const specialsDB = firebase.database().ref('specials');

export default createActions({
  fetchEvents: () => eventsDB
    .once('value')
    .then(res => (res ? res.val() : [])),
  fetchSpecials: () => specialsDB
    .once('value')
    .then(res => (res ? res.val() : [])),
});
