import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';
import FBSDK from 'react-native-fbsdk';

const newUserData = {
  point: 0,
};

const usersDB = firebase.database().ref('users');

export default createActions({
  login: ({ email, password }) => ({
    useReducer: 'auth',
    payload: firebase.auth()
      // Login to Firebase
      .signInAndRetrieveDataWithEmailAndPassword(email, password)
      .then(({ user: { uid } }) => usersDB
        // And get user's record
        .child(uid)
        .once('value')
        .then(userData => userData.val())),
  }),

  fetchUserInfo: id => usersDB
    .child(id)
    .once('value')
    .then(userData => userData.val()),

  loginWithFacebook: () => ({
    useReducer: 'auth',
    payload: FBSDK.LoginManager
      // Login to FB
      .logInWithReadPermissions(['public_profile', 'email'])
      .then(({ isCancelled }) => (
        isCancelled ? null
          // Get FB token
          : FBSDK.AccessToken.getCurrentAccessToken()
            .then(({ accessToken }) => firebase.auth()
              // Signup to Firebase with FB token
              .signInAndRetrieveDataWithCredential(
                firebase.auth.FacebookAuthProvider.credential(accessToken),
              ))
            .then(({
              user: {
                uid,
                email,
                displayName,
                photoURL,
              },
            }) => usersDB // Let's try to fetch user data
              .child(uid)
              .once('value')
              .then(snapshot => snapshot.val())
              .then(userData => userData || usersDB // If record in 'users' exists we just return it
                .child(uid)
                // If not let's just create a new one
                .set({
                  ...newUserData,
                  name: displayName,
                  email,
                  profileImage: photoURL,
                  id: uid,
                })
                // And pass the same data to the reducer
                .then(err => err || ({
                  ...newUserData,
                  name: displayName,
                  email,
                  profileImage: photoURL,
                  id: uid,
                }))))
      )),
  }),

  register: ({ name, email, phone, password }) => ({
    useReducer: 'auth',
    payload: firebase.auth()
      .createUserWithEmailAndPassword(email, password)
      .then(({user: { uid, email }}) => usersDB
        .child(uid)
        .set({
          ...newUserData,
          name,
          email,
          phone,
          id: uid,
        })
        .then(err => err || ({
          ...newUserData,
          name,
          email,
          phone,
          id: uid,
        }))),
  }),

  updateProfile: (newData, id) => ({
    meta: ({ newData }),
    payload: usersDB
      .child(id)
      .update(newData)
      .then(err => err || { ok: true }),
  }),

  uploadImage: (image, id) => ({
    meta: ({ image }),
    payload: firebase.storage()
      .ref('profileImages')
      .child(id)
      .putFile(image)
      .then(({ downloadURL }) => usersDB
        .child(id)
        .update({
          profileImage: downloadURL,
        })
        .then(err => err || downloadURL)),
  }),

  updatePassword: (oldPassword, newPassword) => firebase.auth()
    .currentUser.reauthenticateWithCredential(
      firebase.auth.EmailAuthProvider.credential(
        firebase.auth().currentUser.email,
        oldPassword,
      ),
    )
    .then(() => firebase.auth()
      .currentUser.updatePassword(newPassword)),

  restorePassword: email => firebase.auth().sendPasswordResetEmail(email),

  logout: () => firebase.auth().signOut(),
  dismissError: () => true,
  clearRedirect: () => true,
  loadSettings: () => firebase.database()
    .ref('settings')
    .once('value')
    .then(data => {
      console.log('VVV');
      return data.val()
    })
    .catch(e => console.log('CCV',e)),
  loadTheme: () => firebase.database()
    .ref('theme')
    .once('value')
    .then(data => JSON.parse(data.val())),

});
