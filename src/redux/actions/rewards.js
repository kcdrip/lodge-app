import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';
import moment from 'moment';

const usersDB = firebase.database().ref('users');
const rewardsDB = firebase.database().ref('rewards');

import uuid from 'uuid';
//import { parse } from 'path';

export default createActions({
  getRewardForOffer: (data) => {
    const userId = firebase.auth().currentUser.uid;
    return ({
      meta: ({ data, userId }),
      payload: usersDB // TODO: How should we inform the client here?
        .child(userId)
        .child('rewards')
        .update({
          [data.offerId]: {
            id: data.offerId,
            name: data.offerName,
            date: moment().toISOString(),
          },
        })
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return usersDB
              .child(userId)
              .once('value')
              .then(userData => userData.val());
          }
        }),
    });
  },
  createLoyaltyReward: name => {
    const userId = firebase.auth().currentUser.uid;
    const rewardId = uuid.v4();
    return ({
      meta: ({ userId }),
      payload: usersDB // TODO: How should we inform the client here?
        .child(userId)
        .child('loyaltyRewards')
        .update({
          [rewardId]: {
            id: rewardId,
            name,
            date: moment().toISOString(),
          },
        })
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return usersDB
              .child(userId)
              .once('value')
              .then(userData => userData.val());
          }
        }),
    });
  },
  fetchRewards: () => rewardsDB
    .once('value')
    .then(res => (res ? res.val() : [])),
  checkIn: id => {
    const userId = firebase.auth().currentUser.uid;
    return ({
      meta: ({ id, userId }),
      payload: rewardsDB
        .child(id)
        .child('rewards')
        .update({
          [userId]: {
            name: firebase.auth().currentUser.name,
            date: moment().toISOString(),
          },
        })
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return rewardsDB
              .child(id)
              .once('value')
              .then(data => data.val());
          }
        }),
    });
  },
  updatePoints: point => {
    const userId = firebase.auth().currentUser.uid;
    return ({
      meta: ({ point, userId }),
      payload: usersDB // TODO: How should we inform the client here?
        .child(userId)
        .child('point')
        .set(point)
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return usersDB
              .child(userId)
              .once('value')
              .then(userData => userData.val());
          }
        }),
    });
  },
  updateLastCheckInTime: date => {
    const userId = firebase.auth().currentUser.uid;
    return ({
      meta: ({ date, userId }),
      payload: usersDB // TODO: How should we inform the client here?
        .child(userId)
        .child('lastCheckIn')
        .set(date)
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return usersDB
              .child(userId)
              .once('value')
              .then(userData => userData.val());
          }
        }),
    });
  },
  //transact
  updateTotalPointsEarned: () => {
    const userId = firebase.auth().currentUser.uid;

    return ({
      meta: ({ userId }),
      payload: usersDB // TODO: How should we inform the client here?
        .child(userId)
        .child('totalPointsEarned')
        .transaction(function(totalPointsEarned) {
          if(totalPointsEarned) {
            totalPointsEarned = totalPointsEarned + 1;
            return totalPointsEarned;
          } else return(1);
          
        })
        .then((err) => {
          if (err) {
            throw err;
          } else {
            return usersDB
              .child(userId)
              .once('value')
              .then(userData => userData.val());
          }
        }),
    });
  },
});
