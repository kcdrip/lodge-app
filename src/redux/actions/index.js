
import authActions from './auth';
import eventsActions from './events';
import newsActions from './news';
import friendsActions from './friends';
import articlesActions from './articles';
import notificationsActions from './notifications';
import rewardsActions from './rewards';

export {
  authActions,
  eventsActions,
  newsActions,
  friendsActions,
  articlesActions,
  notificationsActions,
  rewardsActions,
};
