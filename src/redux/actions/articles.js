import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';

const acticlesDB = firebase.database().ref('articles');

export default createActions({
  fetchArticles: () => acticlesDB
    .once('value')
    .then(res => (res ? res.val() : [])),
});
