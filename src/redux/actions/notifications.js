import { createActions } from 'redux-feline-actions';
import firebase from 'react-native-firebase';
import moment from 'moment';

import uuid from 'uuid';

const notificationsDB = firebase.database().ref('notifications');

const getUid = () => firebase.auth().currentUser.uid;
const getName = () => firebase.auth().currentUser.displayName;

export default createActions({
  fetchNotifications: () => notificationsDB
    .once('value')
    .then(res => (res ? res.val() : [])),

  setLikeFor: id => ({
    meta: ({ id, userId: getUid() }),
    payload: notificationsDB
      .child(id)
      .child('likes')
      .update({
        [getUid()]: {
          id: getUid(),
          date: moment().toISOString(),
        },
      })
      .then(err => err || { ok: true }),
  }),
  setDislikeFor: id => ({
    meta: ({ id, userId: getUid() }),
    payload: notificationsDB
      .child(id)
      .child('likes')
      .child(getUid())
      .remove()
      .then(err => err || { ok: true }),
  }),
  markRead: id => ({
    meta: ({ id, userId: getUid() }),
    payload: notificationsDB
      .child(id)
      .child('readers')
      .update({
        [getUid()]: {
          id: getUid(),
          date: moment().toISOString(),
        },
      })
      .then(err => err || { ok: true }),
  }),
  sendComment: (id, content, { userId, profileImage }) => ({
    meta: ({
      id,
      profileImage,
      content,
      userName: getName(),
    }),
    payload: notificationsDB
      .child(id)
      .child('comments')
      .update({
        [uuid.v4()]: {
          userId,
          profileImage,
          userName: getName(),
          content,
          date: moment().toISOString(),
        },
      })
      .then(err => err || { ok: true }),
  }),

});
