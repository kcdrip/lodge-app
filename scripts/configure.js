const prompt = require('prompt');
const colors = require('colors/safe');
const fs = require('fs');

prompt.message = '';
prompt.delimiter = colors.yellow(' >');

console.log(colors.yellow('Configure admin panel\n'));

fs.copyFileSync('./firebaseConfigFiles/google-services.json', './android/app/google-services.json');
fs.copyFileSync('./firebaseConfigFiles/GoogleService-Info.plist', './ios/GkMembershipApp/GoogleService-Info.plist');

prompt.start();

prompt.get([{
  name: 'appDisplayName',
  description: colors.magenta('Application name'),
  required: true
},{
  name: 'bundleId',
  description: colors.magenta('Bundle ID (like com.gkloyalty.main, should be used in FB app and Firebase configuration)'),
  required: true
}, {
  name: 'facebookAppId',
  description: colors.magenta('Facebook App ID'),
  pattern: /^[a-zA-Z0-9\-\_]+$/,
  message: 'Name must be only letters, digits, or dashes',
  required: true
} ], (err, res) => {
  if (err) {
    console.log('Operation cancelled');
    return;
  } else {
    const replaceMetas = text => Object.keys(res).reduce((p, key) => p.replace(new RegExp(`__${key}__`, 'g'), res[key]), text);

    const replaceLine = (text, template, replacer) => {
      const lines = text.split('\n');
      const result = lines.map(s => s.includes(template) ? replacer : s);
      return result.join('\n')
    }

    const replaceFile = (file, template, replacer) => {
      let text = fs.readFileSync(file, 'utf-8');
      fs.writeFileSync(file, replaceLine(text, template, replacer), 'utf-8');
    }

    let plist = fs.readFileSync('./default/DefaultInfo.plist', 'utf-8');
    fs.writeFileSync('./ios/GkMembershipApp/Info.plist', replaceMetas(plist), 'utf-8');

    let strings = fs.readFileSync('./default/defaultStrings.xml', 'utf-8');
    fs.writeFileSync('./android/app/src/main/res/values/strings.xml', replaceMetas(strings), 'utf-8');
    
    let proj = fs.readFileSync('./default/defaultProject.pbxproj', 'utf-8');
    fs.writeFileSync('./ios/GK Loyalty.xcodeproj/project.pbxproj', proj.replace(new RegExp(`com.giantkillers.main`, 'g'), res.bundleId), 'utf-8')

    replaceFile('./android/build.gradle', 'packageName', `        packageName = "${res.bundleId}"`);
    replaceFile('./android/app/src/main/AndroidManifest.xml', 'package="', `    package="${res.bundleId}">`);
    replaceFile('./android/app/src/main/java/com/gkloyalty/MainActivity.java', 'package', `package ${res.bundleId};`);
    replaceFile('./android/app/src/main/java/com/gkloyalty/MainApplication.java', 'package', `package ${res.bundleId};`);
    
    console.log('Project successfully configured\n');
  }
});
