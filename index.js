import { AppRegistry } from 'react-native';
import App from './src/index';
import {name as appName} from './app.json';
import bgMessaging from './src'; // <-- Import the file you created in (2)
AppRegistry.registerHeadlessTask('RNFirebaseBackgroundMessage', () => bgMessaging); 
AppRegistry.registerComponent(appName, () => App);



