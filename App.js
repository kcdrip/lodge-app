/*import React, { Component } from 'react';
import { StyleSheet, Text, View } from 'react-native';
import { Provider } from 'react-redux';

import store from './src/redux/store' // Change it according to current using structure
import Router from './src/config/routes';

import firebase from './src/config/firebase';

const database = firebase.database();

console.ignoredYellowBox = [
  'Setting a timer'
];

//console.disableYellowBox = true;

export default class App extends React.Component {

  state = {
    themeLoaded: false,
    settingsLoaded: false
  };

  componentWillMount() {
    database.ref('settings').once('value')
      .then((snap) => {
        console.log('global.settings ', snap.val());
        global.settings = snap.val();
        global.settings.rewards.geo = JSON.parse(global.settings.rewards.geo);
        this.setState({ settingsLoaded: true });
      })

    database.ref('theme').once('value')
      .then((snap) => {
        global.theme = JSON.parse(snap.val());
        console.log(global.theme)
        this.setState({ themeLoaded: true });
      })
  }

  render() {
    if (!this.state.themeLoaded || !this.state.settingsLoaded) {
      return (
        <View>
          <Text>Loading</Text>
        </View>
      );
    }

    return (
      <Provider store={store}>
        <Router />
      </Provider>
    );
  }
}

*/








/*

const instructions = Platform.select({
  ios: 'Press Cmd+R to reload,\n' + 'Cmd+D or shake for dev menu',
  android:
    'Double tap R on your keyboard to reload,\n' +
    'Shake or press menu button for dev menu',
});

type Props = {};
export default class App extends Component<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.welcome}>Welcome to React Native!</Text>
        <Text style={styles.instructions}>To get started, edit App.js</Text>
        <Text style={styles.instructions}>{instructions}</Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },


});
*/